@AllE2E_SFC @SFC-1
Feature: Omega Salesforce Validation

@SFAccountPayment @SFCorporateCC @SFWebCC
Scenario: TC-01:Starting the Salesforce Script
Given My WebApp 'Omega' is open

@SFAccountPayment @SFCorporateCC @SFWebCC
Scenario: TC-02:Verify Salesforce and login functionality
And I navigate to 'https://omega--omegadev.lightning.force.com' application
And I wait '5' seconds for presence of element 'OmegaSDFC_Logo'
Then I should see element 'OmegaSDFC_Logo' present on page
Then I should see element 'SalesforceEmail' present on page
And I enter 'pkumar@omega.com' in field 'SalesforceEmail' 
And I click 'SalesforceNext'
Then I should see element 'SalesforcePassword' present on page
And I enter 'RComegaQA@123' in field 'SalesforcePassword'
Then I should see element 'SalesforceVerify' present on page
And I click 'SalesforceVerify'
And I wait for '5' seconds

#=====Order Validation Account Payment Corporate Customer=======#
@SFAccountPayment
Scenario: TC-03:Order Validation in Salesforce for Account Payment Corporate Customer Search for the product and validate the values
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'OrderNumber' from 'AccountPayment' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 

@SFAccountPayment
Scenario: TC-04:Order Validation in Salesforce for Account Payment Corporate Customer in Excel sheet
Then I validate 'OrderNumber' from 'AccountPayment' sheet contained at 'SalesforceOrderName'
And I get the details 'SalesforceOrderStatusNumber' from 'AccountPayment' and store as 'SalesforceOrderStatusNumber'
Then I validate 'PONumber' from 'AccountPayment' sheet contained at 'SalesforcePONumber'
Then I validate 'AccountName' from 'AccountPayment' sheet contained at 'SalesforceAccountName'
Then I validate 'OrderPlacedBy' from 'AccountPayment' sheet contained at 'SalesforceContact'
And I get the details 'SalesforceOrderStartDate' from 'AccountPayment' and store as 'SalesforceOrderStartDate'
#And I get the details 'SalesforceStatus' from 'AccountPayment' and store as 'SalesforceStatus'
#And I find Total at 'OrderTotal' from 'AccountPayment' sheet contained at 'SalesforceOrderTotal'

@SFAccountPayment
Scenario: TC-05:Order Validation in Salesforce for Account Payment Corporate Customer where we validate header
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
Then I should see element 'SalesforceOrderHeader' present on page
Then I should see element 'SalesforceOrderHeader_AccName' present on page
Then I should see element 'SalesforceOrderHeader_Status' present on page
Then I should see element 'SalesforceOrderHeader_OrderTotal' present on page

@SFAccountPayment
Scenario: TC-06:Order Validation in Salesforce for Account Payment Corporate Customer where we validate header from excel
Then I validate 'OrderNumber' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader'
Then I validate 'AccountName' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader_AccName'
#Then I validate 'SalesforceStatus' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader_Status'
#Then I find Total at 'OrderTotal' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader_OrderTotal'

@SFAccountPayment
Scenario: TC-07:Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info
Then I should see element 'SalesforceOrderInfo_OrderName' present on page
Then I should see element 'SalesforceOrderInfo_OrderStartDate' present on page
Then I should see element 'SalesforceOrderInfo_AccountName' present on page
Then I should see element 'SalesforceOrderInfo_Status' present on page
Then I should see element 'SalesforceOrderInfo_OrderSource' present on page
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
Then I should see text 'Customer' contained on page at 'SalesforceOrderInfo_MethodOfEntry'

@SFAccountPayment
Scenario: TC-08:Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info from excel sheet
Then I validate 'OrderNumber' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_OrderName'
Then I validate 'SalesforceOrderStartDate' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_OrderStartDate'
Then I validate 'AccountName' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_AccountName'
Then I validate 'SalesforceStatus' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_Status'

@SFAccountPayment
Scenario: TC-09:Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info
And I scroll to 'coordinates' - '0,700'
Then I should see element 'SalesforceCostsInfo_Subtotal' present on page
Then I should see element 'SalesforceCostsInfo_ShippingAmount' present on page
Then I should see element 'SalesforceCostsInfo_DiscountPercentage' present on page
Then I should see element 'SalesforceCostsInfo_DiscountAmount' present on page
Then I should see element 'SalesforceCostsInfo_OrderTotal' present on page

@SFAccountPayment
Scenario: TC-10:Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info from Excel sheet
Then I should see element 'SalesforceCostsInfo_Subtotal' present on page
Then I should see element 'SalesforceCostsInfo_ShippingAmount' present on page
Then I should see element 'SalesforceCostsInfo_DiscountPercentage' present on page
Then I should see element 'SalesforceCostsInfo_DiscountAmount' present on page
Then I should see element 'SalesforceCostsInfo_OrderTotal' present on page

@SFAccountPayment
Scenario: :TC-11:Order validation in salesforce for Account Payment Corporate Customer Billing Information
And I scroll to 'coordinates' - '0,400'
Then I should see element 'SalesforceBillingInfo_SytelineBillTo' present on page
Then I should see text 'ACCOUNT' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'N30' contained on page at 'SalesforceBillingInfo_PaymentTerms'
Then I should see element 'SalesforceBillingInfo_PONumber' present on page
Then I should see element 'SalesforceShippingInfo_ShipVia' present on page

@SFAccountPayment
Scenario: TC-12:Order validation in salesforce for Account Payment Corporate Customer Billing Information from excel sheet
Then I should see text 'C100117-0' contained on page at 'SalesforceBillingInfo_SytelineBillTo'
Then I should see text 'ACCOUNT' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'N30' contained on page at 'SalesforceBillingInfo_PaymentTerms'
Then I validate 'PONumber' from 'AccountPayment' sheet contained at 'SalesforceBillingInfo_PONumber'
Then I validate 'ShippingMethod' from 'AccountPayment' sheet contained at 'SalesforceShippingInfo_ShipVia'

@SFAccountPayment
Scenario: TC-13:Order validation in salesforce for Account Payment Corporate Customer Shipping Information
And I should see element 'SalesforceShippingInfo_ShipTo' present on page
And I get the details 'SalesforceShippingInfo_ShipTo' from 'AccountPayment' and store as 'SalesforceShippingInfo_ShipTo'
And I should see element 'SalesforceShippingInfo_ShipVia' present on page
Then I should see text 'UPS Ground - $15.00' contained on page at 'SalesforceShippingInfo_ShipVia'

@SFAccountPayment
Scenario: TC-14:Order Validation in Salesforce for Account Payment Corporate Customer validate order line items
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderLine_ProductID' present on page
Then I find Product at 'ItemName' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_ProductID'
And I should see element 'SalesforceOrderLine_Quantity' present on page
Then I find Quantity at 'Quantity' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_Quantity'
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
Then I find Total at 'UnitPrice' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_UnitPrice'
And I should see element 'SalesforceOrderLine_TotalPrice' present on page
Then I find Total at 'TotalPrice' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_TotalPrice'

#=====Order Validation CC Payment Corporate Customer=======#
@SFCorporateCC
Scenario: TC-15:Order Validation in Salesforce for CC Payment Corporate Customer Search for the product and validate the values
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'OrderNumber' from 'CorporateCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 

@SFCorporateCC
Scenario: :TC-16:Order Validation in Salesforce for CC Payment Corporate Customer in Excel sheet
Then I validate 'OrderNumber' from 'CorporateCC' sheet contained at 'SalesforceOrderName'
And I get the details 'SalesforceOrderStatusNumber' from 'CorporateCC' and store as 'SalesforceOrderStatusNumber'
Then I validate 'PONumber' from 'CorporateCC' sheet contained at 'SalesforcePONumber'
Then I validate 'AccountName' from 'CorporateCC' sheet contained at 'SalesforceAccountName'
Then I validate 'OrderPlacedBy' from 'CorporateCC' sheet contained at 'SalesforceContact'
And I get the details 'SalesforceOrderStartDate' from 'CorporateCC' and store as 'SalesforceOrderStartDate'
And I get the details 'SalesforceStatus' from 'CorporateCC' and store as 'SalesforceStatus'
#And I find Total at 'OrderTotal' from 'CorporateCC' sheet contained at 'SalesforceOrderTotal'

@SFCorporateCC
Scenario: TC-17:Order Validation CC Payment Corporate Customer page header to check for the elements
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader_AccName' present on page
And I should see element 'SalesforceOrderHeader_Status' present on page
And I should see element 'SalesforceOrderHeader_OrderTotal' present on page

@SFCorporateCC
Scenario: TC-18:Order Validation CC Payment Corporate Customer page header to check for data from excel
Then I validate 'OrderNumber' from 'CorporateCC' sheet contained at 'SalesforceOrderHeader'
Then I validate 'AccountName' from 'CorporateCC' sheet contained at 'SalesforceOrderHeader_AccName'
#Then I validate 'SalesforceStatus' from 'CorporateCC' sheet contained at 'SalesforceOrderHeader_Status'
#Then I find Total at 'OrderTotal' from 'CorporateCC' sheet contained at 'SalesforceOrderHeader_OrderTotal'

@SFCorporateCC
Scenario: TC-19:Order Validation CC Payment Corporate Customer page contents to check the elements
And I should see element 'SalesforceOrderInfo_OrderName' present on page
And I should see element 'SalesforceOrderInfo_OrderStartDate' present on page
And I should see element 'SalesforceOrderInfo_AccountName' present on page
And I should see element 'SalesforceOrderInfo_Status' present on page
And I should see element 'SalesforceOrderInfo_OrderSource' present on page
And I should see element 'SalesforceOrderInfo_MethodOfEntry' present on page
And I scroll to 'coordinates' - '0,750'
And I should see element 'SalesforceCostsInfo_OrderTotal' present on page
And I scroll to 'coordinates' - '0,300'
And I should see element 'SalesforceBillingInfo_PaymentType' present on page
And I should see element 'SalesforceBillingInfo_PaymentTerms' present on page
And I should see element 'SalesforceBillingInfo_PONumber' present on page
And I should see element 'SalesforceShippingInfo_ShipVia' present on page

@SFCorporateCC
Scenario: TC-20:Order Validation CC Payment Corporate Customer page contents to check the data
And I validate 'OrderNumber' from 'CorporateCC' sheet contained at 'SalesforceOrderInfo_OrderName'
And I validate 'SalesforceOrderStartDate' from 'CorporateCC' sheet contained at 'SalesforceOrderInfo_OrderStartDate'
And I validate 'SalesforceAccountName' from 'CorporateCC' sheet contained at 'SalesforceOrderInfo_AccountName'
And I validate 'SalesforceStatus' from 'CorporateCC' sheet contained at 'SalesforceOrderInfo_Status'
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
Then I should see text 'Customer' contained on page at 'SalesforceOrderInfo_MethodOfEntry'
And I validate 'SalesforceOrderTotal' from 'CorporateCC' sheet contained at 'SalesforceCostsInfo_OrderTotal'
Then I should see text 'CARD' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'CC' contained on page at 'SalesforceBillingInfo_PaymentTerms'
And I validate 'SalesforcePONumber' from 'CorporateCC' sheet contained at 'SalesforceBillingInfo_PONumber'
Then I should see text 'UPS Ground - $15.00' contained on page at 'SalesforceShippingInfo_ShipVia'

@SFCorporateCC
Scenario: TC-21:Order Validation CC Payment Corporate Customer page order line items to check the elements
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderLine_ProductID' present on page
And I should see element 'SalesforceOrderLine_Quantity' present on page
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
And I should see element 'SalesforceOrderLine_TotalPrice' present on page
And I should see element 'SalesforceOrderLine_ProductID2' present on page
And I should see element 'SalesforceOrderLine_Quantity2' present on page
And I should see element 'SalesforceOrderLine_UnitPrice2' present on page
And I should see element 'SalesforceOrderLine_TotalPrice2' present on page

@SFCorporateCC
Scenario: TC-22:Order Validation CC Payment Corporate Customer page order line items to check the datas
And I find Product at 'ItemName1' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_ProductID'
And I find Quantity at 'Quantity1' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_Quantity'
And I find Total at 'UnitPrice1' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_UnitPrice'
And I find Total at 'TotalPrice1' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_TotalPrice'
And I find Product at 'ItemName2' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_ProductID2'
And I find Quantity at 'Quantity2' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_Quantity2'
And I find Total at 'UnitPrice2' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_UnitPrice2'
And I find Total at 'TotalPrice2' from 'CorporateCC' sheet contained at 'SalesforceOrderLine_TotalPrice2'

#=====Order Validation  Web Customer CC Payment=======#
@SFWebCC
Scenario: TC-23:Order Validation Web Customer CC Payment to search the product
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'OrderNumber' from 'WebCustomerCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 

@SFWebCC
Scenario: TC-24:Order validation in search page from given datas
Then I validate 'OrderNumber' from 'WebCustomerCC' sheet contained at 'SalesforceOrderName'
And I get the details 'SalesforceOrderStatusNumber' from 'WebCustomerCC' and store as 'SalesforceOrderStatusNumber'
Then I validate 'PONumber' from 'WebCustomerCC' sheet contained at 'SalesforcePONumber'
Then I validate 'AccountName' from 'WebCustomerCC' sheet contained at 'SalesforceAccountName'
Then I validate 'OrderPlacedBy' from 'WebCustomerCC' sheet contained at 'SalesforceContact'
And I get the details 'SalesforceOrderStartDate' from 'WebCustomerCC' and store as 'SalesforceOrderStartDate'
And I get the details 'SalesforceStatus' from 'WebCustomerCC' and store as 'SalesforceStatus'
#And I find Total at 'OrderTotal' from 'WebCustomerCC' sheet contained at 'SalesforceOrderTotal'

@SFWebCC
Scenario: TC-25:Order validation  web customer cc payment page header validation to check for the elements
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader_AccName' present on page
And I should see element 'SalesforceOrderHeader_Status' present on page
And I should see element 'SalesforceOrderHeader_OrderTotal' present on page

@SFWebCC
Scenario: TC-26:Order validation  web customer cc payment page header validation from data
Then I validate 'OrderNumber' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader'
Then I validate 'AccountName' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader_AccName'
#Then I validate 'SalesforceStatus' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader_Status'
#Then I find Total at 'OrderTotal' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader_OrderTotal'

@SFWebCC
Scenario: TC-27:Order validation  web customer cc payment page contents validation to check for the elements
And I should see element 'SalesforceOrderInfo_OrderName' present on page
And I should see element 'SalesforceOrderInfo_OrderStartDate' present on page
And I should see element 'SalesforceOrderInfo_AccountName' present on page
And I should see element 'SalesforceOrderInfo_Status' present on page
And I should see element 'SalesforceOrderInfo_OrderSource' present on page
And I should see element 'SalesforceOrderInfo_MethodOfEntry' present on page
And I scroll to 'coordinates' - '0,750'
And I should see element 'SalesforceCostsInfo_OrderTotal' present on page
And I scroll to 'coordinates' - '0,300'
And I should see element 'SalesforceBillingInfo_PaymentType' present on page
And I should see element 'SalesforceBillingInfo_PaymentTerms' present on page
And I should see element 'SalesforceBillingInfo_PONumber' present on page
And I should see element 'SalesforceShippingInfo_ShipVia' present on page

@SFWebCC
Scenario: TC-28:Order Validation  Web Customer CC Payment page contents validation to check the datas
And I validate 'OrderNumber' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_OrderName'
And I validate 'SalesforceOrderStartDate' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_OrderStartDate'
And I validate 'SalesforceAccountName' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_AccountName'
And I validate 'SalesforceStatus' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_Status'
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
Then I should see text 'Customer' contained on page at 'SalesforceOrderInfo_MethodOfEntry'
And I validate 'SalesforceOrderTotal' from 'WebCustomerCC' sheet contained at 'SalesforceCostsInfo_OrderTotal'
Then I should see text 'CARD' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'CC' contained on page at 'SalesforceBillingInfo_PaymentTerms'
And I validate 'SalesforcePONumber' from 'WebCustomerCC' sheet contained at 'SalesforceBillingInfo_PONumber'
Then I should see text 'UPS Ground - $15.00' contained on page at 'SalesforceShippingInfo_ShipVia'

@SFWebCC
Scenario: TC-29:Order Validation  Web Customer CC Payment page order line items
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderLine_ProductID' present on page
And I should see element 'SalesforceOrderLine_Quantity' present on page
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
And I should see element 'SalesforceOrderLine_TotalPrice' present on page

@SFWebCC
Scenario: TC-30:Order Validation  Web Customer CC Payment page order line items
And I find Product at 'ItemName' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_ProductID'
And I find Quantity at 'Quantity' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_Quantity'
And I find Total at 'UnitPrice' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_UnitPrice'
And I find Total at 'TotalPrice' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_TotalPrice'

#=== Credit Application web ====#
@Ignore
Scenario: TC-19: Verify that User is able to apply for Corporate Account through Credit Application web form available in Hybris and verify the details in SFDC
Then I click 'SalesforceOmegaLogo'

#=== Credit Application web ====#
@Ignore
Scenario: TC-20 Verify that User is able to submit the Contact Us form in Hybris and able to see details in SFDC
Then I click 'SalesforceOmegaLogo'
#14