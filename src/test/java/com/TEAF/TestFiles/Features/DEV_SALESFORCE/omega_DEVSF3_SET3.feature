@AllE2E_SFC @SFC-3
Feature: Omega Salesforce Validation

@SFASM @SFCancel @SFPrepay @Ignore
Scenario: TC-01:Validation of Home Page
Given My WebApp 'Omega' is open
And I navigate to 'https://omega--omegadev.lightning.force.com' application
And I wait '5' seconds for presence of element 'OmegaSDFC_Logo'
Then I should see element 'OmegaSDFC_Logo' present on page
Then I should see element 'SalesforceEmail' present on page
And I enter 'tpatel@omega.com' in field 'SalesforceEmail' 
And I click 'SalesforceNext'
Then I should see element 'SalesforcePassword' present on page
And I enter 'kiZC*ZuoxFA6' in field 'SalesforcePassword'
Then I should see element 'SalesforceVerify' present on page
And I click 'SalesforceVerify'
And I wait for '5' seconds

#=====Order Validation ASM CC Payment Web user=======#
@SFASM
Scenario: TC-02:Verify Salesforce search product for ASM order
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'OrderNumber' from 'ASMWCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 
Then I validate 'OrderNumber' from 'ASMWCC' sheet contained at 'SalesforceOrderName'
Then I get the details 'SalesforceOrderStatusNumber' from 'ASMWCC' and store as 'SalesforceOrderStatusNumber'
Then I validate 'OrderNumber' from 'ASMWCC' sheet contained at 'SalesforceOrderName'
Then I validate 'AccountName' from 'ASMWCC' sheet contained at 'SalesforceAccountName'

@SFASM
Scenario: TC-03:Validation of Salesforce search 
And I click 'SalesforceOrderName'
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader_AccName' present on page
And I should see element 'SalesforceOrderHeader_Status' present on page
And I should see element 'SalesforceOrderHeader_OrderTotal' present on page
And I scroll to 'coordinates' - '0,700'
And I should see element 'SalesforceOrderInfo_OrderName' present on page
And I should see element 'SalesforceOrderInfo_OrderStartDate' present on page
And I should see element 'SalesforceOrderInfo_AccountName' present on page
And I should see element 'SalesforceOrderInfo_Status' present on page
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
And I should see element 'SalesforceOrderInfo_MethodOfEntry' present on page
And I should see text 'ASM' present on page at 'SalesforceOrderInfo_MethodOfEntry'
And I scroll to 'coordinates' - '0,350'
And I should see element 'SalesforceCostsInfo_OrderTotal' present on page
And I scroll to 'coordinates' - '0,300'
And I should see element 'SalesforceBillingInfo_PaymentType_ASM' present on page
Then I should see text 'CARD' contained on page at 'SalesforceBillingInfo_PaymentType_ASM'
And I should see element 'SalesforceBillingInfo_PaymentTerms_ASM' present on page
Then I should see text 'CC' contained on page at 'SalesforceBillingInfo_PaymentTerms_ASM'
And I validate 'PONumber' from 'ASMWCC' sheet contained at 'SalesforceBillingInfo_PONumber_ASM'
And I validate 'ShippingMethod' from 'ASMWCC' sheet contained at 'SalesforceShippingInfo_ShipVia_ASM'

@SFASM
Scenario: TC-04:Validate order line items
And I refresh the WebPage
And I should see element 'SalesforceOrderHeader' present on page
And I wait for '5' seconds
And I scroll to 'coordinates' - '0,700'
And I wait for visibility of element 'SalesforceOrderLine_ProductID'
And I should see element 'SalesforceOrderLine_ProductID' present on page
Then I find Product at 'ItemName' from 'ASMWCC' sheet contained at 'SalesforceOrderLine_ProductID'
And I should see element 'SalesforceOrderLine_Quantity' present on page
Then I find Quantity at 'Quantity' from 'ASMWCC' sheet contained at 'SalesforceOrderLine_Quantity'
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
Then I find Total at 'UnitPrice' from 'ASMWCC' sheet contained at 'SalesforceOrderLine_UnitPrice'
And I should see element 'SalesforceOrderLine_TotalPrice' present on page
Then I find Total at 'TotalPrice' from 'ASMWCC' sheet contained at 'SalesforceOrderLine_TotalPrice'

#======PrepayOrder Validation in Salesforce=====#
@SFPrepay
Scenario: TC-05:Verify Salesforce search product
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page
And I enter 'OrderNumber' from 'PrepayOrder' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 

@SFPrepay
Scenario: TC-06:Verify Salesforce search product and validate contents
And I validate 'OrderNumber' from 'PrepayOrder' sheet contained at 'SalesforceOrderName'
Then I should see text '12345' contained on page at 'SalesforcePONumber'
Then I should see text 'Royal Cyber' contained on page at 'SalesforceAccountName'
Then I should see text 'Taha Patelll' contained on page at 'SalesforceContact'
#Then I should see text '8/21/2019' contained on page at 'SalesforceOrderStartDate'
Then I should see text 'On Hold' contained on page at 'SalesforceStatus'
#Then I should see text 'USD 149.52' contained on page at 'SalesforceOrderTotal'

@SFPrepay
Scenario: TC-06:Validation of Salesforce search product
Then I should see text 'HC00522198' contained on page at 'SalesforceOrderName'
And I wait for '2' seconds
And I click 'SalesforceOrderStatusNumber'
And I wait for '2' seconds
And I refresh the WebPage
Then I should see text 'HC00522198' contained on page at 'SalesforceOrderHeader'
Then I should see text 'Royal Cyber' contained on page at 'SalesforceOrderHeader_AccName'
Then I should see text 'On Hold' contained on page at 'SalesforceOrderHeader_Status'
Then I should see text 'USD 149.52' contained on page at 'SalesforceOrderHeader_OrderTotal'
Then I should see text 'HC00522198' contained on page at 'SalesforceOrderInfo_OrderName'
Then I should see text '8/21/2019' contained on page at 'SalesforceOrderInfo_OrderStartDate'
Then I should see text 'Royal Cyber' contained on page at 'SalesforceOrderInfo_AccountName'
Then I should see text 'On Hold' contained on page at 'SalesforceOrderInfo_Status'
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
Then I should see text 'Customer' contained on page at 'SalesforceOrderInfo_MethodOfEntry'
Then I should see text 'USD 122.00' contained on page at 'SalesforceCostsInfo_Subtotal'
Then I should see text 'USD 15.00' contained on page at 'SalesforceCostsInfo_ShippingAmount'
Then I should see text 'USD 149.52' contained on page at 'SalesforceCostsInfo_OrderTotal'
Then I should see text 'PP' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'PP' contained on page at 'SalesforceBillingInfo_PaymentTerms'
Then I should see text '12345' contained on page at 'SalesforceBillingInfo_PONumber'
Then I should see text 'UPS Ground - $15.00' contained on page at 'SalesforceShippingInfo_ShipVia'

@Ignore
#=====Order Validation Cancel Order=======#
@SFCancel
Scenario: Verify Salesforce search product
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'OrderNumber' from 'ASMWCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 
Then I validate 'OrderNumber' from 'ASMWCC' sheet contained at 'SalesforceOrderName'
Then I get the details 'SalesforceOrderStatusNumber' from 'ASMWCC' and store as 'SalesforceOrderStatusNumber'
Then I validate 'OrderNumber' from 'ASMWCC' sheet contained at 'SalesforceOrderName'
Then I validate 'AccountName' from 'ASMWCC' sheet contained at 'SalesforceAccountName'

@Ignore @SFCancel
Scenario: Validation of Salesforce search product
And I click 'SalesforceOrderStatusNumber'
And I wait for '2' seconds
And I refresh the WebPage

@Ignore @SFCancel
Scenario: Validation of Salesforce search 
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader_AccName' present on page
And I should see element 'SalesforceOrderHeader_Status' present on page
And I should see element 'SalesforceOrderHeader_OrderTotal' present on page
And I should see element 'SalesforceOrderInfo_OrderName' present on page
And I should see element 'SalesforceOrderInfo_OrderStartDate' present on page
And I should see element 'SalesforceOrderInfo_AccountName' present on page
And I should see element 'SalesforceOrderInfo_Status' present on page
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
And I should see element 'SalesforceOrderInfo_MethodOfEntry' present on page
And I scroll to 'coordinates' - '0,1250'
And I should see element 'SalesforceCostsInfo_OrderTotal' present on page
And I scroll to 'coordinates' - '0,300'
And I should see element 'SalesforceBillingInfo_PaymentType' present on page
Then I should see text 'CARD' contained on page at 'SalesforceBillingInfo_PaymentType'
And I should see element 'SalesforceBillingInfo_PaymentTerms' present on page
Then I should see text 'CC' contained on page at 'SalesforceBillingInfo_PaymentTerms'
And I validate 'PONumber' from 'ASMWCC' sheet contained at 'SalesforceBillingInfo_PONumber'
And I validate 'ShippingMethod' from 'ASMWCC' sheet contained at 'SalesforceShippingInfo_ShipVia'

@Ignore @SFCancel
Scenario: Validate order line items
And I refresh the WebPage
And I wait for '5' seconds
And I scroll to 'coordinates' - '0,300'
And I should see element 'SalesforceOrderLine_ProductID' present on page
Then I find Product at 'ItemName' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_ProductID'
And I should see element 'SalesforceOrderLine_Quantity' present on page
Then I find Quantity at 'Quantity' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_Quantity'
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
Then I find Total at 'UnitPrice' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_UnitPrice'
And I should see element 'SalesforceOrderLine_TotalPrice' present on page
Then I find Total at 'TotalPrice' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_TotalPrice'