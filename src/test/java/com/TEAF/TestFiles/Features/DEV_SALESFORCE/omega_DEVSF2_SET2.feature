@AllE2E_SFC @SFC-2
Feature: Omega Salesforce Validation

@SFRegister @SFQuote @SFCPQ @Ignore
Scenario: TC-01:Starting the Salesforce Script
Given My WebApp 'Omega' is open

#===Register User===#
@SFRegister @SFQuote @SFCPQ @Ignore
Scenario: TC-02:Verify Salesforce and login functionality
And I navigate to 'https://omega--omegadev.lightning.force.com' application
And I wait '5' seconds for presence of element 'OmegaSDFC_Logo'
Then I should see element 'OmegaSDFC_Logo' present on page
Then I should see element 'SalesforceEmail' present on page
And I enter 'pkumar@omega.com' in field 'SalesforceEmail' 
And I click 'SalesforceNext'
Then I should see element 'SalesforcePassword' present on page
And I enter 'RComegaQA@123' in field 'SalesforcePassword'
Then I should see element 'SalesforceVerify' present on page
And I click 'SalesforceVerify'
And I wait for '5' seconds

@SFRegister
Scenario: TC-03:Validate Salesforce and Search Registered user and Validate details
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'EmailId' from 'Register' sheet to the field 'SalesforceSearch'
And I press key: enter
And I wait for '15' seconds
Then I should see element 'SalesforceRegisteredName' present on page_
Then I should see element 'SalesforceRegisteredAccountName' present on page_
Then I should see element 'SalesforceRegisteredPhone' present on page_
Then I concat 'FirstName' and 'LastName' from 'Register' sheet is contained at 'SalesforceRegisteredName'
Then I concat 'FirstName' and 'LastName' from 'Register' sheet is contained at 'SalesforceRegisteredAccountName'
Then I validate 'MobileNumber' from 'Register' sheet contained at 'SalesforceRegisteredPhone'

@SFRegister
Scenario: TC-04:Validate Salesforce Registered User
And I click 'SalesforceRegisteredNameLink'
And I wait for '5' seconds
And I should see element 'SalesforceAccountNameHeading' present on page
Then I concat 'FirstName' and 'LastName' from 'Register' sheet is contained at 'SalesforceAccountNameHeading'
And I should see element 'SalesforceContactInfo_AccName' present on page
Then I concat 'FirstName' and 'LastName' from 'Register' sheet is contained at 'SalesforceContactInfo_AccName'
And I should see element 'SalesforceContactInfo_RecType' present on page
Then I validate 'RecordTypeName' from 'Register' sheet contained at 'SalesforceContactInfo_RecType'
And I should see element 'SalesforceContactInfo_UserEmail' present on page
Then I validate 'EmailId' from 'Register' sheet contained at 'SalesforceContactInfo_UserEmail'
And I should see element 'SalesforceContactInfo_Phone' present on page
Then I validate 'MobileNumber' from 'Register' sheet contained at 'SalesforceContactInfo_Phone'
And I should see element 'SalesforceContactInfo_EcommRole' present on page
Then I validate 'Ecommerce_Role' from 'Register' sheet contained at 'SalesforceContactInfo_EcommRole'
And I should see element 'SalesforceContactInfo_RegCreated' present on page
Then I validate 'Country ' from 'Register' sheet contained at 'SalesforceContactInfo_RegCreated'

#======Quote Validation in Salesforce=====#
@SFQuote
Scenario: TC-05:Verify Salesforce Quote search quoted product
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'QuoteNumber' from 'Quotes' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceQuoteNumber' present on page
And I should see element 'SalesforceQuoteContName' present on page
And I should see element 'SalesforceQuoteAccountName' present on page
And I should see element 'SalesforceQuoteStatus' present on page
And I should see element 'SalesforceQuoteStartDate' present on page
And I should see element 'SalesforceQuoteExpireDate' present on page
And I should see element 'SalesforceQuoteSubtotal' present on page
And I should see element 'SalesforceQuoteTotal' present on page

@SFQuote
Scenario: TC-06:Verify search Quote product from given excel data
And I get the details 'SalesforceQuoteNumber' from 'SalesforceQuote' and store as 'QuoteNumber'
And I get the details 'SalesforceQuoteContName' from 'SalesforceQuote' and store as 'QuoteContName'
And I get the details 'SalesforceQuoteAccountName' from 'SalesforceQuote' and store as 'QuoteAccountName'
And I get the details 'SalesforceQuoteStatus' from 'SalesforceQuote' and store as 'QuoteStatus'
And I get the details 'SalesforceQuoteStartDate' from 'SalesforceQuote' and store as 'QuoteStartDate'
And I get the details 'SalesforceQuoteExpireDate' from 'SalesforceQuote' and store as 'QuoteExpireDate'
And I get the details 'SalesforceQuoteSubtotal' from 'SalesforceQuote' and store as 'QuoteSubtotal'
And I get the details 'SalesforceQuoteTotal' from 'SalesforceQuote' and store as 'QuoteTotal'
Then I validate 'QuoteNumber' from 'Quotes' sheet contained at 'SalesforceQuoteNumber'
Then I validate 'QuoteContName' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteContName'
Then I validate 'QuoteAccountName' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteAccountName'
Then I validate 'QuoteStatus' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteStatus'
Then I validate 'QuoteStartDate' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteStartDate'
Then I validate 'QuoteExpireDate' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteExpireDate'
#Then I find Total at 'TotalPrice' from 'Quotes' sheet contained at 'SalesforceTotals_SubTotals'
#Then I find Total at 'TotalPrice' from 'Quotes' sheet contained at 'SalesforceTotals_TotalPrice'

@SFQuote
Scenario: TC-07:Verify salesforce quote product details from given data
And I click 'SalesforceQuoteNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
Then I should see element 'SalesforceOrderHeader' present on page
Then I should see element 'SalesforceOrderHeader_QuoteNumer' present on page
And I should see element 'SalesforceOrderHeader_Syncing' present on page 
Then I should see text 'Quote for Taha Patel' contained on page at 'SalesforceOrderHeader_OpportunityName'
And I should see element 'SalesforceOrderHeader_AccountName' present on page
And I should see element 'SalesforceQuoteInfo_QuoteNumber' present on page
And I should see element 'SalesforceQuoteInfo_Status' present on page
Then I should see text 'Quote for Taha Patel' contained on page at 'SalesforceQuoteInfo_OpportunityName'
Then I should see text '30.00' contained on page at 'SalesforceQuoteInfo_DaysBeforeExpiration'
And I should see element 'SalesforceQuoteInfo_DaysBeforeExpiration' present on page
And I should see element 'SalesforceQuoteInfo_AccountName' present on page
And I should see element 'SalesforceQuoteInfo_QuoteStartDate' present on page
And I should see element 'SalesforceQuoteInfo_Description' present on page
And I should see element 'SalesforceQuoteInfo_ExpirationDate' present on page
And I should see element 'SalesforceQuoteInfo_MethodOfEntry' present on page
And I scroll to 'coordinates' - '0,750'
Then I should see text 'Taha Patel' contained on page at 'SalesforceFor_ContactName'
And I should see element 'SalesforceFor_Phone' present on page
And I should see element 'SalesforceFor_Email' present on page

@SFQuote
Scenario: TC-08:Validation of Salesforce search product from given data
And I validate 'QuoteNumber' from 'Quotes' sheet contained at 'SalesforceOrderHeader'
And I get the details 'SalesforceOrderHeader_QuoteNumer' from 'SalesforceQuote' and store as 'Salesforce_OrderNumber'
And I validate 'Salesforce_OrderNumber' from 'SalesforceQuote' sheet contained at 'SalesforceOrderHeader_QuoteNumer'
Then I validate 'QuoteAccountName' from 'SalesforceQuote' sheet contained at 'SalesforceOrderHeader_AccountName'
Then I validate 'QuoteNumber' from 'Quote' sheet contained at 'SalesforceQuoteInfo_QuoteNumber'
Then I validate 'QuoteStatus' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteInfo_Status'
Then I validate 'QuoteAccountName' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteInfo_AccountName'
Then I validate 'QuoteStartDate' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteInfo_QuoteStartDate'
Then I should see text 'Quote for Taha Patel' contained on page at 'SalesforceQuoteInfo_Description'
Then I validate 'QuoteExpireDate' from 'SalesforceQuote' sheet contained at 'SalesforceQuoteInfo_ExpirationDate'
Then I should see text 'Customer' contained on page at 'SalesforceQuoteInfo_MethodOfEntry'
And I scroll to 'coordinates' - '0,750'
And I validate 'MobileNumber' from 'Register' sheet contained at 'SalesforceFor_Phone'
And I validate 'EmailId' from 'Register' sheet contained at 'SalesforceFor_Email'
#Then I find Total at 'TotalPrice' from 'Quotes' sheet contained at 'SalesforceTotals_SubTotals'
#Then I find Total at 'TotalPrice' from 'Quotes' sheet contained at 'SalesforceTotals_TotalPrice'

#======CPQ order Validation in Salesforce=====#
@SFCPQ
Scenario: TC-09:Verify Salesforce search product for CPQ order
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page	
And I enter 'OrderNumber' from 'ConfigOrder' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I validate 'OrderNumber' from 'ConfigOrder' sheet contained at 'SalesforceOrderName'
And I should see element 'SalesforceOrderStatusNumber' present on page
And I get the details 'SalesforceOrderStatusNumber' from 'CPQSF' and store as 'SalesforceOrderStatusNumber'
And I should see element 'SalesforcePONumber' present on page
And I get the details 'SalesforcePONumber' from 'CPQSF' and store as 'SalesforcePONumber'
And I should see element 'SalesforceAccountName' present on page
And I get the details 'SalesforceAccountName' from 'CPQSF' and store as 'SalesforceAccountName'
And I should see element 'SalesforceContact' present on page
And I get the details 'SalesforceContact' from 'CPQSF' and store as 'SalesforceContact'
And I should see element 'SalesforceOrderStartDate' present on page
And I get the details 'SalesforceOrderStartDate' from 'CPQSF' and store as 'SalesforceOrderStartDate'
And I should see element 'SalesforceStatus' present on page
And I get the details 'SalesforceStatus' from 'CPQSF' and store as 'SalesforceStatus'
And I should see element 'SalesforceOrderTotal' present on page 
And I get the details 'SalesforceOrderTotal' from 'CPQSF' and store as 'SalesforceOrderTotal'

@SFCPQ
Scenario: TC-10:Validate the stored values
And I should see element 'SalesforceOrderName' present on page
And I validate 'SalesforceOrderName' from 'CPQSF' sheet contained at 'SalesforceOrderName'
And I should see element 'SalesforcePONumber' present on page
And I validate 'SalesforcePONumber' from 'CPQSF' sheet contained at 'SalesforcePONumber'
And I should see element 'SalesforceAccountName' present on page
And I validate 'SalesforceAccountName' from 'CPQSF' sheet contained at 'SalesforceAccountName'
And I should see element 'SalesforceContact' present on page
And I validate 'SalesforceContact' from 'CPQSF' sheet contained at 'SalesforceContact'
And I should see element 'SalesforceOrderStartDate' present on page
And I validate 'SalesforceOrderStartDate' from 'CPQSF' sheet contained at 'SalesforceOrderStartDate'
And I should see element 'SalesforceStatus' present on page
And I validate 'SalesforceStatus' from 'CPQSF' sheet contained at 'SalesforceStatus'
And I should see element 'SalesforceOrderTotal' present on page
And I validate 'SalesforceOrderTotal' from 'CPQSF' sheet contained at 'SalesforceOrderTotal'

@SFCPQ
Scenario: TC-11:Validation of Salesforce search product
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
Then I validate 'OrderNumber' from 'ConfigOrder' sheet contained at 'SalesforceOrderHeader'
Then I validate 'SalesforceStatus' from 'CPQSF' sheet contained at 'SalesforceOrderHeader_Status'
#Then I find Total from 'ConfigOrder' sheet contained at 'SalesforceOrderHeader_OrderTotal'
And I validate 'SalesforceOrderTotal' from 'SalesforceOrderTotal' sheet contained at 'SalesforceOrderHeader_OrderTotal'
Then I validate 'OrderNumber' from 'ConfigOrder' sheet contained at 'SalesforceOrderInfo_OrderName'
Then I validate 'SalesforceOrderStartDate' from 'CPQSF' sheet contained at 'SalesforceOrderInfo_OrderStartDate'
Then I validate 'SalesforceAccountName' from 'CPQSF' sheet contained at 'SalesforceOrderInfo_AccountName'
Then I validate 'SalesforceStatus' from 'CPQSF' sheet contained at 'SalesforceOrderInfo_Status'
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
Then I should see text 'Customer' contained on page at 'SalesforceOrderInfo_MethodOfEntry'
Then I validate 'SalesforceOrderTotal' from 'CPQSF' sheet contained at 'SalesforceCostsInfo_OrderTotal'
Then I should see text 'CARD' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'CC' contained on page at 'SalesforceBillingInfo_PaymentTerms'
Then I should see text '123ew' contained on page at 'SalesforceBillingInfo_PONumber'
Then I should see text 'UPS 3 Day Select - $24.00' contained on page at 'SalesforceShippingInfo_ShipVia'

@SFCPQ
Scenario: TC-12:Validate order line items
And I wait for '2' seconds
And I should see element 'SalesforceOrderLine_ProductID' present on page
And I should see element 'SalesforceOrderLine_Quantity' present on page
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
And I should see element 'SalesforceOrderLine_TotalPrice' present on page
And I should see element 'SalesforceOrderLine_ProductID2' present on page
And I should see element 'SalesforceOrderLine_Quantity2' present on page
And I should see element 'SalesforceOrderLine_UnitPrice2' present on page