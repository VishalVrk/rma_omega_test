@DEVSYTE
Feature: Syteline functionality Test Automation scenarios

@AccountPayment @CorporateCC @WebCustomerCC @PrepayOrder
Scenario: wait for 25 mins to reflect changes in integration system
Given My WindowsApp 'Syteline' is open


#======Syteline AccountPayment=======#
@AccountPayment  @CorporateCC @WebCustomerCC @PrepayOrder
Scenario: Verify the syteline desktop application login functinality
And I wait for '15' seconds
When I enter 'tpatel' in field 'UserName' of window
And I enter 'dev1234' in field 'PassWord' of window
And I enter 'dCT01' in field 'ConfigurationFeild' of window
And I click 'Okbutton' button
And I wait for '25' seconds
And I click 'WindowMaximize' button

@AccountPayment
Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end AccountPayment
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button
And I enter Order Number 'OrderNumber' from 'AccountPayment' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button

@AccountPayment
Scenario: validate all the stored order details from Front end AccountPayment
And I should see 'PONumber' from 'AccountPayment' sheet present on window at 'CustomerPONumber'
And I should see 'CustomerNumber' from 'AccountPayment' sheet present on window at 'CustomerType'
And I find ShipTo 'SalesforceShippingInfo_ShipTo' from 'AccountPayment' sheet present on window at 'ShipToCustomerOrderWindow'
And I should see 'TermPayment' from 'AccountPayment' sheet present on window at 'TermsPayment'
And I should see 'ShippingCode' from 'AccountPayment' sheet present on window at 'ShipViaCode'
And I should see 'ShippingDes' from 'AccountPayment' sheet present on window at 'ShipingCodeDescription'
And I should see 'CustomerNumber' from 'AccountPayment' sheet present on window at 'CustomerNumberAddressWindow'
And I should see 'ShippingTerms' from 'AccountPayment' sheet present on window at 'ShippingTerms'

@AccountPayment
Scenario: Verify the contact window AccountPayment
And I click 'ContactWindow' button
And I should see 'ContactNameContactName' from 'AccountPayment' sheet present on window at 'ContactNameContactName'
And I should see 'ContactEmailContactWindow' from 'AccountPayment' sheet present on window at 'ContactEmailContactWindow'

@AccountPayment
Scenario: Verify the Line order items for the placed order AccountPayment
And I click 'LinesMenu' button
And I wait for '40' seconds
And I should see text contained 'Corporate HQ' present on window at 'LinesName'
And I find item 'ItemName' from 'AccountPayment' sheet present on window at 'LineItemSkuId'
And I should see text contained '1' present on window at 'LineNumber'
And I find UnitPrice 'UnitPrice' from 'AccountPayment' sheet present on window at 'LineUnitPrice'
And I find NetPrice 'TotalPrice' from 'AccountPayment' sheet present on window at 'LineNetPrice'
And I find Quantity 'Quantity' from 'AccountPayment' sheet present on window at 'LineQtyOrdered'
And I should see 'SytelineStatus' from 'AccountPayment' sheet present on window at 'LineStatus'


#======Syteline CorporateCC=======#
 @CorporateCC
Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end CorporateCC
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button
And I enter Order Number 'OrderNumber' from 'CorporateCC' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button

 @CorporateCC
Scenario: validate all the stored order details from Front end CorporateCC
And I should see 'PONumber' from 'CorporateCC' sheet present on window at 'CustomerPONumber'
And I should see 'CustomerNumber' from 'CorporateCC' sheet present on window at 'CustomerType'
And I find ShipTo 'SalesforceShippingInfo_ShipTo' from 'CorporateCC' sheet present on window at 'ShipToCustomerOrderWindow'
And I should see 'TermPayment' from 'CorporateCC' sheet present on window at 'TermsPayment'
And I should see 'ShippingCode' from 'CorporateCC' sheet present on window at 'ShipViaCode'
And I should see 'ShippingDes' from 'CorporateCC' sheet present on window at 'ShipingCodeDescription'
And I should see 'CustomerNumber' from 'CorporateCC' sheet present on window at 'CustomerNumberAddressWindow'
And I should see 'ShippingTerms' from 'CorporateCC' sheet present on window at 'ShippingTerms'

@CorporateCC
Scenario: Verify the contact window 
And I click 'ContactWindow' button
And I should see 'ContactNameContactName' from 'CorporateCC' sheet present on window at 'ContactNameContactName'
And I should see 'ContactEmailContactWindow' from 'CorporateCC' sheet present on window at 'ContactEmailContactWindow'

@CorporateCC
Scenario: Verify the Line order items for the placed order CorporateCC
And I click 'LinesMenu' button
And I wait for '10' seconds
And I should see text contained 'Corporate HQ' present on window at 'LinesName'
And I find item 'ItemName' from 'CorporateCC' sheet present on window at 'LineItemSkuId'
And I should see text contained '1' present on window at 'LineNumber'
And I find UnitPrice 'UnitPrice' from 'CorporateCC' sheet present on window at 'LineUnitPrice'
And I find NetPrice 'TotalPrice' from 'CorporateCC' sheet present on window at 'LineNetPrice'
And I find Quantity 'Quantity' from 'CorporateCC' sheet present on window at 'LineQtyOrdered'
And I should see 'SytelineStatus' from 'CorporateCC' sheet present on window at 'LineStatus'


#======Syteline WebCustomerCC=======#
@WebCustomerCC
Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end WebCustomerCC
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button
And I enter Order Number 'OrderNumber' from 'WebCustomerCC' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button

@WebCustomerCC
Scenario: validate all the stored order details from Front end WebCustomerCC
And I should see 'PONumber' from 'WebCustomerCC' sheet present on window at 'CustomerPONumber'
And I should see 'CustomerNumber' from 'WebCustomerCC' sheet present on window at 'CustomerType'
And I find ShipTo 'SalesforceShippingInfo_ShipTo' from 'WebCustomerCC' sheet present on window at 'ShipToCustomerOrderWindow'
And I should see 'TermPayment' from 'WebCustomerCC' sheet present on window at 'TermsPayment'
And I should see 'ShippingCode' from 'WebCustomerCC' sheet present on window at 'ShipViaCode'
And I should see 'ShippingDes' from 'WebCustomerCC' sheet present on window at 'ShipingCodeDescription'
And I should see 'CustomerNumber' from 'WebCustomerCC' sheet present on window at 'CustomerNumberAddressWindow'
And I should see 'ShippingTerms' from 'WebCustomerCC' sheet present on window at 'ShippingTerms'

@WebCustomerCC
Scenario: Verify the contact window WebCustomerCC
And I click 'ContactWindow' button
And I should see 'ContactNameContactName' from 'WebCustomerCC' sheet present on window at 'ContactNameContactName'
And I should see 'ContactEmailContactWindow' from 'WebCustomerCC' sheet present on window at 'ContactEmailContactWindow'

@WebCustomerCC
Scenario: Verify the Line order items for the placed order WebCustomerCC
And I click 'LinesMenu' button
And I wait for '10' seconds
And I should see text contained 'Corporate HQ' present on window at 'LinesName'
And I find item 'ItemName' from 'WebCustomerCC' sheet present on window at 'LineItemSkuId'
And I should see text contained '1' present on window at 'LineNumber'
And I find UnitPrice 'UnitPrice' from 'WebCustomerCC' sheet present on window at 'LineUnitPrice'
And I find NetPrice 'TotalPrice' from 'WebCustomerCC' sheet present on window at 'LineNetPrice'
And I find Quantity 'Quantity' from 'WebCustomerCC' sheet present on window at 'LineQtyOrdered'
And I should see 'SytelineStatus' from 'WebCustomerCC' sheet present on window at 'LineStatus'


#======Syteline PrepayOrder=======#
@PrepayOrder
Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end PrepayOrder
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button
And I enter Order Number 'OrderNumber' from 'PrepayOrder' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button

Scenario: validate all the stored order details from Front end PrepayOrder
And I should see 'PONumber' from 'PrepayOrder' sheet present on window at 'CustomerPONumber'
And I should see 'CustomerNumber' from 'PrepayOrder' sheet present on window at 'CustomerType'
And I find ShipTo 'SalesforceShippingInfo_ShipTo' from 'PrepayOrder' sheet present on window at 'ShipToCustomerOrderWindow'
And I should see 'TermPayment' from 'PrepayOrder' sheet present on window at 'TermsPayment'
And I should see 'ShippingCode' from 'PrepayOrder' sheet present on window at 'ShipViaCode'
And I should see 'ShippingDes' from 'PrepayOrder' sheet present on window at 'ShipingCodeDescription'
And I should see 'CustomerNumber' from 'PrepayOrder' sheet present on window at 'CustomerNumberAddressWindow'
And I should see 'ShippingTerms' from 'PrepayOrder' sheet present on window at 'ShippingTerms'

Scenario: Verify the contact window PrepayOrder
And I click 'ContactWindow' button
And I should see 'ContactNameContactName' from 'PrepayOrder' sheet present on window at 'ContactNameContactName'
And I should see 'ContactEmailContactWindow' from 'PrepayOrder' sheet present on window at 'ContactEmailContactWindow'

Scenario: Verify the Line order items for the placed order PrepayOrder
And I click 'LinesMenu' button
And I wait for '10' seconds
And I should see text contained 'US Corporate Account Example' present on window at 'LinesName'
And I find item 'ItemName' from 'PrepayOrder' sheet present on window at 'LineItemSkuId'
And I should see text contained '1' present on window at 'LineNumber'
And I find UnitPrice 'UnitPrice' from 'PrepayOrder' sheet present on window at 'LineUnitPrice'
And I find NetPrice 'TotalPrice' from 'PrepayOrder' sheet present on window at 'LineNetPrice'
And I find Quantity 'Quantity' from 'PrepayOrder' sheet present on window at 'LineQtyOrdered'
And I should see 'SytelineStatus' from 'PrepayOrder' sheet present on window at 'LineStatus'

#======Syteline PrepayOrder=======#
Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end PrepayOrder
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button
And I enter Order Number 'OrderNumber' from 'PrepayOrder' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button

Scenario: validate all the stored order details from Front end PrepayOrder
And I should see 'PONumber' from 'PrepayOrder' sheet present on window at 'CustomerPONumber'
And I should see 'CustomerNumber' from 'PrepayOrder' sheet present on window at 'CustomerType'
And I find ShipTo 'SalesforceShippingInfo_ShipTo' from 'PrepayOrder' sheet present on window at 'ShipToCustomerOrderWindow'
And I should see 'TermPayment' from 'PrepayOrder' sheet present on window at 'TermsPayment'
And I should see 'ShippingCode' from 'PrepayOrder' sheet present on window at 'ShipViaCode'
And I should see 'ShippingDes' from 'PrepayOrder' sheet present on window at 'ShipingCodeDescription'
And I should see 'CustomerNumber' from 'PrepayOrder' sheet present on window at 'CustomerNumberAddressWindow'
And I should see 'ShippingTerms' from 'PrepayOrder' sheet present on window at 'ShippingTerms'

Scenario: Verify the contact window PrepayOrder
And I click 'ContactWindow' button
And I should see 'ContactNameContactName' from 'PrepayOrder' sheet present on window at 'ContactNameContactName'
And I should see 'ContactEmailContactWindow' from 'PrepayOrder' sheet present on window at 'ContactEmailContactWindow'

Scenario: Verify the Line order items for the placed order PrepayOrder
And I click 'LinesMenu' button
And I wait for '10' seconds
And I should see text contained 'US Corporate Account Example' present on window at 'LinesName'
And I find item 'ItemName' from 'PrepayOrder' sheet present on window at 'LineItemSkuId'
And I should see text contained '1' present on window at 'LineNumber'
And I find UnitPrice 'UnitPrice' from 'PrepayOrder' sheet present on window at 'LineUnitPrice'
And I find NetPrice 'TotalPrice' from 'PrepayOrder' sheet present on window at 'LineNetPrice'
And I find Quantity 'Quantity' from 'PrepayOrder' sheet present on window at 'LineQtyOrdered'
And I should see 'SytelineStatus' from 'PrepayOrder' sheet present on window at 'LineStatus'

#======Syteline ConfigOrder=======#
Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end ConfigOrder
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button
And I enter Order Number 'OrderNumber' from 'ConfigOrder' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button

Scenario: validate all the stored order details from Front end ConfigOrder
And I should see 'PONumber' from 'ConfigOrder' sheet present on window at 'CustomerPONumber'
And I should see 'CustomerNumber' from 'ConfigOrder' sheet present on window at 'CustomerType'
And I find ShipTo 'SalesforceShippingInfo_ShipTo' from 'ConfigOrder' sheet present on window at 'ShipToCustomerOrderWindow'
And I should see 'TermPayment' from 'ConfigOrder' sheet present on window at 'TermsPayment'
And I should see 'ShippingCode' from 'ConfigOrder' sheet present on window at 'ShipViaCode'
And I should see 'ShippingDes' from 'ConfigOrder' sheet present on window at 'ShipingCodeDescription'
And I should see 'CustomerNumber' from 'ConfigOrder' sheet present on window at 'CustomerNumberAddressWindow'
And I should see 'ShippingTerms' from 'ConfigOrder' sheet present on window at 'ShippingTerms'

Scenario: Verify the contact window ConfigOrder
And I click 'ContactWindow' button
And I should see 'ContactNameContactName' from 'ConfigOrder' sheet present on window at 'ContactNameContactName'
And I should see 'ContactEmailContactWindow' from 'ConfigOrder' sheet present on window at 'ContactEmailContactWindow'

Scenario: Verify the Line order items for the placed order ConfigOrder
And I click 'LinesMenu' button
And I wait for '10' seconds
And I should see text contained 'US Corporate Account Example' present on window at 'LinesName'
And I find item 'ItemName' from 'ConfigOrder' sheet present on window at 'LineItemSkuId'
And I should see text contained '1' present on window at 'LineNumber'
And I find UnitPrice 'UnitPrice' from 'ConfigOrder' sheet present on window at 'LineUnitPrice'
And I find NetPrice 'TotalPrice' from 'ConfigOrder' sheet present on window at 'LineNetPrice'
And I find Quantity 'Quantity' from 'ConfigOrder' sheet present on window at 'LineQtyOrdered'
And I should see 'SytelineStatus' from 'ConfigOrder' sheet present on window at 'LineStatus'