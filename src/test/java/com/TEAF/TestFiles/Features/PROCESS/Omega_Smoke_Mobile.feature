@testing3121
Feature: Omega End to end smoke test script

#==================================== HomePage contents ================================
Scenario: TC -1 : Validate the homepage contents
Given My WebApp 'Omega' is open
And I resize the screen dimension to '360' width and '960' height
Then I should see element 'OmegaLogo' present on page
And I should see element 'searchButton' present on page
And I should see element 'SearchField' present on page
And I should see element 'HamburgerMenu' present on page
#And I get all element locators under 'class' : 'header-ctas' on to file name: 'OmegaHeader'
#And I scroll to 'coordinates' - '0,300'

Scenario: TC -2 Register User
And I click 'OmegaLogo'
And I should see element 'HamburgerMenu' present on page
And I click 'HamburgerMenu'
And I scroll to 'coordinates' - '0,300'
And I click 'NavBar_Register'
Then I should see element 'First_Name_Field' present on page
Then I should see element 'Last_Name_Field' present on page
Then I should see element 'Email_ID_Field' present on page
Then I should see element 'Phone_Num_Field' present on page
Then I should see element 'Phone_Ext_Field' present on page
Then I should see element 'Reg_Password_Field' present on page
Then I should see element 'Register_Btn' present on page
When  I clear field 'First_Name_Field'
And I enter 'Taha' in field 'First_Name_Field'
And I clear field 'Last_Name_Field'
And I enter 'Patel' in field 'Last_Name_Field'
And I register 'Email_ID_Field' 
And I enter '123456789' in field 'Phone_Num_Field'
And I enter 'Omega2003@' in field 'Reg_Password_Field'
And I click 'Register_Btn'
Then I should see element 'Acknowledgement' present on page

Scenario: TC -3: Search with valid product name
When I enter 'Sensors' in field 'SearchField'
And I click 'searchButton'
And I click 'Product'
And I click 'AddToCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'

Scenario: TC -4: Add a product to cart From PDP Page
When I enter 'T-FER-1/16' in field 'Search_Testfeild'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'

#================================Checkout using Credit card ===========================
Scenario Outline: TC -5: Verify user is able to check out credit card
When I click 'Checkout'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
And I select option 'United States' in dropdown 'Country_Select' by 'text'
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
#And I click 'SaveShippingAddress'
And I click 'Next_CartPage'
Then I should see element 'VerifyAddress_PopUp' present on page
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'UseBillingAddressCheckBox'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I enter 'Test' in field 'DeliveryAttention'
And I select option '2' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '15' seconds
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |