@OmegaL2
Feature: Omega Cignity Intergration Scenarios

Scenario: wait for 15 mins to reflect changes in integration system
Given I wait for 15 mins to reflect changes in integration system
Given My WebApp 'Omega' is open

@runSalesforce12 @232321 @salesforceAutomationGet
Scenario: Validate Salesforce and login
And I navigate to 'https://omega--omegadev.lightning.force.com' application
And I wait '5' seconds for presence of element 'OmegaSDFC_Logo'
Then I should see element 'OmegaSDFC_Logo' present on page
Then I should see element 'SalesforceEmail' present on page
And I enter 'pkumar@omega.com' in field 'SalesforceEmail' 
And I click 'SalesforceNext'
Then I should see element 'SalesforcePassword' present on page
And I enter 'RComegaQA@123' in field 'SalesforcePassword'
Then I should see element 'SalesforceVerify' present on page
And I click 'SalesforceVerify'
And I wait for '5' seconds

@runSalesforce12 @232321
#=====Order Validation Account Payment Corporate Customer=======#
Scenario: Order Validation in Salesforce for Account Payment Corporate Customer search for the product and validate the values
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'OrderNumber' from 'AccountPayment' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 


Scenario: Order Validation in Salesforce for Account Payment Corporate Customer in Excel sheet
Then I validate 'OrderNumber' from 'AccountPayment' sheet contained at 'SalesforceOrderName'
And I get the details 'SalesforceOrderStatusNumber' from 'AccountPayment' and store as 'SalesforceOrderStatusNumber'
Then I validate 'PONumber' from 'AccountPayment' sheet contained at 'SalesforcePONumber'
Then I validate 'AccountName' from 'AccountPayment' sheet contained at 'SalesforceAccountName'
Then I validate 'OrderPlacedBy' from 'AccountPayment' sheet contained at 'SalesforceContact'
And I get the details 'SalesforceOrderStartDate' from 'AccountPayment' and store as 'SalesforceOrderStartDate'
And I get the details 'SalesforceStatus' from 'AccountPayment' and store as 'SalesforceStatus'
And I find Total at 'OrderTotal' from 'AccountPayment' sheet contained at 'SalesforceOrderTotal'

Scenario: Order Validation in Salesforce for Account Payment Corporate Customer where we validate header
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
Then I should see element 'SalesforceOrderHeader' present on page
Then I should see element 'SalesforceOrderHeader_AccName' present on page
Then I should see element 'SalesforceOrderHeader_Status' present on page
Then I should see element 'SalesforceOrderHeader_OrderTotal' present on page

Scenario: Order Validation in Salesforce for Account Payment Corporate Customer where we validate header from excel
Then I validate 'OrderNumber' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader'
Then I validate 'AccountName' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader_AccName'
Then I validate 'SalesforceStatus' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader_Status'
Then I find Total at 'OrderTotal' from 'AccountPayment' sheet contained at 'SalesforceOrderHeader_OrderTotal'


@runSalesforce12 @232321
Scenario: Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info
Then I should see element 'SalesforceOrderInfo_OrderName' present on page
Then I should see element 'SalesforceOrderInfo_OrderStartDate' present on page
Then I should see element 'SalesforceOrderInfo_AccountName' present on page
Then I should see element 'SalesforceOrderInfo_Status' present on page
Then I should see element 'SalesforceOrderInfo_OrderSource' present on page
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
Then I should see text 'Customer' contained on page at 'SalesforceOrderInfo_MethodOfEntry'

Scenario: Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info from excel sheet
Then I validate 'OrderNumber' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_OrderName'
Then I validate 'SalesforceOrderStartDate' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_OrderStartDate'
Then I validate 'AccountName' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_AccountName'
Then I validate 'SalesforceStatus' from 'AccountPayment' sheet contained at 'SalesforceOrderInfo_Status'

@232321
Scenario: Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info
And I scroll to 'coordinates' - '0,700'
Then I should see element 'SalesforceCostsInfo_Subtotal' present on page
Then I should see element 'SalesforceCostsInfo_ShippingAmount' present on page
Then I should see element 'SalesforceCostsInfo_DiscountPercentage' present on page
Then I should see element 'SalesforceCostsInfo_DiscountAmount' present on page
Then I should see element 'SalesforceCostsInfo_OrderTotal' present on page

Scenario: Order Validation in Salesforce for Account Payment Corporate Customer SalesforceOrder Info from Excel sheet
Then I should see element 'SalesforceCostsInfo_Subtotal' present on page
Then I should see element 'SalesforceCostsInfo_ShippingAmount' present on page
Then I should see element 'SalesforceCostsInfo_DiscountPercentage' present on page
Then I should see element 'SalesforceCostsInfo_DiscountAmount' present on page
Then I should see element 'SalesforceCostsInfo_OrderTotal' present on page

@232321
Scenario: order validation in salesforce for Account Payment Corporate Customer Billing Information
And I scroll to 'coordinates' - '0,400'
Then I should see element 'SalesforceBillingInfo_SytelineBillTo' present on page
Then I should see text 'ACCOUNT' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'N30' contained on page at 'SalesforceBillingInfo_PaymentTerms'
Then I should see element 'SalesforceBillingInfo_PONumber' present on page
Then I should see element 'SalesforceShippingInfo_ShipVia' present on page


Scenario: order validation in salesforce for Account Payment Corporate Customer Billing Information from excel sheet
Then I should see text 'C100117-0' contained on page at 'SalesforceBillingInfo_SytelineBillTo'
Then I should see text 'ACCOUNT' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'N30' contained on page at 'SalesforceBillingInfo_PaymentTerms'
Then I validate 'PONumber' from 'AccountPayment' sheet contained at 'SalesforceBillingInfo_PONumber'
Then I validate 'ShippingMethod' from 'AccountPayment' sheet contained at 'SalesforceShippingInfo_ShipVia'


Scenario: order validation in salesforce for Account Payment Corporate Customer Shipping Information
And I should see element 'SalesforceShippingInfo_ShipTo' present on page
And I get the details 'SalesforceShippingInfo_ShipTo' from 'AccountPayment' and store as 'SalesforceShippingInfo_ShipTo'
And I should see element 'SalesforceShippingInfo_ShipVia' present on page
Then I should see text 'UPS Ground - $15.00' contained on page at 'SalesforceShippingInfo_ShipVia'


@runSalesforce12 @232321
Scenario: Order Validation in Salesforce for Account Payment Corporate Customer validate order line items
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderLine_ProductID' present on page
Then I find Product at 'ItemName' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_ProductID'
And I should see element 'SalesforceOrderLine_Quantity' present on page
Then I find Quantity at 'Quantity' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_Quantity'
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
Then I find Total at 'UnitPrice' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_UnitPrice'
And I should see element 'SalesforceOrderLine_TotalPrice' present on page
Then I find Total at 'TotalPrice' from 'AccountPayment' sheet contained at 'SalesforceOrderLine_TotalPrice'