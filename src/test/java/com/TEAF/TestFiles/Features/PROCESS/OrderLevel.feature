Feature: Omega Order level Discount

Scenario: Omega Order level Discount way - 1
Given My WebApp 'Omega' is open 
Then I should see element 'OmegaLogo' present on page
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'SignIn_Btn' present on page
Then I should see element 'Register_1' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page_
Then I should see element 'Banner' present on page_
Then I should see element 'QuickOrderQuote' present on page_
Then I should see element 'WhatsNewSection' present on page_

Scenario: Verify Log into My Account Corporate User
When I click 'OmegaLogo'
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'taha.patel92@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'  
And I click 'LogIn_Btn'
Then I should see text 'Welcome Taha Patel' present on page at 'Welcome'

Scenario: Omega Order level Discount way - 1
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'VolumeDiscounts'
Then I should see element 'cboxContent' present on page
And I get the details 'VolumeDiscountPrice' from 'OrderLevelDiscount' and store as 'VolumeDiscountPrice'
And I get the details 'VolumeDiscountQuantity' from 'OrderLevelDiscount' and store as 'VolumeDiscountQuantity'
And I click 'cboxClose'

Scenario: Omega Order level Discount way - 1
And I clear field 'pdpAddtoCartInput'
And I enter '11' in field 'pdpAddtoCartInput'
And I click 'AddToCart'
And I wait for '5' seconds
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
Then I should see element 'Checkout' present on page
And I should see element 'Price_reflects' present on page
And I should see element 'removeItemsCart' present on page
And I click 'removeItemsCart'

Scenario: Omega Order level Discount way - 2
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I scroll to 'coordinates' - '0,750'
And I click 'trT-FER-3-8'
And I should see element 'VolumeTableBox' present on page
And I clear field 'qtyInputTable'
And I enter '1' in field 'qtyInputTable'
And I click 'TableAddtoCartButton'
And I wait for '5' seconds
When I click 'CheckoutBtnCartPopUp'
Then I should see element 'Checkout' present on page
And I should see element 'Price_reflects' present on page
And I should see element 'removeItemsCart' present on page
And I click 'removeItemsCart'

Scenario: Omega Order level Discount way - 3
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter '4001AJC' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
Then I clear field 'QuickOrder_Qtyfield1'
And I enter '11' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I wait for '2' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
And I should see element 'Price_reflects' present on page
And I should see element 'removeItemsCart' present on page
And I click 'removeItemsCart'

Scenario: Omega Order level Discount way - 4
And I click 'OmegaLogo'
Then I should see element 'QuickOrder_Header' present on page
And I click 'QuickOrder_Header' 
Then I should see element 'SKU_field' present on page
When I enter 'T-FER-1/16' in field 'SKU_field'
And I press enter key
And I clear the text and enter '11' in field 'QuickOrderQuoteUpdateQty'
And I click 'QuickOrderQuote_AddtoCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I wait for '2' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
And I should see element 'Price_reflects' present on page
And I should see element 'removeItemsCart' present on page
And I click 'removeItemsCart'
