@123456RMA
Feature: Omega Order Shippment in Syteline

Scenario: Verify the syteline desktop application login functinality
#Given I wait for 15 mins to reflect changes in integration system
Given My WindowsApp 'Syteline' is open
And I wait for '15' seconds
When I enter 'tpatel' in field 'UserName' of window
And I enter 'dev1234' in field 'PassWord' of window
And I enter 'dCT01' in field 'ConfigurationFeild' of window
And I click 'Okbutton' button
And I wait for '25' seconds
And I click 'WindowMaximize' button

Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end
And I wait for '15' seconds
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button

Scenario: filter customer orders validate all the stored order details
And I enter Order Number 'OrderNumber' from 'AccountPayment' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button
And I click 'ClearXDEXEdit' button
And I click 'CCGRecheckCompliance' button
And I wait for '5' seconds
And I click 'Reval_okBtn' button
And I wait for '5' seconds

Scenario: Verify the Line order
And I click 'LinesMenu' button
And I store 'DueDate' from 'AccountPayment' sheet at 'OrderLine_DueDateEdit' Pane
And I store 'RequestDate' from 'AccountPayment' sheet at 'OrderLine_DueDateEdit' Pane

Scenario: Verify user open form and filter Shipping Workbench
And I click 'OpenForm' button
And I enter 'Shipping Workbench' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'ShippingWorkBench' button
And I click 'Okbtn_Filter' button
And I enter date 'DueDate' from 'AccountPayment' sheet at the 'ShippingDueDate' Pane
And I enter date 'RequestDate' from 'AccountPayment' sheet at the 'ShippingRequestDate' Pane
And I enter 'CustomerNumber' from 'AccountPayment' sheet at the 'CustomerID' Pane using actions
And I wait for '2' seconds
And I enter 'CustomerNumber' in field from 'AccountPayment' sheet at 'CustomerID' of window
And I enter 'OrderNumber' from 'AccountPayment' sheet at the 'FromOrder' Pane using actions
And I wait for '2' seconds
And I enter 'OrderNumber' in field from 'AccountPayment' sheet at 'FromOrder' of window
And I enter 'OrderNumber' from 'AccountPayment' sheet at the 'ToOrder' Pane using actions
And I wait for '2' seconds
And I enter 'OrderNumber' in field from 'AccountPayment' sheet at 'ToOrder' of window
And I click 'RegenrationBtn' button
And I wait for '10' seconds
And I click 'ConfirmPrint' button
And I click 'ConfirmPrint_okBtn' button

Scenario: Verify user open form and filter Shipment Master
And I click 'OpenForm' button
And I enter 'Shipment Master' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'ShipmentMaster' button
And I click 'Okbtn_Filter' button
And I enter 'test123' from the 'TrackingNumber' Pane using actions
And I enter 'test123' in field 'TrackingNumber' of window
And I click 'ShipButton' button
And I click 'ShipButton_okBtn' button

Scenario: Verify user open form and filter Order Invoicing/Credit Memo
And I click 'OpenForm' button
And I enter 'Order Invoicing/Credit Memo' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'OrderInvoicing' button
And I click 'Okbtn_Filter' button
#And I enter date 'InvoiceDate' from 'AccountPayment' sheet at the 'InvoiceDate' Pane
And I enter 'CustomerNumber' from 'AccountPayment' sheet at the 'StartCustomer' Pane using actions
And I enter 'CustomerNumber' in field from 'AccountPayment' sheet at 'StartCustomer' of window
And I enter 'CustomerNumber' from 'AccountPayment' sheet at the 'EndCustomer' Pane using actions
And I enter 'CustomerNumber' in field from 'AccountPayment' sheet at 'EndCustomer' of window
And I click 'ProcessButton' button
And I wait for '10' seconds
And I click 'InvoiceDate_okBtn' button
And I wait for '100' seconds
And I wait for visibility of element 'Report_okBtn'
And I click 'Report_okBtn' button

Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button

Scenario: filter customer orders validate all the stored order details
And I enter Order Number 'OrderNumber' from 'AccountPayment' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button
And I wait for '5' seconds
And I click 'LinesMenu' button
And I clear 'Status_LineOrder' field Pane
And I enter 'Complete' from the 'Status_LineOrder' Pane
And I click 'Save_Button' button
And I wait for '30' seconds
Then I should see element 'Status_okBtn' present on Window
And I click 'Status_okBtn' button