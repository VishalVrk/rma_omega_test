@Rough
Feature: e2e


@Ignore
Scenario: TC-02: Verify Log into My Account Corporate User
Given My WebApp 'Omega' is open
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
#And I enter 'taha.patel98@royalcyber.com' in field 'SignID_Field'
And I enter 'taha.patel92@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'  
And I click 'LogIn_Btn'
Then I should see text 'Welcome Taha Patel' present on page at 'Welcome'


@Ignore
Scenario: TC-06 Verify product search functionality - Quick Order Search through way 1
And I click 'OmegaLogo'
Then I should see element 'QuickOrder_Header' present on page
And I click 'QuickOrder_Header' 
Then I should see element 'SKU_field' present on page
When I enter 'T-FER-1/16' in field 'SKU_field'
And I press enter key
And I clear the text and enter '2' in field 'QuickOrderQuoteUpdateQty'
And I click 'QuickOrderQuote_AddtoCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'


@Ignore
Scenario: TC-07 Verify product search functionality - Quick Order Search through way 2
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter '4001AJC' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '1' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'


@Ignore
Scenario Outline:Verify place order functionality for Corporate customer using Credit Card Payment method and validate Order details in SFDC & Syteline
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I wait for '2' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I get the details 'Customer_number' from 'CorporateCC' and store as 'CustomerNumber'
Then I click 'Next_CartPage'
And I get the details 'PONumber' from 'CorporateCC' and store as 'PONumber'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
#And I click 'SaveShippingAddress'
And I click 'Next_CartPage'
Then I should see element 'VerifyAddress_PopUp' present on page
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
#add one step
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '15' seconds
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |



@Ignore
Scenario: TC-09: Validate order details and store in excel sheet
And I should see element 'OrderNumber' present on page
And I should see element 'OrderPlacedBy' present on page
And I should see element 'OrderStatus' present on page
And I should see element 'DatePlaced' present on page
And I should see element 'Total' present on page
And I should see element 'ShipTo' present on page
And I should see element 'ShippingMethod' present on page


@Ignore
Scenario: TC-09: Validate order details and store in excel sheet
And I get the details 'OrderNumber' from 'CorporateCC' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'CorporateCC' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'CorporateCC' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'CorporateCC' and store as 'DatePlaced'
And I get the details 'Total' from 'CorporateCC' and store as 'Total'
And I get the details 'ShipTo' from 'CorporateCC' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'CorporateCC' and store as 'ShippingMethod'


@Ignore
Scenario: TC-09: Validate order details and store in excel sheet
And I should see element 'ProductName1' present on page
And I should see element 'ItemName1' present on page
And I should see element 'UnitPrice1' present on page
And I should see element 'Quantity1' present on page
And I should see element 'TotalPrice1' present on page
And I should see element 'ProductName2' present on page
And I should see element 'ItemName2' present on page
And I should see element 'UnitPrice2' present on page
And I should see element 'Quantity2' present on page
And I should see element 'TotalPrice2' present on page
And I should see element 'BillingAddress' present on page
And I should see element 'CCPaymentType' present on page
And I should see element 'OrderTotal' present on page

@Ignore
Scenario: TC-09: Validate order details and store in excel sheet
And I get the details 'ProductName1' from 'CorporateCC' and store as 'ProductName1'
And I get the details 'ItemName1' from 'CorporateCC' and store as 'ItemName1'
And I get the details 'UnitPrice1' from 'CorporateCC' and store as 'UnitPrice1'
And I get the details 'Quantity1' from 'CorporateCC' and store as 'Quantity1'
And I get the details 'TotalPrice1' from 'CorporateCC' and store as 'TotalPrice1'
And I get the details 'ProductName2' from 'CorporateCC' and store as 'ProductName2'
And I get the details 'ItemName2' from 'CorporateCC' and store as 'ItemName2'
And I get the details 'UnitPrice2' from 'CorporateCC' and store as 'UnitPrice2'
And I get the details 'Quantity2' from 'CorporateCC' and store as 'Quantity2'
And I get the details 'TotalPrice2' from 'CorporateCC' and store as 'TotalPrice2'
And I get the details 'BillingAddress' from 'CorporateCC' and store as 'BillingAddress'
And I get the details 'CCPaymentType' from 'CorporateCC' and store as 'PaymentType'
And I get the details 'OrderTotal' from 'CorporateCC' and store as 'OrderTotal'


#=====Order Validation CC Payment Corporate Customer=======#
Scenario: Validate Salesforce and login
Given My WebApp 'Omega' is open
And I navigate to 'https://omega--omegadev.lightning.force.com' application
And I wait '5' seconds for presence of element 'OmegaSDFC_Logo'
Then I should see element 'OmegaSDFC_Logo' present on page
Then I should see element 'SalesforceEmail' present on page
And I enter 'pkumar@omega.com' in field 'SalesforceEmail' 
And I click 'SalesforceNext'
Then I should see element 'SalesforcePassword' present on page
And I enter 'RComegaQA@123' in field 'SalesforcePassword'
Then I should see element 'SalesforceVerify' present on page
And I click 'SalesforceVerify'
And I wait for '5' seconds

Scenario: Order Validation CC Payment Corporate Customer
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'OrderNumber' from 'WebCustomerCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I should see element 'SalesforceOrderName' present on page
And I should see element 'SalesforceOrderStatusNumber' present on page
And I should see element 'SalesforcePONumber' present on page
And I should see element 'SalesforceAccountName' present on page
And I should see element 'SalesforceContact' present on page
And I should see element 'SalesforceOrderStartDate' present on page
And I should see element 'SalesforceStatus' present on page
And I should see element 'SalesforceOrderTotal' present on page 


Scenario: Order Validation in Salesforce for CC Payment Corporate Customer in Excel sheet
Then I validate 'OrderNumber' from 'WebCustomerCC' sheet contained at 'SalesforceOrderName'
And I get the details 'SalesforceOrderStatusNumber' from 'WebCustomerCC' and store as 'SalesforceOrderStatusNumber'
Then I validate 'PONumber' from 'WebCustomerCC' sheet contained at 'SalesforcePONumber'
Then I validate 'AccountName' from 'WebCustomerCC' sheet contained at 'SalesforceAccountName'
Then I validate 'OrderPlacedBy' from 'WebCustomerCC' sheet contained at 'SalesforceContact'
And I get the details 'SalesforceOrderStartDate' from 'WebCustomerCC' and store as 'SalesforceOrderStartDate'
And I get the details 'SalesforceStatus' from 'WebCustomerCC' and store as 'SalesforceStatus'
#And I find Total at 'OrderTotal' from 'WebCustomerCC' sheet contained at 'SalesforceOrderTotal'

Scenario: Order Validation CC Payment Corporate Customer page header validation
And I click 'SalesforceOrderStatusNumberLink'
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader' present on page
And I should see element 'SalesforceOrderHeader_AccName' present on page
And I should see element 'SalesforceOrderHeader_Status' present on page
And I should see element 'SalesforceOrderHeader_OrderTotal' present on page

Scenario: Order Validation CC Payment Corporate Customer page header validation
Then I validate 'OrderNumber' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader'
Then I validate 'AccountName' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader_AccName'
#Then I validate 'SalesforceStatus' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader_Status'
#Then I find Total at 'OrderTotal' from 'WebCustomerCC' sheet contained at 'SalesforceOrderHeader_OrderTotal'

Scenario: Order Validation CC Payment Corporate Customer page contents validation
And I should see element 'SalesforceOrderInfo_OrderName' present on page
And I should see element 'SalesforceOrderInfo_OrderStartDate' present on page
And I should see element 'SalesforceOrderInfo_AccountName' present on page
And I should see element 'SalesforceOrderInfo_Status' present on page
And I should see element 'SalesforceOrderInfo_OrderSource' present on page
And I should see element 'SalesforceOrderInfo_MethodOfEntry' present on page
And I scroll to 'coordinates' - '0,750'
And I should see element 'SalesforceCostsInfo_OrderTotal' present on page
And I scroll to 'coordinates' - '0,300'
And I should see element 'SalesforceBillingInfo_PaymentType' present on page
And I should see element 'SalesforceBillingInfo_PaymentTerms' present on page
And I should see element 'SalesforceBillingInfo_PONumber' present on page
And I should see element 'SalesforceShippingInfo_ShipVia' present on page

Scenario: Order Validation CC Payment Corporate Customer page contents validation
And I validate 'OrderNumber' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_OrderName'
And I validate 'SalesforceOrderStartDate' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_OrderStartDate'
And I validate 'SalesforceAccountName' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_AccountName'
And I validate 'SalesforceStatus' from 'WebCustomerCC' sheet contained at 'SalesforceOrderInfo_Status'
Then I should see text 'Website' contained on page at 'SalesforceOrderInfo_OrderSource' 
Then I should see text 'Customer' contained on page at 'SalesforceOrderInfo_MethodOfEntry'
And I validate 'SalesforceOrderTotal' from 'WebCustomerCC' sheet contained at 'SalesforceCostsInfo_OrderTotal'
Then I should see text 'CARD' contained on page at 'SalesforceBillingInfo_PaymentType'
Then I should see text 'CC' contained on page at 'SalesforceBillingInfo_PaymentTerms'
And I validate 'SalesforcePONumber' from 'WebCustomerCC' sheet contained at 'SalesforceBillingInfo_PONumber'
Then I should see text 'UPS Ground - $15.00' contained on page at 'SalesforceShippingInfo_ShipVia'

Scenario: Order Validation CC Payment Corporate Customer page order line items
And I wait for '2' seconds
And I refresh the WebPage
And I should see element 'SalesforceOrderLine_ProductID' present on page
And I should see element 'SalesforceOrderLine_Quantity' present on page
And I should see element 'SalesforceOrderLine_UnitPrice' present on page
And I should see element 'SalesforceOrderLine_TotalPrice' present on page

Scenario: Order Validation CC Payment Corporate Customer page order line items
And I find Product at 'ItemName' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_ProductID'
And I find Quantity at 'Quantity' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_Quantity'
And I find Total at 'UnitPrice' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_UnitPrice'
And I find Total at 'TotalPrice' from 'WebCustomerCC' sheet contained at 'SalesforceOrderLine_TotalPrice'