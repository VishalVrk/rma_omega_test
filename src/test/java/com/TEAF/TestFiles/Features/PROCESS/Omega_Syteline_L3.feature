@OmegaL3
Feature: Syteline functionality Test Automation scenarios


Scenario: wait for 25 mins to reflect changes in integration system
Given I wait for 25 mins to reflect changes in integration system
Given My WindowsApp 'Syteline' is open

Scenario: Verify the syteline desktop application login functinality
And I wait for '15' seconds
When I enter 'tpatel' in field 'UserName' of window
And I enter 'dev1234' in field 'PassWord' of window
And I enter 'dCT01' in field 'ConfigurationFeild' of window
And I click 'Okbutton' button
And I wait for '25' seconds
And I click 'WindowMaximize' button

Scenario: Verify user open form and filter customer orders validate all the stored order details from Front end
And I click 'OpenForm' button
And I enter 'Customer Orders' in field 'FilterInputFeild' of window
And I click 'FilterBtn' button
And I click 'CustomerOrders' button
And I click 'Okbtn_Filter' button
And I enter Order Number 'OrderNumber' from 'AccountPayment' sheet in the Order Pane
Then I should see element 'CustomerOrderNumberLabel' present on Window
And I click 'FilterInPlace' button


Scenario: validate all the stored order details from Front end
And I should see 'PONumber' from 'AccountPayment' sheet present on window at 'CustomerPONumber'
And I should see 'CustomerNumber' from 'AccountPayment' sheet present on window at 'CustomerType'
And I find ShipTo 'SalesforceShippingInfo_ShipTo' from 'AccountPayment' sheet present on window at 'ShipToCustomerOrderWindow'
And I should see 'TermPayment' from 'AccountPayment' sheet present on window at 'TermsPayment'
And I should see 'ShippingCode' from 'AccountPayment' sheet present on window at 'ShipViaCode'
And I should see 'ShippingDes' from 'AccountPayment' sheet present on window at 'ShipingCodeDescription'
And I should see 'CustomerNumber' from 'AccountPayment' sheet present on window at 'CustomerNumberAddressWindow'
And I should see 'ShippingTerms' from 'AccountPayment' sheet present on window at 'ShippingTerms'

Scenario: Verify the contact window 
And I click 'ContactWindow' button
And I should see 'ContactNameContactName' from 'AccountPayment' sheet present on window at 'ContactNameContactName'
And I should see 'ContactEmailContactWindow' from 'AccountPayment' sheet present on window at 'ContactEmailContactWindow'

Scenario: Verify the Line order items for the placed order
And I click 'LinesMenu' button
And I wait for '40' seconds
And I should see text contained 'Corporate HQ' present on window at 'LinesName'
And I find item 'ItemName' from 'AccountPayment' sheet present on window at 'LineItemSkuId'
And I should see text contained '1' present on window at 'LineNumber'
And I find UnitPrice 'UnitPrice' from 'AccountPayment' sheet present on window at 'LineUnitPrice'
And I find NetPrice 'TotalPrice' from 'AccountPayment' sheet present on window at 'LineNetPrice'
And I find Quantity 'Quantity' from 'AccountPayment' sheet present on window at 'LineQtyOrdered'
And I should see 'SytelineStatus' from 'AccountPayment' sheet present on window at 'LineStatus'
