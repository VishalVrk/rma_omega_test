@OmegaL1
Feature: Omega End to end smoke test script

Scenario: TC-01: Validate the homepage contents
Given My WebApp 'Omega' is open
Then I should see element 'OmegaLogo' present on page
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'SignIn_Btn' present on page
Then I should see element 'Register_1' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrderQuote' present on page
Then I should see element 'WhatsNewSection' present on page

Scenario: TC-02: Verify Log into My Account Corporate User
When I click 'OmegaLogo'
And I click 'SignIn_Btn'
Then I should see element 'SignID_Field' present on page
Then I should see element 'Password_Field' present on page
Then I should see element 'LogIn_Btn' present on page
Then I clear field 'SignID_Field'
And I enter 'taha.patel92@royalcyber.com' in field 'SignID_Field'
And I clear field 'Password_Field'
And I enter 'Omega2003@' in field 'Password_Field'  
And I click 'LogIn_Btn'
Then I should see text 'Welcome Taha Patel' present on page at 'Welcome'

Scenario: TC-03	Verify product search functionality - Global Search
When I click 'OmegaLogo'
When I enter '4001AJC' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'

Scenario Outline: TC-04 Verify place order functionality for Corporate customer using Account Payment method and validate Order details in SFDC & Syteline Multiline
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I wait for '2' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
Then I click 'AccountPaymentType'
Then I should see element 'CostCenter' present on page
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I get the details 'Customer_number' from 'AccountPayment' and store as 'CustomerNumber'
And I click 'Next_CartPage'
And I get the details 'PONumber' from 'AccountPayment' and store as 'PONumber'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |


Scenario: TC-05: Validate order details and store in excel sheet
And I should see element 'OrderNumber' present on page
And I should see element 'OrderPlacedBy' present on page
And I should see element 'OrderStatus' present on page
And I should see element 'DatePlaced' present on page
And I should see element 'Total' present on page
And I should see element 'ShipTo' present on page
And I should see element 'ShippingMethod' present on page
And I scroll to 'coordinates' - '0,250'
And I should see element 'ProductName' present on page
And I should see element 'ItemName' present on page
And I should see element 'UnitPrice' present on page
And I should see element 'Quantity' present on page
And I should see element 'TotalPrice' present on page
And I should see element 'BillingAddress' present on page
And I should see element 'OrderTotal' present on page

Scenario: TC-06: Store details in excel sheet
And I scroll to 'coordinates' - '250,0'
And I get the details 'OrderNumber' from 'AccountPayment' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'AccountPayment' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'AccountPayment' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'AccountPayment' and store as 'DatePlaced'
And I get the details 'Total' from 'AccountPayment' and store as 'Total'
And I get the details 'ShipTo' from 'AccountPayment' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'AccountPayment' and store as 'ShippingMethod'
And I get the details 'ProductName' from 'AccountPayment' and store as 'ProductName'
And I get the details 'ItemName' from 'AccountPayment' and store as 'ItemName'
And I get the details 'UnitPrice' from 'AccountPayment' and store as 'UnitPrice'
And I get the details 'Quantity' from 'AccountPayment' and store as 'Quantity'
And I get the details 'TotalPrice' from 'AccountPayment' and store as 'TotalPrice'
And I get the details 'BillingAddress' from 'AccountPayment' and store as 'BillingAddress'
And I get the details 'OrderTotal' from 'AccountPayment' and store as 'OrderTotal'