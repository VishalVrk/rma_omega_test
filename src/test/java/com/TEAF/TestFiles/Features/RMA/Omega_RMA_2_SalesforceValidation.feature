@SalesforceValidationRMA_QA
Feature: RMA Sceanrios for validaiting order from Hybris in Salesforce

#RMA_TC01 #SingleLine #Completed Stable
@CorporateSF1CCRMA_QA
Scenario: RMA_TC01: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment in Hybris for single line
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'CorporateCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I wait for '5' seconds
And I validate contents from search from 'CorporateCC' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'CorporateCC' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate single line item is present from 'CorporateCC' : sheet


#RMA_TC02 #Multi-Line #Completed Stable
@WebSF1CCRMA_QA
Scenario: RMA_TC02: Validate Return order process in Hybris,SFDC and SL for web Customer Order with CC Payment in Hybris for Multi line
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'WebCustomerCC' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'WebCustomerCC' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'WebCustomerCC' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate multi line item is present from 'WebCustomerCC' : sheet

#RMA_TC03 #Multi line order #unstable need to change payment type
@AccountPaymentSF1RMA_QA 
Scenario: RMA_TC03: Validate Return order process in Hybris,SFDC and SL for Corporate Order with Account Payment in Hybris for multi line and multiple quantity
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'AccountPayment' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'AccountPayment' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'AccountPayment' : sheetname 'ACCOUNT' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate multi line item is present from 'AccountPayment' : sheet

#RMA_TC04 #Multi line Multiple Quantity #Unstable Payment Type
@PrepaySF1RMA_QA
Scenario: RMA_TC04: Validate Return order process in Hybris,SFDC and SL for Prepay customer in Hybris for multi line and multiple quantity
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'PrepayOrder' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'PrepayOrder' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'PrepayOrder' : sheetname 'ACCOUNT' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate multi line item is present from 'PrepayOrder' : sheet in reverse

#RMA_TC05 #Multi line order #
@CorporatePartialReturnSF1RMA_QA
Scenario: RMA_TC05: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment for Partial return when there are 2 lines ordered return 1 line
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'CorporatePartialShipping' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'CorporatePartialShipping' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'CorporatePartialShipping' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate multi line item is present from 'CorporatePartialShipping' : sheet in reverse

#TC_06 Multi Line Order
@PartialShippingSFRMA_QA
Scenario: RMA_TC06: Validate Return order process in Hybris,SFDC and SL for Corporate Order for Partial Shipped items in SyteLine
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'PartialReturnOrder' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'PartialReturnOrder' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'PartialReturnOrder' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate multi line item is present from 'PartialReturnOrder' : sheet in reverse

#TC-07 #SingleLine
@WebCustomerSingleLineSFRMA_QA  
Scenario: RMA_TC07: Validate Return process end to end when reason code is selected as Customer error in Return order page 
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'WebCustomerSingleLine' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'WebCustomerSingleLine' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'WebCustomerSingleLine' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate single line item is present from 'WebCustomerSingleLine' : sheet

#RMA-TC08 #Single Line
@AccountPaymentSingleLineSFRMA_QA @Ignore
Scenario: RMA_TC08: Validate Return process end to end when reason code is selected as Omega error in Return order page 
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'AccountPaymentSingleLine' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'AccountPaymentSingleLine' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'AccountPaymentSingleLine' : sheetname 'ACCOUNT' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate single line item is present from 'AccountPaymentSingleLine' : sheet

#TC_09 Multi Line Order
@CorporateCCMultiLineSFRMA_QA
Scenario: RMA_TC09: Validate Return process end to end when reason code is combination of both Customer error and Omega error in Return order page 
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'CoporateAccountMultiline' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'CoporateAccountMultiline' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'CoporateAccountMultiline' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate multi line item is present from 'CoporateAccountMultiline' : sheet in reverse


#TC_10 Single Line
@RestockingFeeSF1RMA_QA
Scenario: RMA_TC10: Verify Restocking fee on item level is applied when invoice date and validate the Refund amount is in sync in all the integration systems
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'RestockingFee' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'RestockingFee' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'RestockingFee' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate single line item is present from 'RestockingFee' : sheet

#RMA_TC11 Single line multiple Quantity
@VolumeDiscountsSFRMA_QA
Scenario: RMA_TC11:Validate RMA process for Volume discount orders 
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I clear field 'SalesforceSearch'
And I enter 'OrderNumber' from 'VolumeDiscountsRMA' sheet to the field 'SalesforceSearch'
And I press key: enter
And I validate contents from search from 'VolumeDiscountsRMA' : sheet
And I wait for '2' seconds
And I refresh the WebPage
And I validate salesforce contents 'VolumeDiscountsRMA' : sheetname 'CARD' : payment type
And I wait for '2' seconds
And I refresh the WebPage
And I wait for '10' seconds
And I validate single line item is present from 'VolumeDiscountsRMA' : sheet
#And I validate single line discount item is present from 'VolumeDiscountsRMA' : sheet

@Ignore
Scenario: Wait Till Order Reaches The Integrating System
Given I wait for '300' seconds
And I load the excel sheet data to validate the Syteline