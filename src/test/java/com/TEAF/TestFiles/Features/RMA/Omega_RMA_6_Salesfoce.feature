@SalesforceRMAID_QA
Feature: RMA Sceanrios for placing order using Hybris

#RMA_TC01
@CorporateSFCCRRMA_QA
Scenario: RMA_TC01: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment in Hybris for single line
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'CorporateCC' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'CorporateCC' : sheet
And I refresh the WebPage 
And I validate RMA Header from 'CorporateCC' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'CorporateCC' : sheet
And I validate RMA Generate Order details from 'CorporateCC' : sheet

#RMA_TC02
@WebSFCCRRMA_QA
Scenario: RMA_TC02: Validate Return order process in Hybris,SFDC and SL for web Customer Order with CC Payment in Hybris for Multi line
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'WebCustomerCC' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'WebCustomerCC' : sheet
And I refresh the WebPage
And I validate RMA Header from 'WebCustomerCC' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'WebCustomerCC' : sheet
And I validate RMA Generate Order details from 'WebCustomerCC' : sheet

#RMA_TC03
@AccountPaymentSFRRMA_QA
Scenario: RMA_TC03: Validate Return order process in Hybris,SFDC and SL for Corporate Order with Account Payment in Hybris for multi line and multiple quantity
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'AccountPayment' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'AccountPayment' : sheet
And I refresh the WebPage
And I validate RMA Header from 'AccountPayment' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'AccountPayment' : sheet
And I validate RMA Generate Order details from 'AccountPayment' : sheet

#RMA_TC04
@CorporatePartialReturnSFRRMA_QA
Scenario: RMA_TC04: Validate Return order process in Hybris,SFDC and SL for Prepay customer in Hybris for multi line and multiple quantity
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'PrepayOrder' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'PrepayOrder' : sheet
And I refresh the WebPage
And I validate RMA Header from 'PrepayOrder' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'PrepayOrder' : sheet
And I validate RMA Generate Order details from 'PrepayOrder' : sheet

#RMA_TC05
@PrepaySFRRMA_QA
Scenario: RMA_TC05: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment for Partial return when there are 2 lines ordered return 1 line
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'CorporatePartialShipping' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'CorporatePartialShipping' : sheet
And I refresh the WebPage
And I validate RMA Header from 'CorporatePartialShipping' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'CorporatePartialShipping' : sheet
And I validate RMA Generate Order details from 'CorporatePartialShipping' : sheet

#RMA_TC06
@PartialShippingSFRRMA_QA
Scenario: RMA_TC06: Validate Return order process in Hybris,SFDC and SL for Corporate Order for Partial Shipped items in SyteLine
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'PartialReturnOrder' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'PartialReturnOrder' : sheet
And I refresh the WebPage
And I validate RMA Header from 'PartialReturnOrder' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'PartialReturnOrder' : sheet
And I validate RMA Generate Order details from 'PartialReturnOrder' : sheet

#RMA_TC07
@WebCustomerSingleLineSFRRMA_QA
Scenario: RMA_TC07: Validate Return process end to end when reason code is selected as Customer error in Return order page 
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'WebCustomerSingleLine' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'WebCustomerSingleLine' : sheet
And I refresh the WebPage
And I validate RMA Header from 'WebCustomerSingleLine' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'WebCustomerSingleLine' : sheet
And I validate RMA Generate Order details from 'WebCustomerSingleLine' : sheet

#RMA_TC08
@AccountPaymentSingleLineSFRRMA_QA
Scenario: RMA_TC08: Validate Return process end to end when reason code is selected as Omega error in Return order page
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'AccountPaymentSingleLine' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'AccountPaymentSingleLine' : sheet
And I refresh the WebPage
And I validate RMA Header from 'AccountPaymentSingleLine' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'AccountPaymentSingleLine' : sheet
And I validate RMA Generate Order details from 'AccountPaymentSingleLine' : sheet

#RMA_TC09
@CorporateCCMultiLineSFRRMA_QA
Scenario: RMA_TC09: Validate Return process end to end when reason code is combination of both Customer error and Omega error in Return order page
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'CoporateAccountMultiline' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'CoporateAccountMultiline' : sheet
And I refresh the WebPage
And I validate RMA Header from 'CoporateAccountMultiline' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'CoporateAccountMultiline' : sheet
And I validate RMA Generate Order details from 'CoporateAccountMultiline' : sheet

#RMA_TC10
@RestockingFeeSFRRMA_QA
Scenario: RMA_TC10: Verify Restocking fee on item level is applied when invoice date and validate the Refund amount is in sync in all the integration systems
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'RestockingFee' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'RestockingFee' : sheet
And I refresh the WebPage
And I validate RMA Header from 'RestockingFee' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'RestockingFee' : sheet
And I validate RMA Generate Order details from 'RestockingFee' : sheet

#RMA_TC11
@VolumeDiscountsSFRRMA_QA
Scenario: RMA_TC11:Validate RMA process for Volume discount orders
Given My WebApp 'Omega' is open
And I Login Salesforce Application QA
Then I click 'SalesforceOmegaLogo'
Then I should see element 'SalesforceSearch' present on page_
Then I should see element 'SalesforceOmegaLogo' present on page_
And I enter 'RMA_ID' from 'VolumeDiscountsRMA' sheet to the field 'SalesforceSearch'
And I wait for '5' seconds
And I press key: enter
And I validate RMA Salesforce details from 'VolumeDiscountsRMA' : sheet
And I refresh the WebPage
And I validate RMA Header from 'VolumeDiscountsRMA' : sheet
And I wait for '5' seconds
And I validate RMA Salesforce order details from 'VolumeDiscountsRMA' : sheet
And I validate RMA Generate Order details from 'VolumeDiscountsRMA' : sheet

Scenario: Wait till the Return Order is Displayed in Hybris
Given I wait for '300' seconds
And I load the excel sheet data to validate the Syteline