@PlaceOrderStoreFrontRMA_QA
Feature: RMA Sceanrios for placing order using Hybris

#RMA_TC01 #SingleLine #Completed Stable
@CorporateCCRMA_QA
Scenario Outline: RMA_TC01: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment in Hybris for single line
Given I refresh the WebPage
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search' 
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
Given I refresh the WebPage
And I click 'OmegaLogo'
And I should see element 'Minicart' present on page
And I click 'Minicart'
And I scroll to 'coordinates' - '0,150'
And I wait for '4' seconds
And I click 'Checkout_MiniCart'
And I wait for '5' seconds
Then I should see element 'Checkout' present on page_
When I click 'Checkout'
And I click 'CardPayment'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'CorporateCC' and store as 'PONumber'
And I get the details 'CustomerID' from 'CorporateCC' and store as 'CustomerID'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I wait for '5' seconds
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the one line order related information and store it in a excel sheet 'CorporateCC' 

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#RMA_TC02 #Multi-Line #Completed Stable
@WebCCRMA_QA
Scenario Outline: RMA_TC02: Validate Return order process in Hybris,SFDC and SL for web Customer Order with CC Payment in Hybris for Multi line
Given I refresh the WebPage
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
Then I refresh the WebPage
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
Then I refresh the WebPage
And I wait for '3' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I clear field 'POnumberFeild'
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'WebCustomerCC' and store as 'PONumber'
And I get the details 'CustomerID' from 'WebCustomerCC' and store as 'CustomerID'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
When I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,-200'
And I wait for '2' seconds
When I click 'SubmitAsIs'
When I click 'Next_CartPage'
And I wait for '5' seconds
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444222233331111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I get the details 'PONumber_Cart' from 'TestWeb' and store as 'PONumber'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the two line order related information and store it in a excel sheet 'WebCustomerCC'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#RMA_TC03 #Multi line order #unstable need to change payment type
@AccountPaymentRMA_QA
Scenario Outline: RMA_TC03: Validate Return order process in Hybris,SFDC and SL for Corporate Order with Account Payment in Hybris for multi line and multiple quantity
And I click 'OmegaLogo'
Then I should see element 'QuickOrder_Header' present on page
And I focus and click 'QuickOrder_Header'
Then I should see element 'SKU_field' present on page
When I enter 'B-FER-1/16' in field 'SKU_field'
And I press enter key
And I clear field 'QuickOrderQuoteUpdateQty'
And I clear the text and enter '2' in field 'QuickOrderQuoteUpdateQty'
And I click 'QuickOrderQuote_AddtoCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I wait for '5' seconds
Then I refresh the WebPage
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter 'HTC-030' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '3' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
Then I refresh the WebPage
And I wait for '5' seconds
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
Then I click 'AccountPaymentType'
Then I should see element 'CostCenter' present on page_
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I get the details 'AccountCustomerPoNumber' from 'AccountPayment' and store as 'PONumber'
And I get the details 'CustomerID' from 'AccountPayment' and store as 'CustomerID'
And I click 'Next_CartPage'
And I select option '1' in dropdown 'Delivery_Country' by 'index'
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
When I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,-200'
And I wait for '2' seconds
When I click 'SubmitAsIs'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see element 'OrderNumber' present on page_
And I should see the two line order related information and store it in a excel sheet 'AccountPayment'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#RMA_TC04 #Multi line Multiple Quantity #Unstable Payment Type
@PrepayRMA_QA
Scenario Outline: RMA_TC04: Validate Return order process in Hybris,SFDC and SL for Prepay customer in Hybris for multi line and multiple quantity
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter 'HTC-030' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '2' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I wait for '5' seconds
Given I refresh the WebPage
When I click 'OmegaLogo'
When I enter 'B-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I wait for '2' seconds
And I click 'AddToCart'
And I wait for '2' seconds
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
Then I should see element 'PrepayOption' present on page
Then I click 'PrepayOption'
And I clear field 'POnumberFeild'
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'PrepayOrder' and store as 'PONumber'
And I get the details 'CustomerID' from 'PrepayOrder' and store as 'CustomerID'
And I click 'Next_CartPage'
And I select US from dropdown  
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
When I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,-200'
And I wait for '2' seconds
When I click 'SubmitAsIs'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'OrderNumber' present on page_
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I get text from 'PlacedOrderNumber' and store
And I scroll to top of the page
And I should see the two line order related information and store it in a excel sheet 'PrepayOrder'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#RMA_TC05 #Multi line order #
@CorporatePartialReturn_QA
Scenario Outline: RMA_TC05: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment for Partial return when there are 2 lines ordered return 1 line
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '4' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
And I click 'CardPayment'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'PartialReturnOrder' and store as 'PONumber'
And I get the details 'CustomerID' from 'PartialReturnOrder' and store as 'CustomerID'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the two line order related information and store it in a excel sheet 'CorporatePartialShipping'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#TC_06 Multi Line Order
@PartialShipping_QA
Scenario Outline: RMA_TC06: Validate Return order process in Hybris,SFDC and SL for Corporate Order for Partial Shipped items in SyteLine
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'

And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '4' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
And I click 'CardPayment'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'CorporatePartialShipping' and store as 'PONumber'
And I get the details 'CustomerID' from 'CorporatePartialShipping' and store as 'CustomerID'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the two line order related information and store it in a excel sheet 'PartialReturnOrder'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#TC-07 #SingleLine
@WebCustomerSingleLine_QA  
Scenario Outline: RMA_TC07: Validate Return process end to end when reason code is selected as Customer error in Return order page 
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
And I wait for '3' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'WebCustomerSingleLine' and store as 'PONumber'
And I get the details 'CustomerID' from 'WebCustomerSingleLine' and store as 'CustomerID'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
When I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the one line order related information and store it in a excel sheet 'WebCustomerSingleLine'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#RMA-TC08 #Single Line
@AccountPaymentSingleLine_QA @Ignore
Scenario Outline: RMA_TC08: Validate Return process end to end when reason code is selected as Omega error in Return order page 
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
And I wait for '3' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
Then I click 'AccountPaymentType'
Then I should see element 'CostCenter' present on page_
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I get the details 'AccountCustomerPoNumber' from 'AccountPaymentSingleLine' and store as 'PONumber'
And I get the details 'CustomerID' from 'AccountPaymentSingleLine' and store as 'CustomerID'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
#And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I wait for '2' seconds
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the one line order related information and store it in a excel sheet 'AccountPaymentSingleLine'
Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#TC_09 Multi Line Order
@CorporateCCMultiLine_QA
Scenario Outline: RMA_TC09: Validate Return process end to end when reason code is combination of both Customer error and Omega error in Return order page 
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'OmegaLogo'
When I enter 'B-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
And I click 'OmegaLogo'
And I should see element 'Minicart' present on page
And I click 'Minicart'
And I scroll to 'coordinates' - '0,150'
And I wait for '4' seconds
And I click 'Checkout_MiniCart'
And I wait for '5' seconds
Then I should see element 'Checkout' present on page
When I click 'Checkout'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'CoporateAccountMultiline' and store as 'PONumber'
And I get the details 'CustomerID' from 'CoporateAccountMultiline' and store as 'CustomerID'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the two line order related information and store it in a excel sheet 'CoporateAccountMultiline'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#TC_10 Single Line
@RestockingFee_QA
Scenario Outline: RMA_TC10: Verify Restocking fee on item level is applied when invoice date and validate the Refund amount is in sync in all the integration systems
When I click 'OmegaLogo'
When I enter 'HTC-030' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
And I wait for '5' seconds
And I should see element 'Checkout' present on page
When I click 'Checkout'
And I wait for '3' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'RestockingFee' and store as 'PONumber'
And I get the details 'CustomerID' from 'RestockingFee' and store as 'CustomerID'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
When I click 'Next_CartPage'
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the one line order related information and store it in a excel sheet 'RestockingFee'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#RMA_TC11 Single line multiple Quantity
@VolumeDiscounts_QA
Scenario Outline: RMA_TC11:Validate RMA process for Volume discount orders 
When I click 'OmegaLogo'
When I enter 'B-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I scroll to 'coordinates' - '0,850'
And I click 'ProductExpendButton'
And I wait for '3' seconds
And I should see element 'ProductQuantityBox' present on page
And I clear field 'qtyInputTable'
And I wait for '5' seconds
And I enter '11' in field 'qtyInputTable'
And I click 'AddToCart_ProductExpand'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see element 'Checkout' present on page
And I should see element 'Price_reflects' present on page
#And I should see text '* Price reflects volume discount' present on page at 'PriceVolumeDiscount'
When I click 'Checkout'
And I wait for '5' seconds
Then I should see element 'CardPayment' present on page
And I click 'CardPayment'
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I get the details 'CustomerPoNumber' from 'VolumeDiscountsRMA' and store as 'PONumber'
And I get the details 'CustomerID' from 'VolumeDiscountsRMA' and store as 'CustomerID'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
When I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see the one line order related information and store it in a excel sheet 'VolumeDiscountsRMA'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

@Ignore
Scenario: Wait Till Order Reaches The Integrating System
Given I wait for '300' seconds
And I load the excel sheet data to validate the Syteline