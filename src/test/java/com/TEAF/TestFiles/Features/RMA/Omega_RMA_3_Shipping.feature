@SytlineShippingRMA_QA
Feature: RMA Features Shipping scenarios is Sytline

@StylineMultiOrder
Scenario Outline: Return order process in SL for Multi line
When I open form and search 'Customer Orders' & select 'CustomerOrders_SL' then I should see 'OrderNumber_SL'
And I enter 'OrderNumber' from '<SheetName>' and filter the order
And I should see the order details '<SheetName>' feilds present in the page
And I click on Contacts and verify the contact details '<SheetName>' present in the page
And I validate amounts tab from '<SheetName>'
And I wait for '5' seconds
And I click 'LinesButton_SL'
And I wait for '25' seconds
Then I should see order line item 1 and corresponding details '<SheetName>' present in the page
And I click item 2 with order number sheet '<SheetName>' and column 'OrderNumber'
And I wait for '20' seconds
Then I should see order line item 2 and corresponding details '<SheetName>' present in the page
And I click 'CustomerOrdersOptions'
And I wait for '2' seconds
And I click 'AddressOptions'
And I click on override and revalidate the order
And I click 'LinesButton_SL'
And I get order line max due date from '<SheetName>' : sheet 'OrderNumber' : column
And I open form and search 'Shipping Workbench' & select 'ShippingWorkbench_SL' then I should see 'Upto_DueDate_SL'
And I wait for '25' seconds
And I enter due date & order number from '<SheetName>' and click on regenerate button and shippment type '<ShippingOption>'
And I open form and search 'Shipment Master' & select 'ShipmentMaster_SL' then I should see 'TrackingNumber_SL'
And I enter tracking number and ship the order
And I open form and search 'Order Invoicing/Credit Memo' & select 'OrderInvoicing_SL' then I should see 'Invoice_date_SL'
And I enter customer number and process invocing for the order '<SheetName>'
When I click 'CustomerOrderLines_header'
And I wait for '5' seconds
And I verify items are packed, shipped and invoiced to complete the order from '<SheetName>' : 'OrderNumber' and save the item and shipping '<ShippingOption>'

Examples:
|SheetName	                |ShippingOption	  |
#|WebCustomerCC	            |FullShippment 	  |
#|AccountPayment	            |FullShippment 	  |
|CorporatePartialShipping	|PartialShippment |
#|PartialReturnOrder	        |FullShippment 	  |
#|CoporateAccountMultiline	|FullShippment 	  |
#|PrepayOrder	            |FullShippment 	  |

##SINGLE ORDER LINE  ##NEED TO CHANGE DATE
@StylineSingleOrder
Scenario Outline: Return order process in SL for Single Line Order
When I open form and search 'Customer Orders' & select 'CustomerOrders_SL' then I should see 'OrderNumber_SL'
And I enter 'OrderNumber' from '<SheetName>' and filter the order
And I should see the order details '<SheetName>' feilds present in the page
And I click on Contacts and verify the contact details '<SheetName>' present in the page
And I validate amounts tab from '<SheetName>'
And I wait for '5' seconds
And I click 'LinesButton_SL'
And I wait for '25' seconds
Then I should see order line item one and corresponding details '<SheetName>' present in the page
And I get the date 'DueDate_SL' from '<SheetName>' and store as 'DueDate_SL'
And I click 'CustomerOrdersOptions'
And I wait for '2' seconds
And I click 'AddressOptions'
And I click on override and revalidate the order
And I click 'LinesButton_SL'
And I open form and search 'Shipping Workbench' & select 'ShippingWorkbench_SL' then I should see 'Upto_DueDate_SL'
And I wait for '25' seconds
And I enter due date & order number from '<SheetName>' and click on regenerate button and shippment type '<ShippingOption>'
And I open form and search 'Shipment Master' & select 'ShipmentMaster_SL' then I should see 'TrackingNumber_SL'
And I enter tracking number and ship the order
And I open form and search 'Order Invoicing/Credit Memo' & select 'OrderInvoicing_SL' then I should see 'Invoice_date_SL'
And I enter customer number and process invocing for the order '<SheetName>'
When I click 'CustomerOrderLines_header'
And I wait for '5' seconds
And I verify items are packed, shipped and invoiced to complete the order from '<SheetName>' : 'OrderNumber' and save the item single line

Examples:
|SheetName	             |ShippingOption	|
|CorporateCC             |FullShippment 	|
#|WebCustomerSingleLine   |FullShippment     |
##|AccountPaymentSingleLine|FullShippment     |
#|RestockingFee           |FullShippment     |
#|VolumeDiscountsRMA      |FullShippment     |

@Ignore
Scenario: Wait till the Return Order is Displayed in Hybris
Given I wait for '700' seconds
And I load the excel sheet data to validate the Syteline