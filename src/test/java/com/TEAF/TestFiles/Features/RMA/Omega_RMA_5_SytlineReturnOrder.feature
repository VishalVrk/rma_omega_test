@SytlineOmegaCallLog
Feature: RMA Sceanrios for placing order using Hybris

#RMA_TC01
@CorporateCCLRMA_QA
Scenario: RMA_TC01: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment in Hybris for single line
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'CorporateCC' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'CorporateCC' : sheetname 'ItemName' : column and enter date
And I wait for '5' seconds
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'CorporateCC' : sheetname 'ItemName' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'CorporateCC'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'CorporateCC' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@WebCCLRMA_QA
Scenario: RMA_TC02: Validate Return order process in Hybris,SFDC and SL for web Customer Order with CC Payment in Hybris for Multi line
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'WebCustomerCC' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'WebCustomerCC' : sheetname 'ItemName1' : column and enter date
And I wait for '5' seconds
And I should find received date 'WebCustomerCC' : sheetname 'ItemName2' : column and enter date
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'WebCustomerCC' : sheetname 'ItemName1' : column
And I click and select 'WebCustomerCC' : sheetname 'ItemName2' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'WebCustomerCC'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'WebCustomerCC' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@AccountPaymentCLRMA_QA
Scenario: RMA_TC03: Validate Return order process in Hybris,SFDC and SL for Corporate Order with Account Payment in Hybris for multi line and multiple quantity
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'AccountPayment' and filter the order omega call log
And I validate contents from Omega Call Log
#And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'AccountPayment' : sheetname 'ItemName1' : column and enter date
And I wait for '5' seconds
And I should find received date 'AccountPayment' : sheetname 'ItemName2' : column and enter date
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'AccountPayment' : sheetname 'ItemName1' : column
And I click and select 'AccountPayment' : sheetname 'ItemName2' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'AccountPayment'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'AccountPayment' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@PrepayCLRMA_QA
Scenario: RMA_TC04: Validate Return order process in Hybris,SFDC and SL for Prepay customer in Hybris for multi line and multiple quantity
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'PrepayOrder' and filter the order omega call log
And I validate contents from Omega Call Log
#And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'PrepayOrder' : sheetname 'ItemName1' : column and enter date
And I wait for '5' seconds
And I should find received date 'PrepayOrder' : sheetname 'ItemName2' : column and enter date
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'PrepayOrder' : sheetname 'ItemName1' : column
And I click and select 'PrepayOrder' : sheetname 'ItemName2' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'PrepayOrder'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'PrepayOrder' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@CorporatePartialReturnCL_QA
Scenario: RMA_TC05: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment for Partial return when there are 2 lines ordered return 1 line
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'CorporatePartialShipping' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'CorporatePartialShipping' : sheetname 'ItemName1' : column and enter date
And I wait for '5' seconds
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'CorporatePartialShipping' : sheetname 'ItemName1' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'CorporatePartialShipping'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'CorporatePartialShipping' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@PartialShippingCL_QA
Scenario: RMA_TC06: Validate Return order process in Hybris,SFDC and SL for Corporate Order for Partial Shipped items in SyteLine
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'PartialReturnOrder' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'PartialReturnOrder' : sheetname 'ItemName1' : column and enter date
And I wait for '5' seconds
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'PartialReturnOrder' : sheetname 'ItemName1' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'PartialReturnOrder'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'PartialReturnOrder' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@WebCustomerSingleLineCL_QA 
Scenario: RMA_TC07: Validate Return process end to end when reason code is selected as Customer error in Return order page 
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'WebCustomerSingleLine' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'WebCustomerSingleLine' : sheetname 'ItemName' : column and enter date
And I wait for '5' seconds
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'WebCustomerSingleLine' : sheetname 'ItemName' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'WebCustomerSingleLine'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'WebCustomerSingleLine' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@AccountPaymentSingleLineCL_QA
Scenario: RMA_TC08: Validate Return process end to end when reason code is selected as Omega error in Return order page 
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'AccountPaymentSingleLine' and filter the order omega call log
And I validate contents from Omega Call Log
#And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'AccountPaymentSingleLine' : sheetname 'ItemName' : column and enter date
And I wait for '5' seconds
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'AccountPaymentSingleLine' : sheetname 'ItemName' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'AccountPaymentSingleLine'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'AccountPaymentSingleLine' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@CorporateCCMultiLineCL_QA
Scenario: RMA_TC09: Validate Return process end to end when reason code is combination of both Customer error and Omega error in Return order page
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'CoporateAccountMultiline' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'CoporateAccountMultiline' : sheetname 'ItemName1' : column and enter date
And I wait for '5' seconds
And I should find received date 'CoporateAccountMultiline' : sheetname 'ItemName2' : column and enter date
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'CoporateAccountMultiline' : sheetname 'ItemName1' : column
And I click and select 'CoporateAccountMultiline' : sheetname 'ItemName2' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'CoporateAccountMultiline'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'CoporateAccountMultiline' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@RestockingFeeCL_QA
Scenario: RMA_TC10: Verify Restocking fee on item level is applied when invoice date and validate the Refund amount is in sync in all the integration systems
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'RestockingFee' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'RestockingFee' : sheetname 'ItemName' : column and enter date
And I wait for '5' seconds
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'RestockingFee' : sheetname 'ItemName' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'RestockingFee'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'RestockingFee' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

@VolumeDiscountsCL_QA
Scenario: RMA_TC11:Validate RMA process for Volume discount orders
When I open form and search 'Omega Call log' & select 'OmegaCall_Log' then I should see 'RMA_HybrisID'
And I enter 'RMA_ID' from 'VolumeDiscountsRMA' and filter the order omega call log
And I validate contents from Omega Call Log
And I select card from Omega Call Log
And I should see element 'OrderGenerateTab_OmegaCallLog' present on page
And I click 'OrderGenerateTab_OmegaCallLog'
And I should find received date 'VolumeDiscountsRMA' : sheetname 'ItemName' : column and enter date
And I wait for '5' seconds
And I focus and click 'Phone_OmegaCallLog'
And I wait for '5' seconds
And I click by JS 'CustomerOrderLines_Save'
And I wait for '5' seconds
And I click and select 'VolumeDiscountsRMA' : sheetname 'ItemName' : column
And I wait for '2' seconds
And I should see element 'GenerateOrders_OmegaCallLogs' present on page
And I click 'GenerateOrders_OmegaCallLogs'
And I wait for '10' seconds
And I click 'GenerateOrders_OkBtn'
And I wait for '5' seconds
And I click 'Refresh_btnSL'
And I wait for '3' seconds
And I get tracking ID and store in sheet 'VolumeDiscountsRMA'
When I open form and search 'RMAs' & select 'RMAs_form' then I should see 'RMARefNumber'
And I enter 'VolumeDiscountsRMA' : 'ReferenceNumber' from spreadsheet in the feild 'RMARefNumber'
When I should see element 'SearchOrders_SL' present on page
And I focus and click 'SearchOrders_SL'
And I vaildate RMA details

Scenario: Wait till the Return Order is Displayed in Hybris
Given I wait for '700' seconds
And I load the excel sheet data to validate the Syteline