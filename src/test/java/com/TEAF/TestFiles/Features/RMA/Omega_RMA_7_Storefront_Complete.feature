@CompletedValidation_Storefront_QA
Feature: RMA Sceanrios for placing order using Hybris

#RMA_TC01
@CorporateCCRRMA_QA
Scenario: RMA_TC01: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment in Hybris for single line
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'CorporateCC' : sheet 
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'CorporateCC' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'CorporateCC' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'


#RMA_TC02
@WebCCReturnRRMA_QA
Scenario: RMA_TC02: Validate Return order process in Hybris,SFDC and SL for web Customer Order with CC Payment in Hybris for Multi line
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'WebCustomerCC' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'WebCustomerCC' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'WebCustomerCC' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'


#RMA_TC03
@AccountPaymentRRMA_QA
Scenario: RMA_TC03: Validate Return order process in Hybris,SFDC and SL for Corporate Order with Account Payment in Hybris for multi line and multiple quantity
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'AccountPayment' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'AccountPayment' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'AccountPayment' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'


#RMA_TC04
@PrepayRRMA_QA
Scenario: RMA_TC04: Validate Return order process in Hybris,SFDC and SL for Prepay customer in Hybris for multi line and multiple quantity
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'PrepayOrder' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'PrepayOrder' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'PrepayOrder' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'


#RMA_TC05
@CorporatePartialReturnRRMA_QA
Scenario: RMA_TC05: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment for Partial return when there are 2 lines ordered return 1 line
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'CorporatePartialShipping' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'CorporatePartialShipping' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'CorporatePartialShipping' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC06
@PartialShippingRRMA_QA
Scenario: RMA_TC06: Validate Return order process in Hybris,SFDC and SL for Corporate Order for Partial Shipped items in SyteLine
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'PartialReturnOrder' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'PartialReturnOrder' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'PartialReturnOrder' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC07
@WebCustomerSingleLineRRMA_QA 
Scenario: RMA_TC07: Validate Return process end to end when reason code is selected as Customer error in Return order page 
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'WebCustomerSingleLine' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'WebCustomerSingleLine' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'WebCustomerSingleLine' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'


#RMA_TC08
@AccountPaymentSingleLineRRMA_QA
Scenario: RMA_TC08: Validate Return process end to end when reason code is selected as Omega error in Return order page
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'AccountPaymentSingleLine' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'AccountPaymentSingleLine' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'AccountPaymentSingleLine' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC09
@CorporateCCMultiLineRRMA_QA
Scenario: RMA_TC09: Validate Return process end to end when reason code is combination of both Customer error and Omega error in Return order page
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'CoporateAccountMultiline' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'CoporateAccountMultiline' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'CoporateAccountMultiline' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC10
@RestockingFeeRRMA_QA
Scenario: RMA_TC10: Verify Restocking fee on item level is applied when invoice date and validate the Refund amount is in sync in all the integration systems
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'RestockingFee' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'RestockingFee' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'RestockingFee' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC11
@VolumeDiscountsRRMA_QA
Scenario: RMA_TC11:Validate RMA process for Volume discount orders
Given My WebApp 'Omega' is open
And I navigate to ASM Application
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I click on the RMA ID from 'VolumeDiscountsRMA' : sheet
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I validate 'RMA_ID' from 'VolumeDiscountsRMA' sheet contained at 'RMA_NumberReturnOrderCheck'
And I should see element 'RMA_ReturnStatus' present on page
And I should see text 'Completed' contained on page at 'RMA_ReturnStatusCheck'
And I should see element 'RMA_OrderNumber' present on page
And I validate 'OrderNumber' from 'VolumeDiscountsRMA' sheet contained at 'RMA_OrderNumberCheck'
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'