@RMARestockingFEE
Feature: RMA Sceanrios for placing order using Hybris

#RMA_TC11
Scenario: RMA_TC11: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment in Hybris for single line
Given My WebApp 'Omega' is open
And I navigate to 'https://omega.okta.com/app/UserHome' application
And I wait '5' seconds for presence of element 'OmegaSDFC_Logo'
Then I should see element 'OmegaSDFC_Logo' present on page
Then I should see element 'SalesforceEmail' present on page
And I enter 'tpatel@omega.com' in field 'SalesforceEmail' 
And I click 'SalesforceNext'
Then I should see element 'SalesforcePassword' present on page
And I enter 'kiZC*ZuoxFA6' in field 'SalesforcePassword'
Then I should see element 'SalesforceVerify' present on page
And I click 'SalesforceVerify'
And I wait for '5' seconds
And I click 'Hybris_DEV'
And I wait for '10' seconds
And I switch to window with title 'hybris Backoffice'
And I should see element 'Hybris_logo' present on page
And I wait for '10' seconds
And I wait for visibility of element 'Hybris_orderOption'
And I should see element 'Hybris_orderOption' present on page
And I click 'Hybris_orderOption'
And I wait for '2' seconds
And I should see element 'Hybris_SelectOrders' present on page
And I click 'Hybris_SelectOrders'
And I wait for '10' seconds
And I wait for visibility of element 'Hybris_SearchField'
And I should see element 'Hybris_SearchField' present on page
#And I enter 'OrderNumber' from 'WebCustomerCC' sheet to the field 'Hybris_SearchField'
And I enter 'HC00525332' in field 'Hybris_SearchField'
And I wait for '5' seconds
And I press key: enter
And I wait for '5' seconds
And I should see element 'Hybris_FirstElement' present on page
And I click 'Hybris_FirstElement'
And I wait for '2' seconds
And I should see element 'Hybris_Positions' present on page
And I click 'Hybris_Positions'
And I should see 'Hybris_PositionsDate' from field
And I clear field 'Hybris_PositionsDate'
And I get 60 days before given date and enter at 'Hybris_PositionsDate'
And I wait for '5' seconds
And I click 'Hybris_SaveButton'
And I wait for '2' seconds
And I click 'Hybris_rightButton'
And I click 'Hybris_rightButton'
And I should see element 'Hybris_ConsignmentsTab' present on page
And I click 'Hybris_ConsignmentsTab'
And I scroll through page
And I double-click 'Hybris_ShippingMethod'
And I should see element 'Hybris_ShippingMethodDate' present on page
And I clear field 'Hybris_ShippingMethodDate'
And I get 60 days before given date and enter at 'Hybris_ShippingMethodDate'
And I press key: enter
And I wait for '5' seconds
And I click 'Hybris_SaveConsigment'

@CorporateRestockingFee
Scenario: Navigate to Storefront and validate order
When I click 'OmegaLogo'
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I enter 'HC00525332' in field 'SearchOrderNumber'
#When I enter 'OrderNumber' from 'AccountPayment' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I wait for '10' seconds
And I click 'FirstOrderNumber'
And I should see element 'ReturnOrderBtnOrderDetails' present on page
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter '2' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I clear field 'SecondProductReturnQty'
And I enter '3' in the feild 'SecondProductReturnQty' using actions
And I click 'SecondProductBlock'
And I click 'ReturnOrderConfirmationBtn'
And I validate restocking fee validation 'ProductPrice_1' :Price 'QtyProduct_1' :Qty 'RestockingFeeAmount_1' :RestockingFee 'ProductTotal_1' :Total 
And I validate restocking fee validation 'ProductPrice_2' :Price 'QtyProduct_2' :Qty 'RestockingFeeAmount_2' :RestockingFee 'ProductTotal_2' :Total