@ReturnOrderStoreFrontRMA_QA
Feature: RMA scenario to Return order from Storefront

#RMA_TC01 #SingleLine
@CorporateCCRRMA_QA
Scenario: RMA_TC01: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment in Hybris for single line
Given My WebApp 'Omega' is open
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'AccountPayment' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I clear field 'FirstProductReturnQty'
And I click 'FirstProductOriginalPackingCheckBox'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I scroll through page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I scroll to top of the page
And I clear field 'FirstProductReturnQty'
And I enter '1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I scroll to 'coordinates' - '0,600'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page_
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'CorporateCC' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#TC_TC02 #Multi-Line
@WebCCReturnRRMA_QA
Scenario: RMA_TC02: Validate Return order process in Hybris,SFDC and SL for web Customer Order with CC Payment in Hybris for Multi line
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'WebCustomerCC' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I clear field 'SecondProductReturnQty'
And I enter 'QtyProduct_2' in the feild 'SecondProductReturnQty' using actions
And I click 'SecondProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD2' by 'value'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'WebCustomerCC' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC03 #Multi line order
@AccountPaymentRRMA_QA
Scenario: RMA_TC03: Validate Return order process in Hybris,SFDC and SL for Corporate Order with Account Payment in Hybris for multi line and multiple quantity
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'AccountPayment' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option '2' in dropdown 'ReturnReasonDD' by 'index'
And I clear field 'SecondProductReturnQty'
And I enter 'QtyProduct_2' in the feild 'SecondProductReturnQty' using actions
And I click 'SecondProductBlock'
And I select option '2' in dropdown 'ReturnReasonDD2' by 'index'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'AccountPayment' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC04 #Multi line Multiple Quantity
@PrepayRRMA_QA
Scenario: RMA_TC04: Validate Return order process in Hybris,SFDC and SL for Prepay customer in Hybris for multi line and multiple quantity
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'PrepayOrder' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I clear field 'SecondProductReturnQty'
And I enter 'QtyProduct_2' in the feild 'SecondProductReturnQty' using actions
And I click 'SecondProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD2' by 'value'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'PrepayOrder' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC05 #Multi line order #
@CorporatePartialReturnRRMA_QA
Scenario: RMA_TC05: Validate Return order process in Hybris,SFDC and SL for Corporate Customer Order with CC Payment for Partial return when there are 2 lines ordered return 1 line
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'CorporatePartialShipping' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'CorporatePartialShipping' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'


#RMA_TC06 Multi Line Order
@PartialShippingRRMA_QA
Scenario: RMA_TC06: Validate Return order process in Hybris,SFDC and SL for Corporate Order for Partial Shipped items in SyteLine
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'PartialReturnOrder' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'PartialReturnOrder' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC07 #SingleLine
@WebCustomerSingleLineRRMA_QA 
Scenario: RMA_TC07: Validate Return process end to end when reason code is selected as Customer error in Return order page 
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'WebCustomerSingleLine' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'WebCustomerSingleLine' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC08 #Single Line
@AccountPaymentSingleLineRRMA_QA
Scenario: RMA_TC08: Validate Return process end to end when reason code is selected as Omega error in Return order page 
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'AccountPaymentSingleLine' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
#And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I select option '2' in dropdown 'ReturnReasonDD' by 'index'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'AccountPaymentSingleLine' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#TC_09 Multi Line Order
@CorporateCCMultiLineRRMA_QA
Scenario: RMA_TC09: Validate Return process end to end when reason code is combination of both Customer error and Omega error in Return order page
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'CoporateAccountMultilines' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I clear field 'SecondProductReturnQty'
And I enter 'QtyProduct_2' in the feild 'SecondProductReturnQty' using actions
And I click 'SecondProductBlock'
And I select option '2' in dropdown 'ReturnReasonDD2' by 'index'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'CoporateAccountMultiline' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#TC_10 Single Line
@RestockingFeeRRMA_QA
Scenario: RMA_TC10: Verify Restocking fee on item level is applied when invoice date and validate the Refund amount is in sync in all the integration systems
And I navigate to 'https://omega.okta.com/app/UserHome' application
And I wait '5' seconds for presence of element 'OmegaSDFC_Logo'
Then I should see element 'OmegaSDFC_Logo' present on page
Then I should see element 'SalesforceEmail' present on page
And I enter 'tpatel@omega.com' in field 'SalesforceEmail' 
And I click 'SalesforceNext'
Then I should see element 'SalesforcePassword' present on page
And I enter 'kiZC*ZuoxFA6' in field 'SalesforcePassword'
Then I should see element 'SalesforceVerify' present on page
And I click 'SalesforceVerify'
And I wait for '5' seconds
And I click 'Hybris_DEV'
And I wait for '10' seconds
And I switch to window with title 'hybris Backoffice'
And I should see element 'Hybris_logo' present on page
And I wait for '10' seconds
And I wait for visibility of element 'Hybris_orderOption'
And I should see element 'Hybris_orderOption' present on page
And I click 'Hybris_orderOption'
And I wait for '2' seconds
And I should see element 'Hybris_SelectOrders' present on page
And I click 'Hybris_SelectOrders'
And I wait for '10' seconds
And I wait for visibility of element 'Hybris_SearchField'
And I should see element 'Hybris_SearchField' present on page
And I enter 'OrderNumber' from 'RestockingFee' sheet to the field 'Hybris_SearchField'
And I wait for '5' seconds
And I press key: enter
And I wait for '5' seconds
And I should see element 'Hybris_FirstElement' present on page
And I click 'Hybris_FirstElement'
And I wait for '2' seconds
And I should see element 'Hybris_Positions' present on page
And I click 'Hybris_Positions'
And I clear field 'Hybris_PositionsDate'
And I get 60 days before given date and enter at 'Hybris_PositionsDate'
And I wait for '5' seconds
And I click 'Hybris_SaveButton'
And I wait for '2' seconds
And I click 'Hybris_rightButton'
And I click 'Hybris_rightButton'
And I should see element 'Hybris_ConsignmentsTab' present on page
And I click 'Hybris_ConsignmentsTab'
And I scroll through page
And I double-click 'Hybris_ShippingMethod'
And I should see element 'Hybris_ShippingMethodDate' present on page
And I clear field 'Hybris_ShippingMethodDate'
And I get 60 days before given date and enter at 'Hybris_ShippingMethodDate'
And I press key: enter
And I wait for '5' seconds
And I click 'Hybris_SaveConsigment'
Given My WebApp 'Omega' is open
And I navigate to ASM Application
When I click 'OmegaLogo'
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'RestockingFee' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I wait for '10' seconds
And I click 'FirstOrderNumber'
And I should see element 'ReturnOrderBtnOrderDetails' present on page
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I click 'ReturnOrderConfirmationBtn'
And I validate restocking fee validation 'ProductPrice_1' :Price 'QtyProduct_1' :Qty 'RestockingFeeAmount_1' :RestockingFee 'ProductTotal_1' :Total 
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I focus and click 'FirstElementReturnHistory'
And I get the details 'FirstElementReturnHistory' from 'RestockingFee' and store as 'RMA_ID'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

#RMA_TC11 Single line multiple Quantity
@VolumeDiscountsRRMA_QA
Scenario: RMA_TC11:Validate RMA process for Volume discount orders
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'VolumeDiscountsRMA' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'VolumeDiscountsRMA' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

@Ignore
Scenario: RMA_TC13: Verify below RMA emails are generated when RMA is processed
When I click 'OmegaLogo'
And I navigate to ASM Application
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter 'OrderNumber' from 'WebCustomerCC' sheet to the field 'SearchOrderNumber'
And I press key: enter
And I scroll to 'coordinates' - '0,250'
And I wait for '5' seconds
And I should see element 'FirstOrderNumber' present on page
And I focus and click 'FirstOrderNumber'
And I validate elements present on Order History page and check for return order button
And I click 'ReturnOrderBtnOrderDetails'
And I should see element 'ReturnOrderPageHeader' present on page
And I mouse over 'ReturnOrderConfirmationBtn'
And I should see element 'ReturnOrderConfirmationBtn' present on page_
Then I should see element 'ReturnOrderConfirmationBtn' is diabled on page
And I should see element 'ReturnOrderPageHeader' present on page
And I should see element 'RestockingFee_item1' present on page
And I should see element 'ProductPrice_1' present on page
And I should see element 'QtyProduct_1' present on page
And I scroll to 'coordinates' - '0,150'
And I should see element 'RestockingFee_item2' present on page
And I should see element 'ProductPrice_2' present on page
And I should see element 'QtyProduct_2' present on page
And I clear field 'FirstProductReturnQty'
And I enter 'QtyProduct_1' in the feild 'FirstProductReturnQty' using actions
And I click 'FirstProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD' by 'value'
And I clear field 'SecondProductReturnQty'
And I enter 'QtyProduct_2' in the feild 'SecondProductReturnQty' using actions
And I click 'SecondProductBlock'
And I select option 'OrderedInError' in dropdown 'ReturnReasonDD2' by 'value'
And I click 'ReturnOrderConfirmationBtn'
And I click 'SubmitRequestButton'
Then I should see element 'Acknowledgement' present on page
And I wait for '3' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
And I should see element 'ReturnHistory_sublink' present on page
And I click 'ReturnHistory_sublink'
And I wait for '5' seconds
And I should see element 'FirstElementReturnHistory' present on page
And I get the details 'FirstElementReturnHistory' from 'TestWeb' and store as 'RMA_ID'
And I focus and click 'FirstElementReturnHistory'
And I should see element 'ReturnDetails_Heading' present on page
And I should see element 'RMA_NumberReturnOrder' present on page
And I should see element 'RMA_ReturnStatus' present on page
And I should see element 'RMA_OrderNumber' present on page
And I should see element 'Order_lineItem' present on page
And I should see text 'RETURN LABEL' present on page at 'ReturnInstructionLabel'

Scenario: Wait till the Return Order is Displayed in Hybris
Given I wait for '700' seconds
And I load the excel sheet data to validate the Syteline