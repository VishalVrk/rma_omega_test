@DEV2-E2E
Feature: End to end Tax Exemption, Prepay order, Quote, Save Cart , Share Cart

#Scenario: TC-11:Verify the order details functionality for Item line discounts and ensure confirmation in corresponding Integrating System.
#Scenario: TC-12:Verify the order details functionality for order level discounts and ensure confirmation in corresponding Integrating System.

@TaxExemption
Scenario Outline: TC-8.0: Validate tax exemption is applied based on the customer/shipping address  is selected using ASM as Web customer
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter '4001ajc' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '1' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'

#Scenario :Validate tax exemption is applied based on the customer/shipping address  is selected using ASM as Web customer.
And I wait for '5' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I click 'TaxExemption'
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I get the details 'Customer_number' from 'ASMWCC' and store as 'CustomerNumber'
And I click 'Next_CartPage'
And I get the details 'PONumber_ASM' from 'ASMWCC' and store as 'PONumber'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I should see element 'Tax_ExemptionHeading' present on page
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see element 'OrderNumber' present on page_
And I should see element 'Tax_ExemptionHeading' present on page 	

#Scenario:Verify the order details after placing order using Tax Exempt customer and ensure confirmation in corresponding Integrating System.
And I should see element 'OrderNumber' present on page
And I should see element 'OrderPlacedBy' present on page
And I should see element 'OrderStatus' present on page
And I should see element 'DatePlaced' present on page
And I should see element 'Total' present on page
And I should see element 'ShipTo' present on page
And I should see element 'ShippingMethod' present on page
And I should see element 'ProductName' present on page
And I should see element 'ItemName' present on page
And I should see element 'UnitPrice' present on page_
And I should see element 'Quantity' present on page
And I should see element 'TotalPrice' present on page
And I should see element 'BillingAddress' present on page
And I should see element 'CCPaymentType' present on page
And I should see element 'OrderTotal' present on page

#Scenario: Validate order details and store in excel sheet
And I get the details 'OrderNumber' from 'ASMWCC' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'ASMWCC' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'ASMWCC' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'ASMWCC' and store as 'DatePlaced'
And I get the details 'Total' from 'ASMWCC' and store as 'Total'
And I get the details 'ShipTo' from 'ASMWCC' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'ASMWCC' and store as 'ShippingMethod'
And I get the details 'ProductName' from 'ASMWCC' and store as 'ProductName'
And I get the details 'ItemName' from 'ASMWCC' and store as 'ItemName'
And I get the details 'UnitPrice' from 'ASMWCC' and store as 'UnitPrice' 
And I get the details 'Quantity' from 'ASMWCC' and store as 'Quantity'
And I get the details 'TotalPrice' from 'ASMWCC' and store as 'TotalPrice'
And I get the details 'BillingAddress' from 'ASMWCC' and store as 'BillingAddress'
And I get the details 'CCPaymentType' from 'ASMWCC' and store as 'PaymentType'
And I get the details 'OrderTotal' from 'ASMWCC' and store as 'OrderTotal'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

@Prepay
Scenario Outline: TC-9.0:Verify order details after placing order via Prepay shipping methods UPS Worldwide Express and ensure confirmation in corresponding Integrating System.
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I wait for '5' seconds
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'
Then I should see element 'PrepayOption' present on page
Then I click 'PrepayOption'
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I get the details 'Customer_number' from 'PrepayOrder' and store as 'CustomerNumber'
And I click 'Next_CartPage'
And I get the details 'PONumber_ASM' from 'PrepayOrder' and store as 'PONumber'
And I select US from dropdown  
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2' 
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'OrderNumber' present on page_
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I get text from 'PlacedOrderNumber' and store
Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#Scenario:16 Verify order details after placing order via Ship on Account methods and ensure confirmation in corresponding Integrating System.
#Scenario:17 Verify order details after placing order via different Payment methods and ensure confirmation in corresponding Integrating System.

@Quote
Scenario: 10.0:Verify Quote functionality Quote Creation, Conversion to Order and validate in SFDC
#Scenario: TC-18.1:Verify product search functionality - Global Search
When I click 'OmegaLogo'
When I enter '4001ajc' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I wait for '5' seconds
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
#Scenario: 18.2:Verify Quote functionality Quote Creation, Conversion to Order and validate in SFDC
When I click 'OmegaLogo'
And I wait for '5' seconds
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Quote_Btn' present on page
And I click 'Quote_Btn'
Then I should see text 'This cart is being quoted' present on page at 'Quote_Title'
Then I get the value from Support Ticket page 'Quote_Name' and store as 'QuoteName'
And I click 'SubmitQuote_Btn'
Then I should see element 'Quote_Popup' present on page
Then I should see element 'Popup_Title' present on page
Then I should see element 'SubmitQuote_Yes_Btn' present on page
And I click 'SubmitQuote_Yes_Btn'
Then I should see element 'QuoteSuccess_Msg' present on page
And I click 'CreatedQuoteID'
Then I should see text 'Quote Details' present on page at 'QuoteDetailTitle'

#Scenario: TC-18.3:Verify that elements is present in Quote page and store in Excel Sheet
And I get the details 'QuoteNumber' from 'Quotes' and store as 'QuoteNumber'
And I get the details 'QuoteVersion' from 'Quotes' and store as 'QuoteVersion'
And I get the details 'QuoteStatus' from 'Quotes' and store as 'QuoteStatus'
And I get the details 'QuoteDateUpdated' from 'Quotes' and store as 'DateUpdated'
And I get the details 'QuoteDateCreated' from 'Quotes' and store as 'DateCreated'
And I get the details 'QuoteExpiryDate' from 'Quotes' and store as 'ExpiryDate'
And I get the details 'QuoteName' from 'Quotes' and store as 'Name'
And I get the details 'QuoteDescription' from 'Quotes' and store as 'Description'
And I get the details 'QuotePreviousEstimatedTotal' from 'Quotes' and store as 'PreviousEstimatedTotal'
And I get the details 'QuoteProductName1' from 'Quotes' and store as 'ProductName'
And I get the details 'QuoteItemName1' from 'Quotes' and store as 'ItemName'
And I get the details 'QuoteUnitPrice1' from 'Quotes' and store as 'Quantity'
And I get the details 'QuoteQuantity1' from 'Quotes' and store as 'UnitPrice'
And I get the details 'QuoteTotalPrice' from 'Quotes' and store as 'TotalPrice'


#@Ignore
#Scenario Outline: TC-18.5:Verify the payment method in Quote to Order functionality
##And I Zoom out
##And I wait for '5' seconds
###Then I should see element 'ConvertToOrder_Btn' present on page
##And I focus and click 'ConvertToOrder_Btn'
##And I click 'ConvertToOrder_Btn'
##And I Zoom in
##And I wait for '5' seconds
##Then I should see element 'Popup_Title' present on page
##Then I should see element 'ConvertQuoteToOrder_Yes_Btn' present on page
##And I click 'ConvertQuoteToOrder_Yes_Btn'
#Then I should see element 'CardPayment' present on page
#And I enter '12345' in field 'POnumberFeild'
#Then I click 'Next_CartPage'
#And I click 'Next_CartPage'
#And I select US from dropdown 
#And I enter name in field 'FirstName_Cart'
#And I enter '<LN_Cart>' in field 'LastName_Cart'
#And I enter '<CN_Cart>' in field 'CompanyName_Cart'
#And I enter '<AddL1>' in field 'AddressLine1'
#And I enter '<AddL2>' in field 'AddressLine2'
#And I enter '<Town_City>' in field 'Town_City'
#And I enter '<Pin>' in field 'PostalCode_Cart'
#And I enter '<Phone>' in field 'Phone_Cart'
#And I click 'SaveShippingAddress'
#When I click 'Next_CartPage'
#And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
#And I click 'Next_CartPage'
#And I should see text 'Payment Information' present on page
#And I enter '4444333322221111' in field 'CardNumber'
#And I enter 'Test' in field 'Card_FirstName'
#And I enter 'Test' in field 'Card_LastName'
#And I select option '08' in dropdown 'MonthDD' by 'text'
#And I select option '2022' in dropdown 'YearDD' by 'text'
#And I enter '123' in field 'CVV_Number'
#And I enter '60605-2902' in field 'Zipcode'
#And I click 'Next_CartPage'
#And I scroll to 'coordinates' - '0,150'
#And I click 'Term_Accept'
#And I click 'PlaceOrder'
#Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
#
#And I get the details 'OrderNumber' from 'Quote2Order' and store as 'OrderNumber'
#And I get the details 'OrderPlacedBy' from 'Quote2Order' and store as 'OrderPlacedBy'
#And I get the details 'OrderStatus' from 'Quote2Order' and store as 'OrderStatus'
#And I get the details 'DatePlaced' from 'Quote2Order' and store as 'DatePlaced'
#And I get the details 'Total' from 'Quote2Order' and store as 'Total'
#And I get the details 'ShipTo' from 'Quote2Order' and store as 'ShipTo'
#And I get the details 'ShippingMethod' from 'Quote2Order' and store as 'ShippingMethod'
#And I get the details 'ProductName' from 'Quote2Order' and store as 'ProductName'
#And I get the details 'ItemName' from 'Quote2Order' and store as 'ItemName'
#And I get the details 'UnitPrice' from 'Quote2Order' and store as 'UnitPrice'
#And I get the details 'Quantity' from 'Quote2Order' and store as 'Quantity'
#And I get the details 'TotalPrice' from 'Quote2Order' and store as 'TotalPrice'
#And I get the details 'BillingAddress' from 'Quote2Order' and store as 'BillingAddress'
#And I get the details 'CCPaymentType' from 'Quote2Order' and store as 'PaymentType'
#And I get the details 'OrderTotal' from 'Quote2Order' and store as 'OrderTotal'
#
#Examples:
#|LN_Cart |CN_Cart |AddL1		           |Town_City	    |Pin	  |Phone	     	|
#|Test	 |RC	  |1410 S Museum Campus Dr |Chicago 	    |60605    |705-750-8910	    |

@Saved_Cart
Scenario: TC-11.0:Verify Saved Cart functionality
Given I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_click'
And I click 'Product'
And I click 'AddToCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I wait for '2' seconds
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '5' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
Then I should see element 'SaveThisCart' present on page
And I click 'SaveThisCart'
And I enter 'savecart1' in field 'SaveCartName'
And I enter 'Testing_CN8592-R1-T2_Product' in field 'saveCartDescription'
And I click 'SaveCartButton'

@Share_Cart
Scenario: TC-12.0:Verify Share cart Functionality
When I click 'OmegaLogo'
When I enter '4001ajc' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
And I scroll to 'coordinates' - '0,150'
And I click 'EmailCartIcon'
And I enter 'test' in field 'ShareCartName'
And I enter 'vishal.rk@royalcyber.com' in field 'shareReceiverEmail'
And I enter 'test' in field 'sahreCartDescription'
And I click 'shareCartButton'
Then I should see element 'Acknowledgement' present on page