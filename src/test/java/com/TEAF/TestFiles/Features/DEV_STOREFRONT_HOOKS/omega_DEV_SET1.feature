@DEV1-E2E
Feature: End to end Customer registration, Global search, Cost center , Credit Application , Quick order search, Forgot Password, Web Customer CC.

@Resgisterfun @DEV1-E2E
Scenario: TC-1.0:Validate Web account Customer registration in Hybris and ensure confirmation in corresponding Integrating System
Given My WebApp 'Omega' is open
Then I should see element 'OmegaLogo' present on page
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'SignIn_Btn' present on page
Then I should see element 'Register_1' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page_
Then I should see element 'Banner' present on page_
Then I should see element 'QuickOrderQuote' present on page_
Then I should see element 'WhatsNewSection' present on page_
#Scenario: TC-1.1:Validate Web account Customer registration in Hybris
When I click 'OmegaLogo'
Then I should see element 'Register_1' present on page
When I click 'Register_1'
Then I should see element 'First_Name_Field' present on page
Then I should see element 'Last_Name_Field' present on page
Then I should see element 'Email_ID_Field' present on page
Then I should see element 'Phone_Num_Field' present on page
Then I should see element 'Phone_Ext_Field' present on page
Then I should see element 'Reg_Password_Field' present on page
Then I should see element 'Register_Btn' present on page
When  I clear field 'First_Name_Field'
And I enter 'Register' : 'FirstName' from spreadsheet in the feild 'First_Name_Field'
And I clear field 'Last_Name_Field'
And I enter 'Register' : 'LastName' from spreadsheet in the feild 'Last_Name_Field'
And I register 'Email_ID_Field' 
And I enter 'Register' : 'MobileNumber' from spreadsheet in the feild 'Phone_Num_Field'
And I enter 'Register' : 'PassWord' from spreadsheet in the feild 'Reg_Password_Field'
And I click 'Register_Btn'
Then I should see text 'Welcome Vishal VK' present on page at 'Welcome'
#Scenario: TC-1.2:Verify the register user is able to sign-out
And I click 'OmegaLogo'
And I wait for '5' seconds
And I scroll to 'coordinates' - '0,150'
And I click 'MyAccount'
And I click 'Sign_Out'
Then I should see element 'SignIn_Btn' present on page

@Searchfun @DEV1-E2E
Scenario Outline: TC-2.0:Verify product search functionality - Global Search and Cost Center validation using Account Payment as Corporate Customer
When I click 'OmegaLogo'
When I enter 'T-FER-1/16' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'

#Scenario Outline: TC-2.2:Verify place order functionality for Corporate customer using Account Payment method
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '5' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
Then I click 'AccountPaymentType'
Then I should see element 'CostCenter' present on page_
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I should see element 'Customer_number' present on page
And I get the details 'Customer_number' from 'OrderConfirmationAccountPayment' and store as 'CustomerNumber'
And I click 'Next_CartPage'
And I get the details 'PONumber' from 'AccountPayment' and store as 'PONumber'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter 'AddL2' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I should see element 'OrderNumber' present on page_

#Scenario: : Validate the elements in the Order Summary Page
And I should see element 'OrderNumber' present on page
And I should see element 'OrderPlacedBy' present on page
And I should see element 'OrderStatus' present on page
And I should see element 'DatePlaced' present on page
And I should see element 'Total' present on page
And I should see element 'ShipTo' present on page
And I should see element 'ShippingMethod' present on page
And I scroll to 'coordinates' - '0,250'
And I should see element 'ProductName' present on page
And I should see element 'ItemName' present on page
And I should see element 'UnitPrice' present on page
And I should see element 'Quantity' present on page
And I should see element 'TotalPrice' present on page
And I should see element 'BillingAddress' present on page
And I should see element 'OrderTotal' present on page

#Scenario: Validate the details from Order Summary Page and Store it in Excel Sheet
And I get the details 'OrderNumber' from 'AccountPayment' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'AccountPayment' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'AccountPayment' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'AccountPayment' and store as 'DatePlaced'
And I get the details 'Total' from 'AccountPayment' and store as 'Total'
And I get the details 'ShipTo' from 'AccountPayment' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'AccountPayment' and store as 'ShippingMethod'
And I get the details 'ProductName' from 'AccountPayment' and store as 'ProductName'
And I get the details 'ItemName' from 'AccountPayment' and store as 'ItemName'
And I get the details 'UnitPrice' from 'AccountPayment' and store as 'UnitPrice'
And I get the details 'Quantity' from 'AccountPayment' and store as 'Quantity'
And I get the details 'TotalPrice' from 'AccountPayment' and store as 'TotalPrice'
And I get the details 'BillingAddress' from 'AccountPayment' and store as 'BillingAddress'
And I get the details 'OrderTotal' from 'AccountPayment' and store as 'OrderTotal'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |


@CreditAppFun @DEV1-E2E
Scenario: TC-3.0:Verify that User is able to apply for Corporate Account through Credit Application web form available in Hybris and verify the details in SFDC
And I click 'OmegaLogo'
And I click 'ApplyForCredit'
Then I clear field 'BillTo_EmailInvoice'
And I enter 'CreditApplication' : 'BillToEmailId' from spreadsheet in the feild 'BillTo_EmailInvoice'
Then I clear field 'EmailID_forStatements'
And I enter 'CreditApplication' : 'StatementEmailId' from spreadsheet in the feild 'EmailID_forStatements'
Then I clear field 'Bill_Address1'
And I enter 'CreditApplication' : 'Address1' from spreadsheet in the feild 'Bill_Address1'
Then I clear field 'City'
And I enter 'CreditApplication' : 'City' from spreadsheet in the feild 'City'
And I select Albama from dropdown
Then I clear field 'Zip/PostalCode'
And I enter 'CreditApplication' : 'ZipCode' from spreadsheet in the feild 'Zip/PostalCode'
Then I clear field 'CompanyName'
And I enter 'CreditApplication' : 'CompanyName' from spreadsheet in the feild 'CompanyName'
Then I clear field 'AccPayableContact'
And I enter 'CreditApplication' : 'AccountPayableContact' from spreadsheet in the feild 'AccPayableContact'
Then I clear field 'AccPayablePhone'
And I enter 'CreditApplication' : 'AccountPayablePhone' from spreadsheet in the feild 'AccPayablePhone'
Then I clear field 'CompanymainPhone'
And I enter 'CreditApplication' : 'CompanyMainPhone' from spreadsheet in the feild 'CompanymainPhone'
Then I clear field 'PartnerName1'
And I enter 'CreditApplication' : 'PartnerName1' from spreadsheet in the feild 'PartnerName1'
Then I clear field 'PartnerName2'
And I enter 'CreditApplication' : 'PartnerName2' from spreadsheet in the feild 'PartnerName2'
Then I clear field 'RequestedAmount'
And I enter 'CreditApplication' : 'RequestedAmount' from spreadsheet in the feild 'RequestedAmount'
Then I clear field 'Reference1_Name'
And I enter 'CreditApplication' : 'ReferenceName' from spreadsheet in the feild 'Reference1_Name'
Then I clear field 'Reference1_Phone'
And I enter 'CreditApplication' : 'ReferencePhone' from spreadsheet in the feild 'Reference1_Phone'
Then I clear field 'Reference1_Address'
And I enter 'CreditApplication' : 'ReferenceAddress' from spreadsheet in the feild 'Reference1_Address'
Then I clear field 'Reference2_Name'
And I enter 'CreditApplication' : 'Reference2Name' from spreadsheet in the feild 'Reference2_Name'
Then I clear field 'Reference2_Phone'
And I enter 'CreditApplication' : 'Reference2Phone' from spreadsheet in the feild 'Reference2_Phone'
Then I clear field 'Reference2_Address'
And I enter 'CreditApplication' : 'Reference2Address' from spreadsheet in the feild 'Reference2_Address'
Then I clear field 'Reference3_Name'
And I enter 'CreditApplication' : 'Reference3Name' from spreadsheet in the feild 'Reference3_Name'
Then I clear field 'Reference3_Phone'
And I enter 'CreditApplication' : 'Reference3Phone' from spreadsheet in the feild 'Reference3_Phone'
Then I clear field 'Reference3_Address'
And I enter 'CreditApplication' : 'Reference3Address' from spreadsheet in the feild 'Reference3_Address'
And I select option '1' in dropdown 'State' by 'index'
And I select option '1' in dropdown 'YearInBusiness' by 'index'
And I select option '1' in dropdown 'BusinessType' by 'index'
Then I wait for '5' seconds
And I scroll to 'coordinates' - '0,100'
And I click by JS 'Checkbox_ApplyForCredit'
And I click 'Submit_Button'
And I wait for '5' seconds
And I click 'MyAccount'
And I click 'Dropdown_MyAcc'
And I click 'SupportTickets'


@QuickOrderfun @DEV1-E2E
Scenario Outline: TC-4.0:Verify product search functionality - Quick Order Search through 2 ways using credit card payment method
And I click 'OmegaLogo'
Then I should see element 'QuickOrder_Header' present on page
And I click 'QuickOrder_Header' 
Then I should see element 'SKU_field' present on page
When I enter 'T-FER-1/16' in field 'SKU_field'
And I press enter key
And I clear field 'QuickOrderQuoteUpdateQty'
And I clear the text and enter '2' in field 'QuickOrderQuoteUpdateQty'
And I click 'QuickOrderQuote_AddtoCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I wait for '3' seconds
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter '4001ajc' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '1' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
And I wait for '5' seconds

# Scenario Outline : Verify place order functionality for Corporate customer using Credit Card Payment method
And I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '4' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
#And I click 'SaveShippingAddress'
And I click 'Next_CartPage'
#And I wait '2' seconds for presence of element 'VerifyAddress_PopUp'
#Then I should see element 'VerifyAddress_PopUp' present on page
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I scroll to 'coordinates' - '0,150'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,250'
And I enter 'Test' in field 'DeliveryAttention'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
#add one step
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I wait for '15' seconds
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'


#Scenario: Validate the elements in the Order Summary Page
And I should see element 'OrderNumber' present on page
And I should see element 'OrderPlacedBy' present on page
And I should see element 'OrderStatus' present on page
And I should see element 'DatePlaced' present on page
And I should see element 'Total' present on page
And I should see element 'ShipTo' present on page
And I should see element 'ShippingMethod' present on page
And I should see element 'ProductName1' present on page
And I should see element 'ItemName1' present on page
And I should see element 'UnitPrice1' present on page
And I should see element 'Quantity1' present on page
And I should see element 'TotalPrice1' present on page
And I should see element 'ProductName2' present on page
And I should see element 'ItemName2' present on page
And I should see element 'UnitPrice2' present on page
And I should see element 'Quantity2' present on page
And I should see element 'TotalPrice2' present on page
And I should see element 'BillingAddress' present on page
And I should see element 'CCPaymentType' present on page
And I should see element 'OrderTotal' present on page

#Scenario: TC-8.4: Validate order details and store in excel sheet
And I get the details 'OrderNumber' from 'CorporateCC' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'CorporateCC' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'CorporateCC' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'CorporateCC' and store as 'DatePlaced'
And I get the details 'Total' from 'CorporateCC' and store as 'Total'
And I get the details 'ShipTo' from 'CorporateCC' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'CorporateCC' and store as 'ShippingMethod'
And I get the details 'ProductName1' from 'CorporateCC' and store as 'ProductName1'
And I get the details 'ItemName1' from 'CorporateCC' and store as 'ItemName1'
And I get the details 'UnitPrice1' from 'CorporateCC' and store as 'UnitPrice1'
And I get the details 'Quantity1' from 'CorporateCC' and store as 'Quantity1'
And I get the details 'TotalPrice1' from 'CorporateCC' and store as 'TotalPrice1'
And I get the details 'ProductName2' from 'CorporateCC' and store as 'ProductName2'
And I get the details 'ItemName2' from 'CorporateCC' and store as 'ItemName2'
And I get the details 'UnitPrice2' from 'CorporateCC' and store as 'UnitPrice2'
And I get the details 'Quantity2' from 'CorporateCC' and store as 'Quantity2'
And I get the details 'TotalPrice2' from 'CorporateCC' and store as 'TotalPrice2'
And I get the details 'BillingAddress' from 'CorporateCC' and store as 'BillingAddress'
And I get the details 'CCPaymentType' from 'CorporateCC' and store as 'PaymentType'
And I get the details 'OrderTotal' from 'CorporateCC' and store as 'OrderTotal'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

@DEV1-E2E
Scenario: TC-5.0:Verify Forgot Password functionality.
And I click 'SignIn_Btn'
And I click 'ForgotPassword'
And I enter 'taha.patel1234@royalcyber.com' in field 'ForgotPasswordEmail'
And I click 'resetPwd'
And I click 'closeIcon'



@Webuserccpaymentfun @DEV1-E2E
Scenario Outline: TC-6.0: Verify product search functionality - Global Search
When I click 'OmegaLogo'
When I enter '4001ajc' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
And I should see element 'AddedToCart' present on page_
And I wait for '2' seconds
And I wait for element present 'AddedText'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
When I click 'Checkout'

#Scenario : Verify place order with CC payment Web Customer
And I wait for '5' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'

#Scenario: TC-9.3: Validate Order details from Order Summary Page
And I should see element 'OrderNumber' present on page
And I should see element 'OrderPlacedBy' present on page
And I should see element 'OrderStatus' present on page
And I should see element 'DatePlaced' present on page
And I should see element 'Total' present on page
And I should see element 'ShipTo' present on page
And I should see element 'ShippingMethod' present on page
And I should see element 'ProductName' present on page
And I should see element 'ItemName' present on page
And I should see element 'UnitPrice' present on page
And I should see element 'Quantity' present on page
And I should see element 'TotalPrice' present on page
And I should see element 'BillingAddress' present on page
And I should see element 'CCPaymentType' present on page
And I should see element 'OrderTotal' present on page

#Scenario: TC-9.4: Validate order details and store in excel sheet
And I get the details 'OrderNumber' from 'WebCustomerCC' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'WebCustomerCC' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'WebCustomerCC' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'WebCustomerCC' and store as 'DatePlaced'
And I get the details 'Total' from 'WebCustomerCC' and store as 'Total'
And I get the details 'ShipTo' from 'WebCustomerCC' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'WebCustomerCC' and store as 'ShippingMethod'
And I get the details 'ProductName' from 'WebCustomerCC' and store as 'ProductName'
And I get the details 'ItemName' from 'WebCustomerCC' and store as 'ItemName'
And I get the details 'UnitPrice' from 'WebCustomerCC' and store as 'UnitPrice'
And I get the details 'Quantity' from 'WebCustomerCC' and store as 'Quantity'
And I get the details 'TotalPrice' from 'WebCustomerCC' and store as 'TotalPrice'
And I get the details 'BillingAddress' from 'WebCustomerCC' and store as 'BillingAddress'
And I get the details 'CCPaymentType' from 'WebCustomerCC' and store as 'PaymentType'
And I get the details 'OrderTotal' from 'WebCustomerCC' and store as 'OrderTotal'


Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

@Contactregisfun @DEV1-E2E
Scenario: TC-7.0:Verify that User is able to submit the Contact Us form in Hybris and able to see details in SFDC
And I click 'OmegaLogo'
And I click 'Contact_Us'
And I enter 'Taha' in field 'FirstName'
And I enter 'Patel' in field 'LastName'
And I enter 'RC' in field 'Contactus_CompanyName'
And I select Quality from dropdown
And I enter '123456789' in field 'PhoneNo'
And I enter 'Test' in field 'ContactUs_City' 
And I enter 'Test' in field 'ContactUs_Address'
And I select option '1' in dropdown 'ContactUs_State' by 'index'
And I enter '60353' in field 'Zip/PostalCode'
And I enter 'Contactus' in field 'Subject'
And I get the value from Support Ticket page 'Subject' and store as 'SubjectName'
And I enter 'Testing' in field 'Description'
And I remove Captcha element
And I click 'Submit_Button'
And I wait for element present 'MyAccount'
And I wait for '2' seconds
And I click 'MyAccount'
And I click 'Dropdown_MyAcc'
And I click 'SupportTickets'
