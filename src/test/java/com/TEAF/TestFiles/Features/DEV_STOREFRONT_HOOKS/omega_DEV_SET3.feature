@DEV3-E2E
Feature: End to end CPQ order , Place order using ASM , Cancel Order, Mark as Govt , Bread Crumbs , Email Cart

@ConfigureProduct @DEV3-E2E
Scenario Outline: TC-13.0:Verify configure functionality Creation of configure product, Placing order and integrations in SFDC, Syteline
When I click 'OmegaLogo'
When I enter 'sa1e' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'Customize_Button'
And I switch to iFrame Configure Product
And I wait '5' seconds for presence of element 'JType'
And I should see element 'JType' present on page
And I wait for '5' seconds
And I click 'JType'
And I wait '5' seconds for presence of element '72InElement'
And I should see element '72InElement' present on page
And I wait for '5' seconds
And I click '72InElement'
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'M8_plug'
And I should see element 'M8_plug' present on page
And I wait for '5' seconds
And I click 'M8_plug'
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'CPQ_None'
And I should see element 'CPQ_None' present on page
And I wait for '5' seconds
And I click 'CPQ_None'
And I wait for '2' seconds
And I wait '5' seconds for presence of element 'NextButton_CPQ'
And I should see element 'NextButton_CPQ' present on page
Then I wait for '5' seconds
And I click 'NextButton_CPQ'
And I wait '5' seconds for presence of element 'Save_Config'
And I should see element 'Save_Config' present on page
And I click 'Save_Config'
And I switch to back to default content
And I wait for '2' seconds
Then I should see element 'SavedConfigurationHeader' present on page
And I wait for '2' seconds
When I click 'ConfigurePopup_AddtoCart'
And I wait for '2' seconds
And I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I wait for element present 'QuickOrder_ContinueShopping'
And I click 'QuickOrder_ContinueShopping'
#Scenario Outline: TC-21.2:Verify configure functionality Creation of configure product, Placing order and integrations in SFDC, Syteline
And I wait for '5' seconds
And I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '3' seconds
And I click 'Checkout_MiniCart'
And I wait for '2' seconds
Then I should see element 'Checkout' present on page
And I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I click 'CardPayment'
And I clear the text and enter '123ew' in field 'PO_Num'
And I click 'Next_CartPage'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
#And I enter '<FN_Cart>' in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I select option 'Illinois' in dropdown 'StateDD_Cart' by 'text'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
#And I click 'SaveShippingAddress'
And I click 'Next_CartPage'
#Then I should see element 'VerifyAddress_PopUp' present on page
And I wait for '2' seconds
And I scroll to 'coordinates' - '0,150'
When I click 'SubmitAsIs'
And I enter 'Test' in field 'DeliveryAttention'
And I select option '2' in dropdown 'DeliveryMethodDD' by 'index'
And I wait for '2' seconds
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'OrderNumber' present on page_
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'

#Scenario: TC-21.3:Validate order details and store in excel sheet
And I get the details 'OrderNumber' from 'ConfigOrder' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'ConfigOrder' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'ConfigOrder' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'ConfigOrder' and store as 'DatePlaced'
And I get the details 'Total' from 'ConfigOrder' and store as 'Total'
And I get the details 'ShipTo' from 'ConfigOrder' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'ConfigOrder' and store as 'ShippingMethod'
And I get the details 'ProductName' from 'ConfigOrder' and store as 'ProductName'
And I get the details 'ItemName' from 'ConfigOrder' and store as 'ItemName'
And I get the details 'UnitPrice' from 'ConfigOrder' and store as 'UnitPrice'
And I get the details 'Quantity' from 'ConfigOrder' and store as 'Quantity'
And I get the details 'TotalPrice' from 'ConfigOrder' and store as 'TotalPrice'
And I get the details 'BillingAddress' from 'ConfigOrder' and store as 'BillingAddress'
And I get the details 'CCPaymentType' from 'ConfigOrder' and store as 'PaymentType'
And I get the details 'OrderTotal' from 'ConfigOrder' and store as 'OrderTotal'

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

@WebCustomer_ASM @DEV3-E2E
Scenario Outline: TC-14.0:Verify Web Customer is able Place Order using ASM
When I navigate to asm application
Then I should see element 'OmegaLogo' present on page
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page_
#Scenario: TC-22.1: Verify if the user is able to sign-in into ASM agent
Then I should see element 'ASM_Logo' present on page_
Then I should see element 'Asagent_username' present on page
Then I should see element 'Asagent_password' present on page
Then I clear field 'Asagent_username'
When I enter 'asagent' in field 'Asagent_username'
And I clear field 'Asagent_password'
And I enter '123456' in field 'Asagent_password'
And I click 'Asagent_SignIn'
#Scenario: TC-22.3: Verify if the user is able to add item in the cart by home page quick-order 
And I wait for '5' seconds
When I click 'OmegaLogo'
Then I should see element 'QuickOrder_Partnumfield1' present on page
And I wait for '3' seconds
When I enter '4001ajc' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '1' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'
#Scenario Outline:TC-22.4:Verify Web Customer is able Place Order using ASM
And I wait for '5' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '5' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I wait for '5' seconds
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
And I click 'Term_Accept'
And I click 'PlaceOrder'
And I should see element 'OrderNumber' present on page_
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'
And I get text from 'PlacedOrderNumber' and store
#Scenario: TC-22.5: Validate order details and store in excel sheet
And I get the details 'OrderNumber' from 'ASMCAP' and store as 'OrderNumber'
And I get the details 'OrderPlacedBy' from 'ASMCAP' and store as 'OrderPlacedBy'
And I get the details 'OrderStatus' from 'ASMCAP' and store as 'OrderStatus'
And I get the details 'DatePlaced' from 'ASMCAP' and store as 'DatePlaced'
And I get the details 'Total' from 'ASMCAP' and store as 'Total'
And I get the details 'ShipTo' from 'ASMCAP' and store as 'ShipTo'
And I get the details 'ShippingMethod' from 'ASMCAP' and store as 'ShippingMethod'
And I get the details 'ProductName' from 'ASMCAP' and store as 'ProductName'
And I get the details 'ItemName' from 'ASMCAP' and store as 'ItemName'
And I get the details 'UnitPrice' from 'ASMCAP' and store as 'UnitPrice'
And I get the details 'Quantity' from 'ASMCAP' and store as 'Quantity'
And I get the details 'TotalPrice' from 'ASMCAP' and store as 'TotalPrice'
And I get the details 'BillingAddress' from 'ASMCAP' and store as 'BillingAddress'
#Scenario: TC-22.1: Verify if the user is able to sign-in into ASM agent
And I wait for '5' seconds
When I click 'OmegaLogo'
And I wait for '5' seconds
And I click 'MyAccount'
And I should see element 'OrderHistorySubMenu' present on page
And I click 'OrderHistorySubMenu'
When I enter '$$PlacedOrderNumber' in field 'SearchOrderNumber'
Then I should see text '$$PlacedOrderNumber' contained on page at 'FirstProduct'
And I press key: enter
And I wait for '5' seconds
#change locator
And I scroll to 'coordinates' - '0,150'
And I wait for '5' seconds
And I click 'FirstOrderNumber'
Then I should see element 'CancelOrderBtn' present on page
And I click 'CancelOrderBtn'
And I click 'CancelConfirmationBtn'
Then I should see element 'CancelReasonDD' present on page
And I select option 'LateDelivery' in dropdown 'CancelReasonDD' by 'value'
And I click 'SubmitCancelOrder'
Then I should see text 'Request for cancellation submitted successfully' present on page

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#@Cancel_Order @DEV3-E2E
#Scenario: TC-15.0:Verify Cancel Order functionality through ASM and validate in SFDC & Syteline



@Mark_as_Govt @DEV3-E2E
Scenario Outline: TC-15.0:Verify Web Customer is able to mark Order as ''Mark as Govt" Order using ASM view and ensure confirmation in corresponding integration system
When I click 'OmegaLogo'
When I navigate to asm application
Then I should see element 'OmegaLogo' present on page
Then I should see element 'AllProducts_Menu' present on page
Then I should see element 'ResouRces_Menu' present on page
Then I should see element 'Banner' present on page
Then I should see element 'QuickOrder_Text' present on page_
#Scenario: TC-22.1: Verify if the user is able to sign-in into ASM agent
Then I should see element 'ASM_Logo' present on page_
Then I should see element 'Asagent_username' present on page
Then I should see element 'Asagent_password' present on page
Then I clear field 'Asagent_username'
When I enter 'asagent' in field 'Asagent_username'
And I clear field 'Asagent_password'
And I enter '123456' in field 'Asagent_password'
And I click 'Asagent_SignIn'
And I wait for '3' seconds
When I enter '4001ajc' in field 'QuickOrder_Partnumfield1'
And I wait for '3' seconds
And I get text from 'QuickOrder_Partnumfield1' and store
And I click 'QuickOrder_Qtyfield1'
And I enter '1' in field 'QuickOrder_Qtyfield1'
And I click 'QuickOrder_Add2CartBtn'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
And I click 'QuickOrder_ContinueShopping'

#Scenario Outline: TC-24.1:Verify Web Customer is able to mark Order as ''Mark as Govt" Order using ASM view and ensure confirmation in corresponding integration system
And I wait for '5' seconds
When I click 'OmegaLogo'
And I click 'Minicart'
And I should see element 'Minicart' present on page
And I scroll to 'coordinates' - '0,150'
And I wait for '5' seconds
And I click 'Checkout_MiniCart'
Then I should see element 'Checkout' present on page
When I click 'Checkout'
Then I should see element 'CardPayment' present on page
And I click 'TaxExemption'
And I enter '12345' in field 'POnumberFeild'
Then I click 'Next_CartPage'
And I click 'Next_CartPage'
And I select US from dropdown 
And I enter name in field 'FirstName_Cart'
And I enter '<LN_Cart>' in field 'LastName_Cart'
And I enter '<CN_Cart>' in field 'CompanyName_Cart'
And I enter '<AddL1>' in field 'AddressLine1'
And I enter '<AddL2>' in field 'AddressLine2'
And I enter '<Town_City>' in field 'Town_City'
And I enter '<Pin>' in field 'PostalCode_Cart'
And I enter '<Phone>' in field 'Phone_Cart'
And I click 'SaveShippingAddress'
When I click 'Next_CartPage'
And I select option '1' in dropdown 'DeliveryMethodDD' by 'index'
And I click 'Next_CartPage'
And I should see text 'Payment Information' present on page
And I enter '4444333322221111' in field 'CardNumber'
And I enter 'Test' in field 'Card_FirstName'
And I enter 'Test' in field 'Card_LastName'
And I select option '08' in dropdown 'MonthDD' by 'text'
And I select option '2022' in dropdown 'YearDD' by 'text'
And I enter '123' in field 'CVV_Number'
And I enter '60605-2902' in field 'Zipcode'
And I click 'Next_CartPage'
And I scroll to 'coordinates' - '0,150'
#And I should see element 'Tax_ExemptionHeading' present on page
And I click 'Term_Accept'
And I click 'GovtCheckBox'
And I click 'PlaceOrder'
Then I should see text 'Thank you for your Order!' contained on page at 'Success_Header'	

Examples:
|LN_Cart  |CN_Cart |AddL1	                |Town_City	    |Pin	  |Phone	     |
|Test	  |RC	   |55 Shuman Blvd Ste 275  |Naperville 	|60563    |705-750-8910	 |

#Scenario: TC-25.0:Validate new contact registration in Salesforce, place order in Hybris and ensure confirmation in corresponding Integrating System
#Scenario: TC-26:Validate Chat functionality between the Sales Agent &  customer

@DEV3-E2E
Scenario: TC-16:Verify Category navigation L1, L2 , L3 and L4 and Verify Bread crumbs in PDP
And I click 'OmegaLogo'
And I click 'Allproducts_Menu'
And I mouse over 'ProcessSwitches'
And I mouse over 'LevelSwitches'
And I click 'FloatSwitches'
And I should see element 'ActiveBreadcrumb' present on page
And I should see element 'BreadcrumbSecoundItem' present on page
And I should see element 'BreadcrumbFirstItem' present on page
And I should see element 'Home_breadcrumb' present on page_ 

@DEV3-E2E
Scenario: TC-17:Verify Email Cart functionality for Corporate Customer
When I click 'OmegaLogo'
When I enter '4001ajc' in field 'TextBox_Search'
And I click 'Search_Icon'
And I click 'Product'
And I click 'AddToCart'
Then I should see text 'Added to Your Shopping Cart' contained on page at 'AddedText'
When I click 'CheckoutBtnCartPopUp'
Then I should see text 'Cart' present on page at 'CartPageHeading'
And I scroll to 'coordinates' - '0,150'
And I click 'EmailCartIcon'
And I enter 'test' in field 'ShareCartName'
And I enter 'vishal.rk@royalcyber.com' in field 'shareReceiverEmail'
And I enter 'test' in field 'sahreCartDescription'
And I click 'shareCartButton'
Then I should see element 'Acknowledgement' present on page
