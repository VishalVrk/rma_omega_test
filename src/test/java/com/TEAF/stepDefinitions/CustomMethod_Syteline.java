package com.TEAF.stepDefinitions;

import org.apache.commons.lang3.StringUtils;
import org.junit.ComparisonFailure;
import org.openqa.selenium.WebDriver;

import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import groovy.util.logging.Commons;
import junit.framework.Assert;

public class CustomMethod_Syteline {

	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();

	@Then("I check item '(.*)' has been shiped")
	public static void i_check_item_changed_as_quantity(String sheetname,String columnname,String element) throws Exception {

		String Quantity = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname,columnname,"StoreFront");
		Quantity = Quantity+".000";
		System.out.println("Found Quantity: "+ Quantity);
		Omega_CustomMethods.i_should_see_value_from_input_field(Quantity,element);
	}

	@When("^I enter tracking number and ship the order$")
	public static void i_enter_trackingnumber_and_shipthe_order() {
		CommonSteps.I_pause_for_seconds(10);
		CommonSteps.I_focus_click("TrackingNumber_SL");
		CommonSteps.I_enter_in_field("test", "TrackingNumber_SL");
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_click("ShipButton_SL");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_wait_for_visibility_of_element("OrderShipping_SL");
		CommonSteps.I_click("OrderShipping_SL");
	}

	@When("^I enter customer number and process invocing for the order '(.*)'$")
	public static void i_enter_customernumber_process_invocing_the_order(String sheetname) throws Exception {
		CommonSteps.I_focus_click("OrderShippingStartingCustomer_SL");
		Omega_CustomMethods.i_enter_values_in_the_spreasheet(sheetname, "CustomerID",
				"OrderShippingStartingCustomer_SL");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_focus_click("OrderShippingEndingCustomer_SL");
		Omega_CustomMethods.i_enter_values_in_the_spreasheet(sheetname, "CustomerID",
				"OrderShippingEndingCustomer_SL");
		CommonSteps.I_focus_click("Process_SL");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_wait_for_visibility_of_element("PrintOrderInvoicing_SL");
		CommonSteps.I_should_see_on_page("PrintOrderInvoicing_SL");
		CommonSteps.I_focus_click("PrintOrderInvoicing_SL");
		CommonSteps.I_pause_for_seconds(15);
		CommonSteps.I_wait_for_visibility_of_element("OrderInvoicing_okbtn_SL");
		CommonSteps.I_focus_click("OrderInvoicing_okbtn_SL");
		CommonSteps.I_pause_for_seconds(5);

	}

	@When("^I open form and search '(.*)' & select '(.*)' then I should see '(.*)'$")
	public static void i_open_form_search_filter(String searchText, String selectSearch, String searchResult)
			throws Exception {
		Thread.sleep(5000);
		CommonSteps.I_should_see_on_page("OpenForm");
		CommonSteps.I_focus_click("OpenForm");
		CommonSteps.I_pause_for_seconds(3);
		CommonSteps.I_clear_Field("Filer_SL");
		CommonSteps.I_enter_in_field(searchText, "Filer_SL");
		CommonSteps.I_pause_for_seconds(2);
		Omega_CustomMethods.i_press_enter_key_();
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_should_see_on_page(selectSearch);
		CommonSteps.I_click(selectSearch);
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_click("okButton_SL");
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_should_see_on_page(searchResult);
	}

	@When("^I enter '(.*)' from '(.*)' and filter the order$")
	public static void i_enter_ordernumber_filter_the_order(String columnname, String sheetname) throws Exception {
		try {
			CommonSteps.I_pause_for_seconds(10);
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname,
					columnname, "StoreFront");
			System.out.println(fetchValuesFromSpreadSheet);
			Thread.sleep(3000);
			CommonSteps.I_enter_in_field(fetchValuesFromSpreadSheet, "OrderNumber_SL");
			CommonSteps.I_pause_for_seconds(2);
			CommonSteps.I_should_see_on_page("SearchOrders_SL");
			CommonSteps.I_focus_click("SearchOrders_SL");
			CommonSteps.I_pause_for_seconds(10);
			CommonSteps.I_should_see_on_page("Order_NumberSL");
			Omega_CustomMethods.i_should_see_value_from_input_field(fetchValuesFromSpreadSheet, "Order_NumberSL");
		} 
		
		catch (ComparisonFailure e) {
				System.out.println("Order not reached retry-"+1);
				CommonSteps.I_click("SearchOrders_SL");
				CommonSteps.I_pause_for_seconds(5);
				CommonSteps.I_click("SaveCustomerOrder_NoBtn");
				CommonSteps.I_pause_for_seconds(500);
				String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname,
						columnname, "StoreFront");
				System.out.println(fetchValuesFromSpreadSheet);
				Thread.sleep(3000);
				CommonSteps.I_pause_for_seconds(2);
				CommonSteps.I_should_see_on_page("SearchOrders_SL");
				CommonSteps.I_focus_click("SearchOrders_SL");
				CommonSteps.I_pause_for_seconds(10);
				CommonSteps.I_should_see_on_page("Order_NumberSL");
				Omega_CustomMethods.i_should_see_value_from_input_field(fetchValuesFromSpreadSheet, "Order_NumberSL");
		}
	}
	
	@When("^I enter '(.*)' from '(.*)' and filter the order omega call log$")
	public static void i_enter_ordernumber_filter_the_order_omegaCallLog(String columnname, String sheetname) throws Exception {
		try {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname,
					columnname, "StoreFront");
			System.out.println(fetchValuesFromSpreadSheet);
			Thread.sleep(3000);
			CommonSteps.I_enter_in_field(fetchValuesFromSpreadSheet, "RMA_HybrisID");
			CommonSteps.I_pause_for_seconds(2);
			CommonSteps.I_should_see_on_page("SearchOrders_SL");
			CommonSteps.I_focus_click("SearchOrders_SL");
			CommonSteps.I_pause_for_seconds(10);
			CommonSteps.I_should_see_on_page("RMA_HybrisID");
			Omega_CustomMethods.i_should_see_value_from_input_field(fetchValuesFromSpreadSheet, "RMA_HybrisID");
		} catch (ComparisonFailure e) {
				CommonSteps.I_click("SearchOrders_SL");
				String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname,
						columnname, "StoreFront");
				System.out.println(fetchValuesFromSpreadSheet);
				CommonSteps.I_pause_for_seconds(500);
				CommonSteps.I_should_see_on_page("SearchOrders_SL");
				CommonSteps.I_focus_click("SearchOrders_SL");
				CommonSteps.I_pause_for_seconds(10);
				CommonSteps.I_should_see_on_page("RMA_HybrisID");
			}
			
		}
	
	@Given("^I validate amounts tab from '(.*)'$")
	public static void i_validate_amounts_tab(String sheetname) throws Exception {
		CommonSteps.I_focus_click("AmountOptions");
		CommonSteps.I_should_see_on_page("Currency_Amounts");
		CommonSteps.I_should_see_on_page("Rate_Amounts");
//		String SalesTax = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "PONumber", "StoreFront");
//		Omega_CustomMethods.i_should_see_value_from_input_field(SalesTax, "SalesTax_Amounts");
		CommonSteps.I_should_see_on_page("SalesTax_Amounts");
//		String TotalPrice = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "PONumber", "StoreFront");
//		Omega_CustomMethods.i_should_see_value_from_input_field(TotalPrice, "TotalPrice_Amounts");
		CommonSteps.I_should_see_on_page("TotalPrice_Amounts");
//		String MultiSiteTotal = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "PONumber", "StoreFront");
//		Omega_CustomMethods.i_should_see_value_from_input_field(MultiSiteTotal, "MultiSiteTotal_Amounts");
		CommonSteps.I_should_see_on_page("MultiSiteTotal_Amounts");

	}

	@When("^I should see the order details '(.*)' feilds present in the page$")
	public static void i_should_see_details_feilds_present_page(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Order_DateSL");
		Omega_CustomMethods.i_validate_date_in_sytline_from_excel_sheet(sheetname, "DatePlaced", "Order_DateSL");
		CommonSteps.I_should_see_on_page("Status_SL");
		CommonSteps.I_should_see_on_page("Customer_SL");
		CommonSteps.I_should_see_on_page("CustomerPO_SL");
		String customerPO = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "PONumber", "StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(customerPO, "CustomerPO_SL");
		CommonSteps.I_should_see_on_page("Shipping_TermsSL");
		CommonSteps.I_should_see_on_page("Ship_viaSL");
		CommonSteps.I_should_see_on_page("FromAddress");
		CommonSteps.I_should_see_on_page("ToAddress");
		i_get_order_details_from_syteline_order_details(sheetname);
	}

	@When("^I get the order details '(.*)' from the Syteline Order Details page$")
	public static void i_get_order_details_from_syteline_order_details(String sheetname) throws Exception {
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("OrderNumber_Filtered", sheetname,
				"OrderNumber", "Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("CustomerSytelineNo", sheetname,
				"CustomerNumber", "StoreFront");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("CustomerSytelineNo", sheetname,
				"CustomerNumber", "Syteline");

		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Order_DateSL", sheetname, "DatePlaced",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("CustomerPO_SL", sheetname, "PONumber",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("PaymentOptions", sheetname, "PaymentType",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("ShippmentMethod", sheetname,
				"ShippingMethod", "Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("FromAddress", sheetname, "BillingAddress",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("ToAddress", sheetname, "ShipTo", "Syteline");

	}

	@When("^I get the contact details '(.*)' from the Syteline Order Details page$")
	public static void i_get_contact_details_from_syteline_order_details(String sheetname) throws Exception {
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Contacts_InfoSL", sheetname,
				"OrderPlacedBy", "SFDC");

		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Contacts_EmailSL", sheetname,
				"ContactEmail", "SFDC");

	}

	@When("^I click on Contacts and verify the contact details '(.*)' present in the page$")
	public static void i_click_on_contacts_verify_contactdetails(String sheetname) throws Exception {
		CommonSteps.I_click("Contact_HeadingSL");
		Thread.sleep(5000);
		CommonSteps.I_should_see_on_page("Contacts_InfoSL");
		String orderPlaced = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "OrderPlacedBy",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(orderPlaced, "Contacts_InfoSL");
		CommonSteps.I_should_see_on_page("Contacts_EmailSL");
		String ContactEmail = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "ContactEmail",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(ContactEmail, "Contacts_EmailSL");
		i_get_contact_details_from_syteline_order_details(sheetname);

	}

	@When("I should see order line item 1 and corresponding details '(.*)' present in the page$")
	public static void i_should_see_orderline_item_1_detials(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Order_NameSL");
		CommonSteps.I_should_see_on_page("Item_NameSL");
		Omega_CustomMethods.i_remove_itemName(sheetname, "ItemName1", "Item_NameSL");
		String productName = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "ProductName1",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(productName, "Item_TextName");
		Omega_CustomMethods.i_find_unit_price(sheetname, "UnitPrice1", "UnitPrice_SL");
		CommonSteps.I_should_see_on_page("NetPrice_SL");
		Omega_CustomMethods.i_find_total_price(sheetname, "TotalPrice1", "NetPrice_SL");
		CommonSteps.I_should_see_on_page("UnitCost_SL");
		CommonSteps.I_should_see_on_page("AvailableToOrder");
		CommonSteps.I_should_see_on_page("QtyOrdered");
		Omega_CustomMethods.i_find_Qty(sheetname, "Quantity1", "QtyOrdered");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Item_TextName", sheetname, "ProductName1",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Item_NameSL", sheetname, "ItemName1",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("UnitPrice_SL", sheetname, "UnitPrice1",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("NetPrice_SL", sheetname, "TotalPrice1",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("QtyOrdered", sheetname, "Quantity1",
				"Syteline");
	}
	
	@When("I should see order line item one and corresponding details '(.*)' present in the page$")
	public static void i_should_see_orderline_item_one_detials(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Order_NameSL");
		CommonSteps.I_should_see_on_page("Item_NameSL");
		Omega_CustomMethods.i_remove_itemName(sheetname, "ItemName", "Item_NameSL");
		String productName = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "ProductName",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(productName, "Item_TextName");
		Omega_CustomMethods.i_find_unit_price(sheetname, "UnitPrice", "UnitPrice_SL");
		CommonSteps.I_should_see_on_page("NetPrice_SL");
		Omega_CustomMethods.i_find_total_price(sheetname, "TotalPrice", "NetPrice_SL");
		CommonSteps.I_should_see_on_page("UnitCost_SL");
		CommonSteps.I_should_see_on_page("AvailableToOrder");
		CommonSteps.I_should_see_on_page("QtyOrdered");
		Omega_CustomMethods.i_find_Qty(sheetname, "Quantity", "QtyOrdered");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Item_TextName", sheetname, "ProductName",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Item_NameSL", sheetname, "ItemName",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("UnitPrice_SL", sheetname, "UnitPrice",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("NetPrice_SL", sheetname, "TotalPrice",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("QtyOrdered", sheetname, "Quantity",
				"Syteline");

	}


	@When("^I click on override and revalidate the order$")
	public static void i_click_on_override_and_revalidate_the_order() {
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_focus_click("OverrideButton_SL");
		CommonSteps.I_pause_for_seconds(10);
		CommonSteps.I_should_see_on_page("Revalidate_SL");
		CommonSteps.I_focus_click("Revalidate_SL");
		CommonSteps.I_wait_for_visibility_of_element("RevalidateOkBtn_SL");
		CommonSteps.I_should_see_on_page("RevalidateOkBtn_SL");
		CommonSteps.I_focus_click("RevalidateOkBtn_SL");
	}

	@When("I should see order line item 2 and corresponding details '(.*)' present in the page$")
	public static void i_should_see_orderline_item_2_detials(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Order_NameSL");
		CommonSteps.I_should_see_on_page("Item_NameSL");
		Omega_CustomMethods.i_remove_itemName(sheetname, "ItemName2", "Item_NameSL");
		String productName = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "ProductName2",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(productName, "Item_TextName");
		Omega_CustomMethods.i_find_unit_price(sheetname, "UnitPrice2", "UnitPrice_SL");
		CommonSteps.I_should_see_on_page("NetPrice_SL");
		Omega_CustomMethods.i_find_total_price(sheetname, "TotalPrice2", "NetPrice_SL");
		CommonSteps.I_should_see_on_page("UnitCost_SL");
		CommonSteps.I_should_see_on_page("AvailableToOrder");
		CommonSteps.I_should_see_on_page("QtyOrdered");
		Omega_CustomMethods.i_find_Qty(sheetname, "Quantity2", "QtyOrdered");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Item_TextName", sheetname, "ProductName2",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("Item_NameSL", sheetname, "ItemName2",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("UnitPrice_SL", sheetname, "UnitPrice2",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("NetPrice_SL", sheetname, "TotalPrice2",
				"Syteline");
		Omega_CustomMethods.i_get_the_value_details_from_orderdetails_page("QtyOrdered", sheetname, "Quantity2",
				"Syteline");

	}
	
	@Given("^I navigate to ASM Application$")
	public static void i_navigate_to_asm_application() {
		Omega_CustomMethods.i_navigate_to_asm_application();
		CommonSteps.I_should_see_on_page("ASM_Logo");
		CommonSteps.I_should_see_on_page("Asagent_username");
		CommonSteps.I_should_see_on_page("Asagent_password");
		CommonSteps.I_should_see_on_page("Asagent_username");
		CommonSteps.I_enter_in_field("asagent", "Asagent_username");
		CommonSteps.I_clear_Field("Asagent_password");
		CommonSteps.I_enter_in_field("123456", "Asagent_password");
		CommonSteps.I_click("Asagent_SignIn");
	}
	
	@Given("^I validate elements present on Order History page and check for return order button$")
	public static void i_validate_return_order_button() {
		CommonSteps.I_should_see_on_page("OrderNumberTitle");
		CommonSteps.I_should_see_on_page("OrderPlacedTitle");
		CommonSteps.I_should_see_on_page("TotalTitle");
		CommonSteps.I_should_see_on_page("OrderStatusTitle");
		CommonSteps.I_should_see_on_page("OrderPlacedByTitle");
		CommonSteps.I_should_see_on_page("OrderNumberValue");
		CommonSteps.I_should_see_on_page("OrderPlacedValue");
		CommonSteps.I_should_see_on_page("TotalValue");
		CommonSteps.I_should_see_on_page("OrderStatusValue");
		CommonSteps.I_should_see_on_page("OrderPlacedByValue");
		CommonSteps.I_should_see_on_page("ReorderBtnOrderDetails");
		CommonSteps.I_should_see_on_page("ReturnOrderBtnOrderDetails");
		CommonSteps.I_should_see_on_page("ShipToAddress");
		CommonSteps.I_should_see_on_page("ShipMethod");
		CommonSteps.I_should_see_on_page("ReorderBtnBottom");	
	}
	
	@Given("^I validate contents from Omega Call Log from '(.*)'$")
	public static void i_validate_omega_call_log_page_from_excel(String sheetname) throws Exception {
		String orderNumber = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "OrderNumber",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(orderNumber, "Originating_OrderID");
		CommonSteps.I_should_see_on_page("Originating_OrderID");
		CommonSteps.I_should_see_on_page("FromRetrunAddress_SL");
		CommonSteps.I_should_see_on_page("ToReturnAddress_SL");
		CommonSteps.I_should_see_on_page("StatusReturn_SL");
		CommonSteps.I_should_see_on_page("Contact_OmegaCallLog");
		CommonSteps.I_should_see_on_page("Email_OmegaCallLog");
		CommonSteps.I_should_see_on_page("Phone_OmegaCallLog");
		CommonSteps.I_should_see_on_page("TakenBy_OmegaCallLog");
		CommonSteps.I_should_see_on_page("TermsCode_OmegaCallLog");
		CommonSteps.I_should_see_on_page("ShipVia_OmegaCallLog");
	}
	
	@Given("^I select card from Omega Call Log$")
	public static void i_select_card_from_omega_call_log() {
		CommonSteps.I_should_see_on_page("PayWithCardButton_OmegaCallLog");
		CommonSteps.I_click("PayWithCardButton_OmegaCallLog");
		CommonSteps.I_should_see_on_page("CreditCardDetails_Title");
		CommonSteps.I_should_see_on_page("CardNumber_input");
		CommonSteps.I_clickJS("CardNumber_input");
		CommonSteps.I_should_see_on_page("SelectCard_Number");
		CommonSteps.I_focus_click("SelectCard_Number");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("Authorize_OmegaCallLog");
		CommonSteps.I_focus_click("Authorize_OmegaCallLog");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_focus_click("Authorize_OkBtn");
	}
	
	@Given("^I get tracking ID and store in sheet '(.*)'$")
	public static void i_get_tracking_details_and_store(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("Tracking_tab");
		CommonSteps.I_focus_click("Tracking_tab");
		CommonSteps.I_pause_for_seconds(10);
		CommonSteps.I_should_see_on_page("ReferenceNumber_OmegaCallLogs");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ReferenceNumber_OmegaCallLogs",sheetname,"ReferenceNumber");
		
	}
	
	@Given("^I change order status to Complete$")
	public static void i_change_order_status_to_complete() {
		CommonSteps.I_pause_for_seconds(3);
		CommonSteps.I_focus_click("OmegaCall_LogStatus");
		CommonSteps.I_should_see_on_page("OmegaCall_LogStatusComplete");
		CommonSteps.I_focus_click("OmegaCall_LogStatusComplete");
		CommonSteps.I_pause_for_seconds(3);
		CommonSteps.I_focus_click("Phone_OmegaCallLog");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_focus_click("CustomerOrderLines_Save");
	}
	
	@Given("^I vaildate RMA details from '(.*)'$")
	public static void i_validate_the_RMA_detail(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("RMADate");
		CommonSteps.I_should_see_on_page("RMAStatus");
		CommonSteps.I_should_see_on_page("RMACustomerName");
		CommonSteps.I_pause_for_seconds(5);
		String CustomerNumber = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "CustomerID",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(CustomerNumber, "RMACustomerName");
		CommonSteps.I_should_see_on_page("RMACustomerAddress");
		CommonSteps.I_should_see_on_page("RMAShipToAddress");
		CommonSteps.I_should_see_on_page("RMAContactName");
		String CustomerName = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "OrderPlacedBy",
				"StoreFront");
		Omega_CustomMethods.i_should_see_value_from_input_field(CustomerName, "RMAContactName");
		CommonSteps.I_should_see_on_page("RMAContactNumber");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_click("RMAGeneralTab");
		CommonSteps.I_should_see_on_page("RMAWarehouse");
		CommonSteps.I_should_see_on_page("RMATakenBy");
		CommonSteps.I_should_see_on_page("RMAEndUserType");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("RMAGeneralAmounts");
		CommonSteps.I_click("RMAGeneralAmounts");
		CommonSteps.I_should_see_on_page("RMAGeneralAmounts_Currency");
		CommonSteps.I_should_see_on_page("RMAGeneralAmounts_ExchangeRate");
		CommonSteps.I_should_see_on_page("RMAGeneralAmounts_TotalCredit");
		CommonSteps.I_should_see_on_page("RMAGeneralAmounts_SalesTax");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_click("RMALinesButton");
		CommonSteps.I_should_see_on_page("RMALinesButton_RefNumber");
		CommonSteps.I_should_see_on_page("RMALinesButton_Date");
		CommonSteps.I_should_see_on_page("RMALinesButton_Line");
		CommonSteps.I_should_see_on_page("RMALinesButton_Item");
		CommonSteps.I_should_see_on_page("RMALinesButton_ItemDescription");
		CommonSteps.I_should_see_on_page("RMALinesButton_OrderNumber");
		CommonSteps.I_should_see_on_page("RMALinesButton_QtyCredited");
		CommonSteps.I_should_see_on_page("RMALinesButton_QtyReceived");
		CommonSteps.I_pause_for_seconds(5);
		
		}
	
	@Given("^I validate RMA line items from '(.*)' : sheet$")
	public static void i_validate_rma_lines_for_multiLine(String sheetname) throws Exception {
		CommonSteps.I_click("RMALinesButton_AmountsTab");
		CommonSteps.I_should_see_on_page("RMALinesButton_AmountsTab");
		CommonSteps.I_should_see_on_page("RMALinesButton_UnitCredit");
		String UnitCredit = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "UnitPrice1",
				"StoreFront");
		String UnitCreditLines = StringUtils.stripStart(UnitCredit, "$");
		String UnitCreditLinesItem = UnitCreditLines+"000";
		System.out.println("Unit Credit: "+UnitCreditLinesItem);
		Omega_CustomMethods.i_should_see_value_from_input_field(UnitCreditLinesItem, "RMALinesButton_UnitCredit");
		CommonSteps.I_should_see_on_page("RMALinesButton_ExtCredit");
		Omega_CustomMethods.i_should_see_value_from_input_field(UnitCreditLines, "RMALinesButton_ExtCredit");
		CommonSteps.I_should_see_on_page("RMALinesButton_RestockingFee");
		CommonSteps.I_should_see_on_page("RMALinesButton_RestockingFeeAmt");
		CommonSteps.I_should_see_on_page("RMALinesButton_NetCredit");
		Omega_CustomMethods.i_should_see_value_from_input_field(UnitCreditLines, "RMALinesButton_NetCredit");
		
		
	}

}
