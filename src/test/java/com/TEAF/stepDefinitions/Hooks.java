package com.TEAF.stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	//Pre-pay Account
	
	@Before("@PrepayRMA_QA,@PrepayRRMA_QA")
	public void signinOmegaApplicationforsearch() throws Exception {
		try {
			CommonSteps.my_webapp_is_open("Omega");
			CommonSteps.I_click("SignIn_Btn");
			CommonSteps.I_should_see_on_page("SignID_Field");
			CommonSteps.I_enter_in_field("taha.patel_pp_us@royalcyber.com", "SignID_Field");
			CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
			CommonSteps.I_click("LogIn_Btn");
			CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
			Omega_CustomMethods.i_clear_the_cart_for_omega_application();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	//Account Payment
	
	@Before("@CorporateCCRMA_QA,@CorporateCCRRMA1_QA,@AccountPaymentRMA_QA,@AccountPaymentRRMA_QA,@CorporatePartialReturn_QA,@CorporatePartialReturnRRMA_QA,@PartialShipping_QA,@PartialShippingRRMA_QA,@AccountPaymentSingleLine_QA,@AccountPaymentSingleLineRRMA_QA,@CorporateCCMultiLine_QA,@CorporateCCMultiLineRRMA_QA,@AccountPaymentRRMAC_QA,@CorporatePartialReturnRRMA1_QA")
	public void signinOmegaApplicationforsearchQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("taha.patel_ap_us@royalcyber.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
//		Omega_CustomMethods.i_clear_the_cart_for_omega_application();

		
	}
	
	// Web Account Payment
	
	@Before("@WebCCRMA_QA,@WebCCReturnRRMA1_QA,@WebCustomerSingleLine_QA,@WebCustomerSingleLineRRMA_QA,@RestockingFee_QA,@RestockingFeeRRMA_QA,@VolumeDiscounts_QA,@VolumeDiscountsRRMA_QA,@WebCCReturnRRMAC_QA")
	public void signinOmegaApplicationforccQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("vishalvrk97@gmail.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
//		Omega_CustomMethods.i_clear_the_cart_for_omega_application();

	}
	
	@Before("@RestockingFeeRRMA1_QA,@RestockingFeeRRMA2_QA")
	public static void restocking() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
//		CustomMethod_StoreFront.i_change_date_in_hybris_back_office();
//		CommonSteps.I_pause_for_seconds(10);
		CommonSteps.my_webapp_is_open("Omega");
		CommonSteps.I_click("SignIn_Btn");
		CommonSteps.I_should_see_on_page("SignID_Field");
		CommonSteps.I_enter_in_field("vishalvrk97@gmail.com", "SignID_Field");
		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
		CommonSteps.I_click("LogIn_Btn");
		CommonSteps.I_should_see_text_present_on_page_At("Welcome Taha Patel", "Welcome");
	}

//	@Before("@EmailVerification,@EmailVerificationRRMA,@EmailVerification_QA")
//	public void signinEmailVerification() throws Exception {
//		CommonSteps.my_webapp_is_open("Omega");
//		CommonSteps.I_click("SignIn_Btn");
//		CommonSteps.I_should_see_on_page("SignID_Field");
//		CommonSteps.I_enter_in_field("logeshkumar.r@royalcyber.com", "SignID_Field");
//		CommonSteps.I_enter_in_field("Omega2003@", "Password_Field");
//		CommonSteps.I_click("LogIn_Btn");
//		CommonSteps.I_should_see_text_present_on_page_At("Welcome Logesh R", "Welcome");
//		Omega_CustomMethods.i_clear_the_cart_for_omega_application();
//	}
	
//	
//	@Before("")
//	public void signInSytline() throws Exception {
//		CommonSteps.my_webapp_is_open("Omega");
//		Omega_CustomMethods.i_navigate_to_application("https://omgdev.apptrix.com/WSWebClient/iux.aspx");
//		CommonSteps.I_clear_Field("UserName");
//		CommonSteps.I_clear_Field("PassWord");
//		CommonSteps.I_enter_in_field("tpatel", "UserName");
//		CommonSteps.I_enter_in_field("dev1234", "PassWord");
//		Thread.sleep(3000);
//		CommonSteps.I_clear_Field("Config");
//		Thread.sleep(3000);
//		CommonSteps.I_enter_in_field("dCT01", "Config");
//		Thread.sleep(5000);
//		Omega_CustomMethods.i_press_enter_key_();
//		Thread.sleep(6000);
//		CommonSteps.I_click("Login_SL");
//		CommonSteps.I_wait_for_visibility_of_element("OpenForm");
//		CommonSteps.I_should_see_on_page("OpenForm");
//	}
//	
	@Before("@StylineMultiOrder,@StylineSingleOrder,@CorporateCCLRMA_QA,@WebCCLRMA_QA,@AccountPaymentCLRMA_QA,@PrepayCLRMA_QA,@CorporatePartialReturnCL_QA,@PartialShippingCL_QA,@WebCustomerSingleLineCL_QA,@AccountPaymentSingleLineCL_QA,@CorporateCCMultiLineCL_QA,@RestockingFeeCL_QA,@VolumeDiscountsCL_QA")
	public void signInSytlineQA() throws Exception {
		CommonSteps.my_webapp_is_open("Omega");
		Omega_CustomMethods.i_navigate_to_application("https://omega.apptrix.com/WSWebClient/Default.aspx");
		CommonSteps.I_clear_Field("UserName");
		CommonSteps.I_clear_Field("PassWord");
		CommonSteps.I_enter_in_field("tpatel", "UserName");
		CommonSteps.I_enter_in_field("pilot123", "PassWord");
		Thread.sleep(3000);
		CommonSteps.I_clear_Field("Config");
		Thread.sleep(3000);
		CommonSteps.I_enter_in_field("pCT01", "Config");
		Thread.sleep(5000);
		Omega_CustomMethods.i_press_enter_key_();
		Thread.sleep(6000);
		CommonSteps.I_click("Login_SL");
		CommonSteps.I_wait_for_visibility_of_element("OpenForm");
		CommonSteps.I_should_see_on_page("OpenForm");
	}
	
	@After("@StylineMultiOrder,@StylineSingleOrder,@CorporateCCLRMA_QA,@WebCCLRMA_QA,@AccountPaymentCLRMA_QA,@PrepayCLRMA_QA,@CorporatePartialReturnCL_QA,@PartialShippingCL_QA,@WebCustomerSingleLineCL_QA,@AccountPaymentSingleLineCL_QA,@CorporateCCMultiLineCL_QA,@RestockingFeeCL_QA,@VolumeDiscountsCL_QA")
	public void signOutSytline() throws Exception {
		CommonSteps.I_focus_click("systemButton");
		Thread.sleep(5000);
		CommonSteps.I_mouse_over("FormOption_SL");
		CommonSteps.I_focus_click("SignOut_Option");
		Thread.sleep(5000);
		try {
//		CommonSteps.I_focus_click("SaveCustomerOrder_NoBtn");
		CommonSteps.I_should_see_on_page("UserName");
		}
		catch (Exception e) {
			CommonSteps.I_should_see_on_page("UserName");
		}
	}
	
	
	@After("@PrepayRMA_QA,@PrepayRRMA_QA,@CorporateCCRMA_QA,@CorporateCCRRMA_QA,@AccountPaymentRMA_QA,@AccountPaymentRRMA_QA,@CorporatePartialReturn_QA,@CorporatePartialReturnRRMA_QA,@PartialShipping_QA,@PartialShippingRRMA_QA,@AccountPaymentSingleLine_QA,@AccountPaymentSingleLineRRMA_QA,@CorporateCCMultiLine_QA,@CorporateCCMultiLineRRMA_QA,@WebCCRMA_QA,@WebCCReturnRRMA_QA,@WebCustomerSingleLine_QA,@WebCustomerSingleLineRRMA_QA,@RestockingFee_QA,@RestockingFeeRRMA_QA,@VolumeDiscounts_QA,@VolumeDiscountsRRMA_QA")
	public void signOutOmegaApplication() throws Exception {
		CommonSteps.I_Refresh_WebPage();
		CommonSteps.I_click("OmegaLogo");
		Thread.sleep(5000);
		CommonSteps.I_click("MyAccount");
		CommonSteps.I_click("Sign_Out");
		CommonSteps.I_should_see_on_page("SignIn_Btn");
	}
}
