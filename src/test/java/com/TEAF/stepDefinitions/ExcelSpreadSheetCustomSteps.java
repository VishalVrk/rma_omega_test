package com.TEAF.stepDefinitions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelSpreadSheetCustomSteps {

	public static void main(String[] args) throws Exception {
		String fetchValuesFromSpreadSheet = fetchValuesFromSpreadSheet("TestWeb", "DueDate", "SFDC");
		System.out.println(fetchValuesFromSpreadSheet);
	}

	public static String fetchValuesFromSpreadSheet(String sheetname, String columnname, String app) throws Exception {
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			int rowCount = sheet.getPhysicalNumberOfRows();
			String fetchValue = null;
			int clnNo = 0;
			if (app.equalsIgnoreCase("StoreFront")) {
				clnNo = 1;
			} else if (app.equalsIgnoreCase("SFDC")) {
				clnNo = 2;
			} else if (app.equalsIgnoreCase("Syteline")) {
				clnNo = 3;
			} else {
				throw new Exception("Not a valid APP name");
			}
			for (int i = 0; i < rowCount; i++) {
				String stringCellValue = sheet.getRow(i).getCell(0).getStringCellValue();
				if (stringCellValue.equals(columnname)) {
					Cell cell = sheet.getRow(i).getCell(clnNo);
					CellType cellType = cell.getCellType();
					if (cellType.equals(CellType.STRING)) {
						fetchValue = cell.getStringCellValue();
					} else if (cellType.equals(CellType.NUMERIC)) {
						double numericCellValue = cell.getNumericCellValue();
						long l = (long) numericCellValue;
						fetchValue = String.valueOf(l);
					}
				}
			}
			return fetchValue;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}

	}

	public static void updateValuesToExcel(String sheetname, String columnname, String value, String app)
			throws Exception {
		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			int clnNo = 0;
			if (app.equalsIgnoreCase("StoreFront")) {
				clnNo = 1;
			} else if (app.equalsIgnoreCase("SFDC")) {
				clnNo = 2;
			} else if (app.equalsIgnoreCase("Syteline")) {
				clnNo = 3;
			} else {
				throw new Exception("Not a valid APP name");
			}
			for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {

				Cell cell = sheet.getRow(i).getCell(0);
				String stringCellValue = cell.getStringCellValue();
				if (stringCellValue.equals(columnname)) {

					Cell c1 = sheet.getRow(i).getCell(clnNo);
					if (c1 == null) {
						c1 = sheet.getRow(i).createCell(clnNo);
					}
					c1.setCellValue(value);
				}
			}
			FileOutputStream fout = new FileOutputStream(f);
			wb.write(fout);
			wb.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		} //

	}

//	public static void updateDateToExcel(String sheetname, String columnname, Date value, int rowNum) throws Exception {
//		try {
//			java.io.File f = new java.io.File(
//					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
//			FileInputStream fin = new FileInputStream(f);
//			Workbook wb = new XSSFWorkbook(fin);
//			Sheet sheet = wb.getSheet(sheetname);
//			Row row = sheet.getRow(0);
//			for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
//
//				Cell cell = row.getCell(i);
//				String stringCellValue = cell.getStringCellValue();
//				if (stringCellValue.equals(columnname)) {
//					Row data = sheet.getRow(rowNum);
//					if (data == null) {
//						data = sheet.createRow(rowNum);
//					}
//
//					Cell c1 = data.getCell(i);
//					if (c1 == null) {
//						c1 = data.createCell(i);
//					}
//					c1.setCellValue(value);
//				}
//			}
//			FileOutputStream fout = new FileOutputStream(f);
//			wb.write(fout);
//			wb.close();
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			throw new Exception();
//		} //
//
//	}
}
