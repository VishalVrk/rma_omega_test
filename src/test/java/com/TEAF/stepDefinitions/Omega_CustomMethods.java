package com.TEAF.stepDefinitions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;

import org.apache.bcel.generic.IF_ACMPEQ;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tools.ant.filters.TokenFilter.ContainsString;
import org.apache.tools.ant.taskdefs.Truncate;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Resources.FetchingEmail;
import com.Resources.FetcingEmail;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.TEAF.stepDefinitions.CommonSteps;
import com.gargoylesoftware.htmlunit.javascript.host.file.File;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.lu.an;
import cucumber.runtime.CucumberException;

public class Omega_CustomMethods {

	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();

	@Given("^I verify title of the page$")
	public static void I_verify_title_of_the_page() throws Exception {
		try {
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			String expectedTitle = "About Us | Omega Engineering";
			Assert.assertEquals(expectedTitle, actualTitle);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	public static void i_click_by_js(WebElement wElement) {
		try {
			wElement.isEnabled();
			wElement.isDisplayed();
			WrapperFunctions.clickByJS(wElement);
			StepBase.embedScreenshot();
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@When("^I load the excel sheet data to validate the Syteline$")
	public static void i_load_the_excel_sheet_data_to_validate_syteline() throws IOException {
		java.io.File des = new java.io.File(
				System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
		String pathname = System.getProperty("ExcelPath", "c:\"");
		java.io.File src = new java.io.File(pathname + "\\OmegaTestDatas.xlsx");
		FileUtils.copyFile(src, des);
		if (des.isFile()) {
			System.out.println("File Copied");
		}
	}

	@Given("^I get 60 days before given date and enter at '(.*)'$")
	public static void i_get_60_days_before_given_date(String locator) throws Exception {
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String value = element.getAttribute("value");
		SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -60);
		String finalDate = formatter.format(cal.getTime());
		System.out.println("Final Date: " + finalDate);
		element.sendKeys(finalDate);
	}

	@Given("^I validate restocking fee validation '(.*)' :Price '(.*)' :Qty '(.*)' :RestockingFee '(.*)' :Total$")
	public static void i_validate_restocking_fee_validation(String price, String Quantity, String restockAmount,
			String Total) throws Exception {
		WebElement PriceElement = driver.findElement(GetPageObjectRead.OR_GetElement(price));
		wrapFunc.highLightElement(PriceElement);
		WebElement QuantityElement = driver.findElement(GetPageObjectRead.OR_GetElement(Quantity));
		wrapFunc.highLightElement(QuantityElement);
		WebElement restockAmountElement = driver.findElement(GetPageObjectRead.OR_GetElement(restockAmount));
		wrapFunc.highLightElement(restockAmountElement);
		WebElement TotalElement = driver.findElement(GetPageObjectRead.OR_GetElement(Total));
		wrapFunc.highLightElement(TotalElement);
		String PriceValue = PriceElement.getText().trim();
		PriceValue = StringUtils.stripStart(PriceValue, "$");
		System.out.println(PriceValue);
		String QuantityValue = QuantityElement.getText().trim();
		System.out.println(QuantityValue);
		String restockAmountValue = restockAmountElement.getText().trim();
		restockAmountValue = StringUtils.stripStart(restockAmountValue, "$");
		System.out.println(restockAmountValue);
		String TotalValue = TotalElement.getText().trim();
		TotalValue = StringUtils.stripStart(TotalValue, "$");
		System.out.println(TotalValue);

//		int priceInt = Integer.parseInt(PriceValue);
		double priceInt = Double.parseDouble(PriceValue);
		double QtyInt = Double.parseDouble(QuantityValue);
		double rstkAmtInt = Double.parseDouble(restockAmountValue);
		double totalInt = Double.parseDouble(TotalValue);
		rstkAmtInt = priceInt * 0.2 * QtyInt;
		totalInt = (priceInt * QtyInt) - rstkAmtInt;
		String RestockFeeFinal = new DecimalFormat("0.00").format(rstkAmtInt);
		System.out.println(RestockFeeFinal);
		String TotalAmountFinal = new DecimalFormat("0.00").format(totalInt);
		System.out.println(TotalAmountFinal);
		Assert.assertEquals(restockAmountValue, RestockFeeFinal);
		wrapFunc.highLightElement(restockAmountElement);
		Assert.assertEquals(TotalValue, TotalAmountFinal);
		wrapFunc.highLightElement(TotalElement);

	}

	@Given("^I should see value '(.*)' present on page at '(.*)'$")
	public static void i_should_see_value_from_input_field(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					System.out.println("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element),
					Integer.parseInt(System.getProperty("test.implicitlyWait")));
			String actualText = wElement.getAttribute("value");
			WrapperFunctions.highLightElement(wElement);
			expectedText = expectedText.trim();
			actualText = actualText.trim();
			Assert.assertEquals(expectedText, actualText);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	public static void i_validate_search_element(String sheetname, String column ,String element) throws Exception {
		String expectedText = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		WebElement wElement = WrapperFunctions.getElementByLocator(element,
				GetPageObjectRead.OR_GetElement(element),
				Integer.parseInt(System.getProperty("test.implicitlyWait")));
		String actualText = wElement.getAttribute("value");
		WrapperFunctions.highLightElement(wElement);
		expectedText = expectedText.trim();
		actualText = actualText.trim();
		Assert.assertTrue(expectedText.contains(actualText));
	}

	@Given("^I should find elements '(.*)' : sheetname '(.*)' : column$")
	public static void i_should_see_tables_present_in_sytline(String sheetname, String column) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String value = fetchValuesFromSpreadSheet.substring(6).toString();
		String locator = "((//td//div[text()='" + value + "']))[1]";
		System.out.println(locator);
	}

	@Given("^I scroll till element '(.*)'$")
	public static void i_should_scroll_till_the_element(String locator) throws Exception {
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		Thread.sleep(500);
	}

	@Given("^I should see value contains '(.*)' present on page at '(.*)'$")
	public static void i_should_see_contains_value_from_input_field(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					System.out.println("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element),
					Integer.parseInt(System.getProperty("test.implicitlyWait")));
			String actualText = wElement.getAttribute("value");
			WrapperFunctions.highLightElement(wElement);
			System.out.println("Expected Text" + expectedText);
			System.out.println("Actual Text" + actualText);

			Assert.assertTrue(expectedText.toLowerCase().contains(actualText.toLowerCase()));
			StepBase.embedScreenshot();
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I should find received date '(.*)' : sheetname '(.*)' : column and enter date$")
	public static void i_enter_received_date_from_table(String sheetname, String column) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String value = fetchValuesFromSpreadSheet.substring(6).toString();
		String locator = "((//td//div[text()='" + value + "'])[2]//..//.//..//..//div)[7]";
		System.out.println(locator);
		WebElement element = driver.findElement(By.xpath(locator));
		Thread.sleep(5000);
		Actions ac = new Actions(driver);
		System.out.println("Click action sendkeys");
		Thread.sleep(2000);
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy"); 
		Calendar cal = Calendar.getInstance(); 
		Date date = cal.getTime(); 
		String todaysdate = dateFormat.format(date);
		System.out.println("Today's date : " + todaysdate);
		try {
		ac.moveToElement(element).click().build().perform();
		Thread.sleep(2000);
		ac.moveToElement(element).click().build().perform();}
		catch (Exception e) {
			driver.findElement(By.xpath(locator)).click();
		}
		ac.sendKeys(todaysdate).build().perform();
		System.out.println("Action Performed..");
		Thread.sleep(5000);
		Omega_CustomMethods.i_press_enter_key_();
		CommonSteps.I_should_see_on_page("Phone_OmegaCallLog");
	}

	@Given("^I click and select '(.*)' : sheetname '(.*)' : column$")
	public static void i_enter_select_date_from_table(String sheetname, String column) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String value = fetchValuesFromSpreadSheet.substring(6).toString();
		String locator = "((//td//div[text()='" + value + "'])[2]//..//.//..//..//div)[3]";
		System.out.println(locator);
		Thread.sleep(5000);
		WebElement element = driver.findElement(By.xpath(locator));
		Actions ac = new Actions(driver);
		ac.moveToElement(element).click(element).build().perform();
	}

	@Given("^I get refrence number '(.*)' : sheetname '(.*)' : column and Store at '(.*)'$")
	public static void i_enter_get_data_referenceOrderNumber(String sheetname, String column, String ColumnName)
			throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String value = fetchValuesFromSpreadSheet.substring(6).toString();
		String locator = "((((//td//div[text()='" + value + "'])[3])//..//..//div)[3]//..)[1]";
		WebElement element = driver.findElement(By.xpath(locator));
		Thread.sleep(5000);
		String ElementText = element.getText();
		System.out.println(ElementText);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, ColumnName, ElementText, "StoreFront");

	}

	@Given("^I enter text '(.*)' in textarea at '(.*)' field$")
	public static void I_Click_TAB(String text, String locator) throws Exception {
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		Thread.sleep(2000);
		element.sendKeys(Keys.TAB);
		element.clear();
		element.sendKeys(text);
	}

	@Given("^I Zoom out$")
	public static void I_Zoom_out() {
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("document.body.style.zoom = '0.75'");
	}

	@When("^I verify the order confirmation email sent to the registrated web account successfully$")
	public static void i_verify_the_order_confirmation_email() {
		FetcingEmail.emailVerification();
	}

	@Given("^I find unit price at '(.*)' : sheet '(.*)' : column contained at '(.*)'$")
	public static void i_find_unit_price(String sheetname, String column, String locator) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String UnitPrice = StringUtils.stripStart(fetchValuesFromSpreadSheet, "$");
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String expectedUnit = element.getAttribute("value");
		UnitPrice = UnitPrice + "000";
		System.out.println(UnitPrice);
		Assert.assertEquals(UnitPrice.trim(), expectedUnit.trim());
	}

	@Given("^I validate '(.*)' from '(.*)' sheet to the field '(.*)' from Sytline$")
	public static void validatesheetfeild(String Value, String sheetname, String element) throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String value = container.getAttribute("value");
			wrapFunc.highLightElement(container);
			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if (hr.getCell(i).getStringCellValue().equals(Value)) {
					cellValue = content.getCell(i).getStringCellValue();
				}
			}
			System.out.println("Cell Value " + cellValue);

			Assert.assertEquals(cellValue, value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I validate address '(.*)' from '(.*)' sheet to the field '(.*)' from Sytline$")
	public static void enter_field_address_input(String Value, String sheetname, String element) throws Exception {

		try {
//			java.io.File f = new java.io.File(
//					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
//			FileInputStream fin = new FileInputStream(f);
//			Workbook wb = new XSSFWorkbook(fin);
//			Sheet sheet = wb.getSheet(sheetname);
//			Row hr = sheet.getRow(0);
//			Row content = sheet.getRow(1);
//			String cellValue = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String value = container.getAttribute("value").toString();
			wrapFunc.highLightElement(container);
//			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
//				if (hr.getCell(i).getStringCellValue().equals(Value)) {
//					cellValue = content.getCell(i).getStringCellValue();
//				}
//			}
//			System.out.println("Cell Value " + cellValue.toString());

			Assert.assertNotNull(value);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@When("^I get the email id from the order confirmation page and store in sheet '(.*)' and column '(.*)'$")
	public static void i_get_the_emailid_order_confirmation_page(String sheetname, String columnname) throws Exception {
		try {
			String emailId = driver.findElement(By.xpath("//div[@class='checkout-success__body']/p[2]")).getText();
			String[] split = emailId.split(" ");
			ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, columnname, split[split.length - 1],
					"StoreFront");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@When("^I get the qty '(.*)' and price '(.*)' from the table along with price and enter in '(.*)'$")
	public static void i_get_the_qty_from_the_table(String qtyValue, String pricevalue, String qtyFeild)
			throws Exception {
		try {
			String qty = driver.findElement(GetPageObjectRead.OR_GetElement(qtyValue)).getText();
			String[] split = qty.split("-");
			WebElement qtyInput = driver.findElement(GetPageObjectRead.OR_GetElement(qtyFeild));
			qtyInput.sendKeys(split[0]);
			String attribute = qtyInput.getAttribute("value");
			System.out.println("Qty value entered is " + attribute + " qty stored is " + split[0]);
			String price = driver.findElement(GetPageObjectRead.OR_GetElement(qtyValue)).getText();
			HashMapContainer.add("$$" + qtyValue, split[0]);
			HashMapContainer.add("$$" + pricevalue, price);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I validate Date '(.*)' : sheet '(.*)' : column at '(.*)' field$")
	public static void i_validate_date_in_sytline_from_excel_sheet(String sheetname, String column, String locator)
			throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb",
				"DatePlaced", "StoreFront");
		WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String SytDate = container.getAttribute("value");
		System.out.println("Date is " + fetchValuesFromSpreadSheet);
		Date ExcelDate = new SimpleDateFormat("MM/dd/yy hh:mm a").parse(fetchValuesFromSpreadSheet);
		String sampleDate = new SimpleDateFormat("MM/dd/yyyy").format(ExcelDate);
//	         System.out.println(ExcelDate);
		System.out.println(fetchValuesFromSpreadSheet);
		System.out.println(sampleDate);
//		Assert.assertEquals(sampleDate, SytDate);
	}

	@Given("^I find item '(.*)' : sheet '(.*)' : column '(.*)' : location$")
	public static void i_remove_itemName(String sheetname, String column, String location) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String itemName = StringUtils.stripStart(fetchValuesFromSpreadSheet, "Item #:");
		System.out.println(itemName);
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(location));
		String actualText = element.getAttribute("value");
		Assert.assertEquals(itemName, actualText);
	}

	@When("^I click item 2 with order number sheet '(.*)' and column '(.*)'$")
	public static void i_click_item_2(String sheet, String column) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheet, column,
				"StoreFront");
		driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[4]")).click();
	}
	
	@When("^I enter due date & order number from '(.*)' and click on regenerate button and shippment type '(.*)'$")
	public static void i_enter_due_date_order_number_click_on_regenerate(String sheetname, String ShippmentType) throws Exception {
		if(ShippmentType.contains("PartialShippment")) {
			Omega_CustomMethods.i_enter_date_from_excelSheet_NP("DueDate", sheetname, "Upto_DueDate_SL");
			CommonSteps.I_should_see_on_page("Location_SL");
			CommonSteps.I_focus_click("Location_SL");
			i_press_enter_key_();
			CommonSteps.I_pause_for_seconds(15);
			CommonSteps.I_clear_Field("Upto_RequestDate_SL");
			CommonSteps.I_focus_click("Upto_RequestDate_SL");
			i_enter_date_from_excelSheet_NP("DueDate", sheetname, "Upto_RequestDate_SL");
			i_press_enter_key_();
			CommonSteps.I_pause_for_seconds(5);
			i_enter_values_in_the_spreasheet(sheetname, "OrderNumber", "FromOrder_SL");
			i_press_enter_key_();
			i_enter_values_in_the_spreasheet(sheetname, "OrderNumber", "ToOrder_SL");
			CommonSteps.I_focus_click("Orders_SelectedSL");
			CommonSteps.I_focus_click("Orders_SelectedSL");
			CommonSteps.I_pause_for_seconds(5);
			CommonSteps.I_focus_click("Orders_SelectedSL");
			CommonSteps.I_clickJS("Regenerate_SL");
			CommonSteps.I_pause_for_seconds(45);
			Omega_CustomMethods.i_should_See_item_name_present_on_the_shipping_page(sheetname, "ItemName1");
			// Omega_CustomMethods.i_should_See_item_name_present_on_the_shipping_page("TestWeb",
			// "ItemName2");
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "ItemName2",
					"StoreFront");
			String value = fetchValuesFromSpreadSheet.substring(6).toString();
			String locator = "((//div[contains(text(),'"+value+"')])[2]//..//..//div)[2]";
			WebElement element = driver.findElement(By.xpath(locator));
			Actions ac = new Actions(driver);
			ac.moveToElement(element).click(element).build().perform();
			CommonSteps.I_pause_for_seconds(15);
			CommonSteps.I_click("PrintPickList_SL");
			CommonSteps.I_pause_for_seconds(5);
			CommonSteps.I_wait_for_visibility_of_element("ReportSubmitted_SL");
			CommonSteps.I_click("ReportSubmitted_SL");
			CommonSteps.I_pause_for_seconds(10);
		}
		else {
			Omega_CustomMethods.i_enter_date_from_excelSheet_NP("DueDate", sheetname, "Upto_DueDate_SL");
			CommonSteps.I_should_see_on_page("Location_SL");
			CommonSteps.I_focus_click("Location_SL");
			i_press_enter_key_();
			CommonSteps.I_pause_for_seconds(15);
			CommonSteps.I_clear_Field("Upto_RequestDate_SL");
			CommonSteps.I_focus_click("Upto_RequestDate_SL");
			i_enter_date_from_excelSheet_NP("DueDate", sheetname, "Upto_RequestDate_SL");
			i_press_enter_key_();
			CommonSteps.I_pause_for_seconds(5);
			i_enter_values_in_the_spreasheet(sheetname, "OrderNumber", "FromOrder_SL");
			i_press_enter_key_();
			i_enter_values_in_the_spreasheet(sheetname, "OrderNumber", "ToOrder_SL");
			CommonSteps.I_focus_click("Orders_SelectedSL");
			CommonSteps.I_focus_click("Orders_SelectedSL");
			CommonSteps.I_pause_for_seconds(5);
			CommonSteps.I_focus_click("Orders_SelectedSL");
			CommonSteps.I_clickJS("Regenerate_SL");
			CommonSteps.I_pause_for_seconds(45);
			try {
			Omega_CustomMethods.i_should_See_item_name_present_on_the_shipping_page(sheetname, "ItemName1");
			}catch (Exception e) {
				Omega_CustomMethods.i_should_See_item_name_present_on_the_shipping_page(sheetname, "ItemName");
			}
			CommonSteps.I_click("PrintPickList_SL");
			CommonSteps.I_pause_for_seconds(5);
			CommonSteps.I_wait_for_visibility_of_element("ReportSubmitted_SL");
			CommonSteps.I_click("ReportSubmitted_SL");
			CommonSteps.I_pause_for_seconds(10);
		}
	}

	@Given("^I get order line max due date from '(.*)' : sheet '(.*)' : column$")
	public static void i_get_next_order_line_item(String sheetname, String column) throws Exception {
//		String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, column);
//		String locator = "(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[4]";
//		System.out.println(locator);
//		WebElement element = driver.findElement(By.xpath(locator));
//		element.click();
//		comm
		CommonSteps.I_wait_for_visibility_of_element("DueDate_SL");
		CommonSteps.I_should_see_on_page("DueDate_SL");
		CommonSteps.I_pause_for_seconds(10);
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String locator = "(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])";
		List<WebElement> orders = driver.findElements(By.xpath(locator));
		if (orders.size() > 4) {
			driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[2]")).click();
			;
			Thread.sleep(3000);
			String date1 = driver
					.findElement(By.xpath("//label[text()='Calculated Due Date:']//following-sibling::div[3]//input"))
					.getAttribute("value");
			System.out.println("Due Date " + date1);
			driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[4]")).click();
			;
			Thread.sleep(3000);

			String date2 = driver
					.findElement(By.xpath("//label[text()='Calculated Due Date:']//following-sibling::div[3]//input"))
					.getAttribute("value");
			System.out.println("Due Date " + date2);
			if (Integer.parseInt(date1.substring(0, 2)) > Integer.parseInt(date2.substring(0, 2))) {
				Date date = new SimpleDateFormat("MM/dd/yyyy").parse(date1);
				ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "DueDate", date1, "SFDC");
				System.out.println("Max due date " + date);
			} else {
				Date date = new SimpleDateFormat("MM/dd/yyyy").parse(date2);
				ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "DueDate", date2, "SFDC");
				System.out.println("Max due date " + date);

			}
		}

	}

	@Given("^I get order line due date from '(.*)' : sheet '(.*)' : column$")
	public static void i_get__order_line_item(String sheetname, String column) throws Exception {
//		String fetchValuesFromSpreadSheet = Omega_CustomMethods.fetchValuesFromSpreadSheet(sheetname, column);
//		String locator = "(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[4]";
//		System.out.println(locator);
//		WebElement element = driver.findElement(By.xpath(locator));
//		element.click();
//		comm
		CommonSteps.I_wait_for_visibility_of_element("DueDate_SL");
		CommonSteps.I_should_see_on_page("DueDate_SL");
		CommonSteps.I_pause_for_seconds(10);
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String locator = "(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])";
		driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[2]")).click();
		;
		Thread.sleep(3000);
		String date1 = driver
				.findElement(By.xpath("//label[text()='Calculated Due Date:']//following-sibling::div[3]//input"))
				.getAttribute("value");
		System.out.println("Due Date " + date1);
//		driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[4]")).click();
		;
		Thread.sleep(3000);

		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(date1);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "DueDate", date1, "SFDC");
		System.out.println("Max due date " + date);
	}
	
	@When("^I verify items are packed, shipped and invoiced to complete the order from '(.*)' : '(.*)' and save the item single line$")
	public static void i_complete_the_order_lines_and_save(String sheetname, String column) throws Exception {
		CommonSteps.I_wait_for_visibility_of_element("CustomerOrderLines_StatusClick");
		CommonSteps.I_should_see_on_page("CustomerOrderLines_StatusClick");
		CommonSteps.I_pause_for_seconds(10);
		CommonSteps.I_focus_click("Refresh_btnSL");
		CommonSteps.I_pause_for_seconds(10);
		CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity", "PackedNumber");
		CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity", "ShippedNumber");
		CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname, "Quantity", "InvoiceNumber");
		CommonSteps.I_pause_for_seconds(10);
		CommonSteps.I_should_see_on_page("CustomerOrderLines_StatusClick");
		CommonSteps.I_clear_Field("CustomerOrderLines_StatusClick");
		CommonSteps.I_enter_in_field("Complete", "CustomerOrderLines_StatusClick");
		i_press_enter_key_();
		CommonSteps.I_focus_click("DueDate_SL");
		CommonSteps.I_pause_for_seconds(25);
		CommonSteps.I_click("CustomerOrderLines_Save");
		System.out.println("Save Clicked");
		CommonSteps.I_pause_for_seconds(20);
		CommonSteps.I_wait_for_visibility_of_element("Save_okBtn");
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_click("Save_okBtn");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_click("CustomerOrdersOptions");
		CommonSteps.I_pause_for_seconds(10);
		
	}

	@When("^I verify items are packed, shipped and invoiced to complete the order from '(.*)' : '(.*)' and save the item and shipping '(.*)'$")
	public static void i_complete_the_order_lines_and_save(String sheetname, String column,String ShippmentType) throws Exception {
		if(ShippmentType.contains("PartialShippment")) {
			CommonSteps.I_wait_for_visibility_of_element("CustomerOrderLines_StatusClick");
			CommonSteps.I_should_see_on_page("CustomerOrderLines_StatusClick");
			CommonSteps.I_pause_for_seconds(10);
			CommonSteps.I_focus_click("Refresh_btnSL");
			CommonSteps.I_pause_for_seconds(10);
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
					"StoreFront");
			String locator = "(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])";
			List<WebElement> orders = driver.findElements(By.xpath(locator));
			if (orders.size() > 4) {
				driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[2]")).click();
				;
				Thread.sleep(3000);

				CommonSteps.I_click("Refresh_btnSL");
				CommonSteps.I_pause_for_seconds(5);

				CommonSteps.I_click("Refresh_btnSL");
				CommonSteps.I_pause_for_seconds(5);

				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity1", "PackedNumber");
				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity1", "ShippedNumber");
				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname, "Quantity1", "InvoiceNumber");

				CommonSteps.I_should_see_on_page("CustomerOrderLines_StatusClick");
				CommonSteps.I_clear_Field("CustomerOrderLines_StatusClick");
				CommonSteps.I_enter_in_field("Complete", "CustomerOrderLines_StatusClick");
				i_press_enter_key_();
				CommonSteps.I_focus_click("DueDate_SL");
				CommonSteps.I_pause_for_seconds(25);
				CommonSteps.I_click("CustomerOrderLines_Save");
				System.out.println("Save Clicked");
				CommonSteps.I_pause_for_seconds(20);
				CommonSteps.I_click("CustomerOrdersOptions");
				CommonSteps.I_pause_for_seconds(10);
		}
		}
			
			else {
			CommonSteps.I_wait_for_visibility_of_element("CustomerOrderLines_StatusClick");
			CommonSteps.I_should_see_on_page("CustomerOrderLines_StatusClick");
			CommonSteps.I_pause_for_seconds(10);
			CommonSteps.I_focus_click("Refresh_btnSL");
			CommonSteps.I_pause_for_seconds(10);
			String fetchValuesFromSpreadSheet1 = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
					"StoreFront");
			String locator1 = "(//div[contains(text(),'" + fetchValuesFromSpreadSheet1 + "')])";
			List<WebElement> orders1 = driver.findElements(By.xpath(locator1));
			if (orders1.size() > 4) {
				driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet1 + "')])[2]")).click();
				;
				Thread.sleep(3000);

				CommonSteps.I_click("Refresh_btnSL");
				CommonSteps.I_pause_for_seconds(5);

				CommonSteps.I_click("Refresh_btnSL");
				CommonSteps.I_pause_for_seconds(5);

				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity1", "PackedNumber");
				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity1", "ShippedNumber");
				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname, "Quantity1", "InvoiceNumber");

				CommonSteps.I_should_see_on_page("CustomerOrderLines_StatusClick");
				CommonSteps.I_clear_Field("CustomerOrderLines_StatusClick");
				CommonSteps.I_enter_in_field("Complete", "CustomerOrderLines_StatusClick");
				i_press_enter_key_();
				CommonSteps.I_focus_click("DueDate_SL");
				CommonSteps.I_pause_for_seconds(25);

				driver.findElement(By.xpath("(//div[contains(text(),'" + fetchValuesFromSpreadSheet1 + "')])[4]")).click();
				;
				Thread.sleep(3000);
				CommonSteps.I_pause_for_seconds(5);
				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity2", "PackedNumber");
				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname,"Quantity2", "ShippedNumber");
				CustomMethod_Syteline.i_check_item_changed_as_quantity(sheetname, "Quantity2", "InvoiceNumber");
				CommonSteps.I_should_see_on_page("CustomerOrderLines_StatusClick");
				CommonSteps.I_clear_Field("CustomerOrderLines_StatusClick");
				CommonSteps.I_enter_in_field("Complete", "CustomerOrderLines_StatusClick");
				i_press_enter_key_();
				CommonSteps.I_focus_click("DueDate_SL");
				CommonSteps.I_pause_for_seconds(5);
				CommonSteps.I_click("CustomerOrderLines_Save");
				System.out.println("Save Clicked");
				CommonSteps.I_pause_for_seconds(10);
				CommonSteps.I_wait_for_visibility_of_element("Save_okBtn");
				CommonSteps.I_pause_for_seconds(2);
				CommonSteps.I_click("Save_okBtn");
				CommonSteps.I_pause_for_seconds(5);
				CommonSteps.I_click("CustomerOrdersOptions");
				CommonSteps.I_pause_for_seconds(10);
//				Omega_CustomMethods.i_should_see_value_from_input_field("Completed", "CustomerOrderStatus");
		}}
	}

	@Given("^I find total price at '(.*)' : sheet '(.*)' : column contained at '(.*)'$")
	public static void i_find_total_price(String sheetname, String column, String locator) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String TotalPrice = StringUtils.stripStart(fetchValuesFromSpreadSheet, "$");
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String expectedUnit = element.getAttribute("value").trim();
		System.out.println(TotalPrice);
		Assert.assertEquals(expectedUnit, TotalPrice);
	}

	@When("^I compare all the orders from '(.*)' : sheet '(.*)' : column and get the greater due date$")
	public static void i_compare_all_the_orders_and_get_the_greater_due_date(String sheetname, String column)
			throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String locator = "(//div[contains(text(),'" + fetchValuesFromSpreadSheet + "')])[4]";
		System.out.println(locator);
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();

	}

	@Given("^I find Qty '(.*)' : sheet '(.*)' : column contained at '(.*)'$")
	public static void i_find_Qty(String sheetname, String column, String locator) throws Exception {
		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, column,
				"StoreFront");
		String Qty = fetchValuesFromSpreadSheet + ".000";
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String expectedQty = element.getAttribute("value").trim();
		System.out.println(Qty);
		Assert.assertEquals(expectedQty, Qty);
	}

	/*
	 * public static String fetchValuesFromSpreadSheet(String sheetname, String
	 * columnname) throws Exception { StringBuffer cellValue = new StringBuffer();
	 * try { java.io.File f = new java.io.File( System.getProperty("user.dir") +
	 * "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx"); FileInputStream
	 * fin = new FileInputStream(f); Workbook wb = new XSSFWorkbook(fin); Sheet
	 * sheet = wb.getSheet(sheetname);
	 * 
	 * int rowCount = sheet.getPhysicalNumberOfRows();
	 * 
	 * for (int r = 0; r < rowCount; r++) {
	 * 
	 * Row hr = sheet.getRow(0);
	 * 
	 * for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) { String
	 * stringCellValue = sheet.getRow(0).getCell(i).getStringCellValue(); //
	 * System.out.println("Header " + stringCellValue); if
	 * (stringCellValue.equalsIgnoreCase(columnname)) { Row row = sheet.getRow(r +
	 * 1); if (row == null) { continue; } Cell cell = row.getCell(i); if (cell ==
	 * null) { continue; } CellType cellType = cell.getCellType(); if
	 * (cellType.equals(CellType.STRING)) { String cv = row.getCell(i,
	 * MissingCellPolicy.CREATE_NULL_AS_BLANK).getStringCellValue();
	 * cellValue.append(cv + "&");
	 * 
	 * } else if (cellType.equals(CellType.NUMERIC)) { double numericCellValue =
	 * row.getCell(i, MissingCellPolicy.CREATE_NULL_AS_BLANK)
	 * .getNumericCellValue(); long l = (long) numericCellValue; String cv =
	 * String.valueOf(l); cellValue.append(cv + "&");
	 * 
	 * } } } } String itemValue = cellValue.toString(); int andCount = 0; for (int i
	 * = 0; i < itemValue.length(); i++) { if (itemValue.charAt(i) == '&') {
	 * andCount = andCount + 1; } } if (andCount == 1) { itemValue =
	 * itemValue.replace("&", ""); } return itemValue;
	 * 
	 * } catch (Exception e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); throw new Exception(); }
	 * 
	 * }
	 */
	@When("I should see item name '(.*)' : '(.*)' present on the shipping page$")
	public static void i_should_See_item_name_present_on_the_shipping_page(String sheetName, String columnName)
			throws Exception {

		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetName,
				columnName, "StoreFront");
		if (fetchValuesFromSpreadSheet.contains("&")) {
			String[] split;
			split = fetchValuesFromSpreadSheet.split("&");
			for (int i = 0; i < split.length; i++) {
				String itemfromExcel = split[i].substring(6, split[i].length());
				List<WebElement> itemName = driver
						.findElements(By.xpath("(//div[contains(text(),'" + itemfromExcel + "')])"));
				for (WebElement wb : itemName) {
					wb.isDisplayed();
					String actaulItemFromWebPage = wb.getText();
					Assert.assertEquals(itemfromExcel, actaulItemFromWebPage);
					wrapFunc.highLightElement(wb);
					System.out.println("Actual from Mulitpe:" + actaulItemFromWebPage);
					System.out.println("Item from Excel" + itemfromExcel);
				}
			}

		}
		String itemfromExcel = fetchValuesFromSpreadSheet.substring(6, fetchValuesFromSpreadSheet.length());
		WebElement wb = driver.findElement(By.xpath("(//div[contains(text(),'" + itemfromExcel + "')])[2]"));
		wb.isDisplayed();
		String actaulItemFromWebPage = wb.getText();
		Assert.assertEquals(itemfromExcel, actaulItemFromWebPage);
		wrapFunc.highLightElement(wb);
		System.out.println("Actual from Mulitpe:" + actaulItemFromWebPage);
		System.out.println("Item from Excel" + itemfromExcel);

	}

//	public static void updateValuesToExcel(String sheetname, String columnname, String value, int rowNum)
//			throws Exception {
//		try {
//			java.io.File f = new java.io.File(
//					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
//			FileInputStream fin = new FileInputStream(f);
//			Workbook wb = new XSSFWorkbook(fin);
//			Sheet sheet = wb.getSheet(sheetname);
//			if (sheet == null) {
//				sheet = wb.createSheet(sheetname);
//			}
//			Row row = sheet.getRow(0);
//			for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
//
//				Cell cell = row.getCell(i);
//				String stringCellValue = cell.getStringCellValue();
//				if (stringCellValue.equals(columnname)) {
//					Row data = sheet.getRow(rowNum);
//					if (data == null) {
//						data = sheet.createRow(rowNum);
//					}
//
//					Cell c1 = data.getCell(i);
//					if (c1 == null) {
//						c1 = data.createCell(i);
//					}
//					c1.setCellValue(value);
//				}
//			}
//			FileOutputStream fout = new FileOutputStream(f);
//			wb.write(fout);
//			wb.close();
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			throw new Exception();
//		} //
//
//	}

//	public static void updateDateToExcel(String sheetname, String columnname, Date value, int rowNum) throws Exception {
//		try {
//			java.io.File f = new java.io.File(
//					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
//			FileInputStream fin = new FileInputStream(f);
//			Workbook wb = new XSSFWorkbook(fin);
//			Sheet sheet = wb.getSheet(sheetname);
//			Row row = sheet.getRow(0);
//			for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
//
//				Cell cell = row.getCell(i);
//				String stringCellValue = cell.getStringCellValue();
//				if (stringCellValue.equals(columnname)) {
//					Row data = sheet.getRow(rowNum);
//					if (data == null) {
//						data = sheet.createRow(rowNum);
//					}
//
//					Cell c1 = data.getCell(i);
//					if (c1 == null) {
//						c1 = data.createCell(i);
//					}
//					c1.setCellValue(value);
//				}
//			}
//			FileOutputStream fout = new FileOutputStream(f);
//			wb.write(fout);
//			wb.close();
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			throw new Exception();
//		} //
//
//	}

	@Given("^I fetch values '(.*)' from webpage and store as '(.*)'$")
	public static void i_fetch_values_from_the_webpage() {

	}

//public static void main(String[] args) {
//	String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("", column, "StoreFront");
//
//}
	@Given("^I enter '(.*)' : '(.*)' from spreadsheet in the feild '(.*)'$")
	public static void i_enter_values_in_the_spreasheet(String sheet, String column, String feild) throws Exception {
		try {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheet, column,
					"StoreFront");
			CommonSteps.I_enter_in_field(fetchValuesFromSpreadSheet, feild);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

//	public static void main(String[] args) {
//		String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheet, column,
//				"StoreFront");
//		
//	}
	@Given("^I validate '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void i_validate_values_present(String value, String sheetname, String element) throws Exception {

		try {
			 {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, value,
							"StoreFront");
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();
			Assert.assertEquals(fetchValuesFromSpreadSheet, containerText);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I find Total at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void i_find_price_value(String value, String sheetname, String element)
			throws Exception {

		try {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, value,
					"StoreFront");
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();
			
			String TotalValue = StringUtils.stripStart(fetchValuesFromSpreadSheet, "$");
			String Total = "USD " + TotalValue;
			System.out.println(Total);
			Assert.assertEquals(containerText, Total);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	public static void i_find_dicount_price(String value, String sheetname, String element) throws Exception {
		try {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, value,
					"StoreFront");
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();
			String TotalValue = fetchValuesFromSpreadSheet.substring(9);
			String Total = "USD " + TotalValue;
			System.out.println(Total);
			Assert.assertEquals(containerText.substring(0, 1), Total);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}
	
	public static void i_find_total_price_excel_value(String value, String sheetname, String element) throws Exception {
		try {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, value,
					"StoreFront");
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();
			String TotalValue = fetchValuesFromSpreadSheet.substring(1,6);
			String Total = "USD " + TotalValue;
			System.out.println(Total);
			Assert.assertEquals(containerText, Total);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I find PONumber at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void ReadExcelValueIsContainedPONumber(String PONumber, String sheetname, String element)
			throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue = "";
			String TotalValue = "";
			String Total = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();

			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {

				if (hr.getCell(i).getStringCellValue().equals(PONumber)) {
					cellValue = content.getCell(i).getStringCellValue().toString();
					TotalValue = StringUtils.stripStart(cellValue, "PO Number:");
					Total = TotalValue;
				}
			}
			Assert.assertTrue(containerText.contains(Total));
			System.out.println(Total);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I find Quantity at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void i_find_quantity_from_excel(String value, String sheetname, String element)
			throws Exception {

		try {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, value,
					"StoreFront");
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();
			String Quantity = fetchValuesFromSpreadSheet + ".00";
			Assert.assertTrue(containerText.contains(Quantity));
			System.out.println(Quantity);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I find Product at '(.*)' from '(.*)' sheet contained at '(.*)'$")
	public static void i_find_products_from_excel(String value, String sheetname, String element)
			throws Exception {
		try {
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, value,
					"StoreFront");
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();
			String TotalValue = StringUtils.stripStart(fetchValuesFromSpreadSheet, "Item #:");
			String Product = TotalValue;
			Assert.assertEquals(Product, containerText);
			System.out.println(Product);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I concat '(.*)' and '(.*)' from '(.*)' sheet is contained at '(.*)'$")
	public static void registerSalesforce(String Value1, String Value2, String sheetname, String element)
			throws Exception {

		try {
			java.io.File f = new java.io.File(
					System.getProperty("user.dir") + "\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
			FileInputStream fin = new FileInputStream(f);
			Workbook wb = new XSSFWorkbook(fin);
			Sheet sheet = wb.getSheet(sheetname);
			Row hr = sheet.getRow(0);
			Row content = sheet.getRow(1);
			String cellValue1 = "";
			String cellValue2 = "";
			String cellValue3 = "";
			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String containerText = container.getText().toString();

			for (int i = 0; i < hr.getPhysicalNumberOfCells(); i++) {
				if (hr.getCell(i).getStringCellValue().equals(Value1)) {
					cellValue1 = content.getCell(i).getStringCellValue();
				}

				if (hr.getCell(i).getStringCellValue().equals(Value2)) {
					cellValue2 = content.getCell(i).getStringCellValue();
				}

				cellValue3 = cellValue1 + " " + cellValue2;
			}
			System.out.println(cellValue3);
			Assert.assertTrue(containerText.contains(cellValue3));

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I enter '(.*)' from '(.*)' sheet to the field '(.*)'$")
	public static void enter_field(String Value, String sheetname, String element) throws Exception {

		try {

			WebElement container = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			String fetchValuesFromSpreadSheet = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, Value,
					"StoreFront");
			Thread.sleep(4000);
			container.sendKeys(fetchValuesFromSpreadSheet);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}

	@Given("^I get the details '(.*)' from '(.*)' and store as '(.*)'$")
	public static void i_get_the_details_from_orderdetails_page(String feild, String sheet, String values)
			throws Exception {

		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(feild));
		String text = element.getText();
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheet, values, text, "StoreFront");
//			System.out.println(text);

	}

	@Given("^I get the order values '(.*)' from '(.*)' and store as '(.*)'$")
	public static void i_get_the_value_details_from_orderdetails_page(String feild, String sheet, String values,
			String Appname) throws Exception {

		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(feild));
		String text = element.getAttribute("value");
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheet, values, text, Appname);
//			System.out.println(text);

	}

	@When("^I should see price '(.*)' match at '(.*)'$")
	public static void i_should_see_price_match_at(String price, String element) throws Exception {
		try {
			String actualPrice = driver.findElement(GetPageObjectRead.OR_GetElement(element)).getText();
			if (price.startsWith("$$")) {
				price = HashMapContainer.get(price);
			}
			System.out.println("Actual Price is " + actualPrice);
			System.out.println("Expected Price is " + price);
			int indexOf = actualPrice.lastIndexOf("$");
			actualPrice = actualPrice.substring(indexOf, actualPrice.length());
			Assert.assertEquals(price, actualPrice);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	public static void i_clear_the_cart_for_omega_application() throws Exception {

		CommonSteps.I_click("Minicart");
		try {
			driver.findElement(By.xpath("//div[@class='nav-cart active']//..//a[contains(text(),'Checkout')]")).click();
			// CommonSteps.I_click("Checkout_MiniCart");
			CommonSteps.I_should_see_on_page("Checkout");
			List<WebElement> removeBtn = driver.findElements(By.xpath("(//span[@data-entry-action='REMOVE'])"));
			for (int i = 1; i <= removeBtn.size(); i++) {
				if (i % 2 == 1) {
					driver.findElement(By.xpath("(//span[@data-entry-action='REMOVE'])[1]")).click();
					;
					WebElement productRemoveAlert = driver.findElement(By.xpath("//div[@class='global-alerts']//div"));
					productRemoveAlert.isDisplayed();
					wrapFunc.highLightElement(productRemoveAlert);
				}
			}
			WebElement emptyCart = driver.findElement(By.xpath("//h2[text()='Your shopping cart is empty']"));
			emptyCart.isDisplayed();
			WrapperFunctions.highLightElement(emptyCart);
			;
			CommonSteps.I_click("OmegaLogo");

		} catch (org.openqa.selenium.NoSuchElementException e) {
			driver.findElement(By.xpath("(//div[text()='Empty Cart'])[1]")).isDisplayed();
			wrapFunc.highLightElement(driver.findElement(By.xpath("(//div[text()='Empty Cart'])[1]")));
		}

	}

	@Given("^I get the date '(.*)' from '(.*)' and store as '(.*)'$")
	public static void i_get_the_details_from_page(String feild, String sheet, String values) throws Exception {

		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(feild));
		String text = element.getAttribute("value");
		System.out.println("Date: " + text);
		Date date = new SimpleDateFormat("MM/dd/yyyy").parse(text);
		System.out.println("Date: " + date);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheet, "DueDate", text, "SFDC");

	}

	@Given("^I enter date '(.*)' from '(.*)' sheet at the '(.*)'$")
	public static void i_enter_date_from_excelSheet_NP(String value, String sheetname, String location)
			throws Exception {
		try {
			String cellValue = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, value, "SFDC");
//			SimpleDateFormat Dvalue = new SimpleDateFormat("MM/dd/yyyy");
//			String DateValue = Dvalue.format(cellValue);
			System.out.println("Date: Before " + cellValue.toString());

			WebElement data = driver.findElement(GetPageObjectRead.OR_GetElement(location));
			data.clear();
			data.sendKeys(cellValue);
			System.out.println("Date: After " + data.getAttribute("value"));

		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			Thread.sleep(5000);
			e.printStackTrace();
		}
	}

	@When("^I change the screen dimension to '(.*)' - '(.*)'$")
	public static void i_change_the_screen_dimension(int width, int height) throws Exception {
		try {
			driver.manage().window().setSize(new Dimension(width, height));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("^I navigate to '(.*)' application$")
	public static void i_navigate_to_application(String url) {
		try {
			String UpdateUrl = url;
			driver.navigate().to(UpdateUrl);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);

		}
	}

	@When("^I click the RMA Number '(.*)'$")
	public static void i_click_rma_number(String number) throws Exception {
		try {
			String no = HashMapContainer.get(number);
			driver.findElement(By.xpath("//a[contains(text(),'" + no + "')]")).click();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

	@When("^I get the RMA number from confirmation message and store as '(.*)'")
	public static void i_get_rma_number_from_confirmation_message(String element) throws Exception {
		try {
			String text = driver.findElement(GetPageObjectRead.OR_GetElement("Success_Message")).getText();
			String[] split = text.split(" ");
			String rma = split[8].substring(0, split[8].length() - 1);
			HashMapContainer.add("$$" + element, rma);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}

	}

	@When("^I add scenario name$")
	public static void i_add_scenario_name() throws InterruptedException {
		Thread.sleep(5000);
		Cookie cookie = new Cookie("zaleniumMessage", "Go to home page");
		driver.manage().addCookie(cookie);
		Thread.sleep(5000);

	}

	@When("^I press key: enter$")
	public static void i_press_enter_key_() throws Exception {
		try {
			Actions ac = new Actions(driver);
			ac.sendKeys(Keys.ENTER).build().perform();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}
	}

//	@Given("^I get all element locators '(.*)' : '(.*)' on to file name: '(.*)'$")
//	public static void i_capture_locators_in_the_page(String key, String value, String fileName) throws Throwable {
//		try {
//			System.out.println(driver.getCurrentUrl());
//			LocatorGenerator.createExcel(CommonSteps.appName, fileName, driver.getCurrentUrl());
//			LocatorGenerator.getTagChildElementsbyId(driver.getPageSource(), key, value, true);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			throw new CucumberException(e.getMessage(), e);
//		}
//	}

	@Given("^I verify title of the page in US$")
	public static void I_verify_title_of_the_page_in_US() throws Exception {
		try {
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			String expectedTitle = "About Us | Omega Engineering US Site";
			Assert.assertEquals(expectedTitle, actualTitle);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Then("^I wait for element present '(.*)'")
	public static void I_wait_for_element_present(String element) throws Exception {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(GetPageObjectRead.OR_GetElement(element)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I verify title of the page in French$")
	public static void I_verify_title_of_the_page_FR() throws Exception {
		try {
			String actualTitle = driver.getTitle();
			System.out.println(actualTitle);
			String expectedTitle = "À propos de nous | Omega Engineering";
			Assert.assertEquals(expectedTitle, actualTitle);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Then("^I remove Captcha element")
	public static void I_remove_Captcha() {
		try {
			WebElement yourElement = driver.findElement(By.xpath("//div[@class='g-recaptcha']"));
			JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
			jsExecutor.executeScript("arguments[0].parentNode.removeChild(arguments[0])", yourElement);
			Thread.sleep(3000);
			JavascriptExecutor js = (JavascriptExecutor) driver;
//			js.executeScript("return document.getElementByXpath('//input[@value='Submit']').disabled=false;");

			js.executeScript("document.getElementById('bt_submit').removeAttribute('disabled');");
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	/*
	 * @Then("^I should see text '(.*)' present on page$") public static void
	 * I_should_see_text_present_on_page(String expectedText) throws Exception {
	 * 
	 * try { CommonSteps.I_should_see_text_present_on_page(expectedText); } catch
	 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace(); throw
	 * new Exception();
	 * 
	 * } }
	 */

	@Then("I press enter key")
	public void i_press_enter_key() {
		driver.findElement(By.id("1")).sendKeys(Keys.RETURN);
	}

	@Then("I select US from dropdown")
	public static void i_select_US_from_dropdown() {
		Select country = new Select(driver.findElement(By.className("delivery-country")));
		country.selectByVisibleText("United States");
	}

	@When("I select Quality from dropdown")
	public void i_select_Quality_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select type = new Select(driver.findElement(By.id("type")));
		type.selectByValue("Quality");
	}

	@When("I select UPS Ground from dropdown")
	public void i_select_UPS_Ground_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select delivery_method = new Select(driver.findElement(By.id("delivery_method")));
		delivery_method.selectByValue("UPS_US");
	}

	@When("I select CA Ground from dropdown")
	public void i_select_CA_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000iHDo")));
		state.selectByValue("CA");
	}

	@Then("^I enter name in field '(.*)'")
	public static void I_enter_name_in_field(String element) {
		try {
			// Generate a random name
			final String randomName = randomName();

			// Find the email form field
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			WebElement email = driver.findElement(GetPageObjectRead.OR_GetElement(element));

			// Type the random email to the form
			email.sendKeys(randomName);
			System.out.println(randomName);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	private static String randomName() {
		return "random-" + UUID.randomUUID().toString();
	}

	@Given("$I should enter random discount at '(.*)'^")
	public static void randomDiscount(String locator) {
		WebElement element = driver.findElement(GetPageObjectRead.OR_GetElement(locator));
		String RandomDiscount = "Discount-" + UUID.randomUUID().toString() + " 10%";
		System.out.println(RandomDiscount);
		element.sendKeys(RandomDiscount);
	}

	@When("^I navigate to asm application$")
	public static void i_navigate_to_asm_application() {
		try {
			String currentUrl = driver.getCurrentUrl();
			String url = currentUrl + "?asm=true";
			driver.navigate().to(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);

		}
	}

	@Then("^I register '(.*)'")
	public static void I_register_email(String element) {
		try {
			// Generate a random email

			String uuid = UUID.randomUUID().toString().substring(0, 10);

			final String randomEmail = uuid + "@royalcyber.com";

			// Find the email form field
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			WebElement email = driver.findElement(GetPageObjectRead.OR_GetElement(element));

			// Type the random email to the form
			email.sendKeys(randomEmail);
			HashMapContainer.add("EmailId", randomEmail);
			ExcelSpreadSheetCustomSteps.updateValuesToExcel("Register", "EmailId", randomEmail, "StoreFront");
			System.out.println(randomEmail);
		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	private static String randomEmail() {
		return "random-" + UUID.randomUUID().toString() + "@example.com";
	}

	@When("^I enter username and password to login the application$")
	public static void i_enter_username_password_to_login_the_app() throws Exception {
		try {
			CommonSteps.I_enter_in_field(System.getProperty("UserName", "taha.patel66@royalcyber.com"), "UserName");
			CommonSteps.I_pause_for_seconds(5);
			CommonSteps.I_enter_in_field(System.getProperty("PassWord", "Omega2003@"), "Password");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();

		}

	}

	@Given("^I verify url of the page$")
	public static void I_verify_url_of_the_page() throws Exception {
		try {
			String url = driver.getCurrentUrl();
			System.out.println(url);
			if (url.contains("fr")) {
				System.out.println("Page displayed in French language");
			} else {
				System.out.println("Page NOT displayed in French");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I navigate back$")
	public static void I_navigate_back() throws Exception {
		try {
			driver.navigate().back();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I verify pdf of the page$")
	public static void I_verify_pdf_of_the_page() throws Exception {
		try {
			String url = driver.getCurrentUrl();
			System.out.println(url);
			if (url.contains("pdf")) {
				System.out.println("PDF is displayed");
			} else {
				System.out.println("PDF is not displayed");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I switch to iFrame Configure Product$")
	public static void I_switch_to_iFrame_ConfigureProduct() throws Exception {
		try {
			driver.switchTo().frame("configurationIframe");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@Given("^I switch to back to default content$")
	public static void I_switch_back_to_default_content() throws Exception {
		try {
			Thread.sleep(5000);
			driver.switchTo().defaultContent();
			Thread.sleep(5000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	/*
	 * @Then("^I should see text '(.*)' present on page$") public static void
	 * I_should_see_text_present_on_page(String expectedText) throws Exception {
	 * 
	 * try { CommonSteps.I_should_see_text_present_on_page(expectedText); } catch
	 * (Exception e) { // TODO Auto-generated catch block e.printStackTrace(); throw
	 * new Exception();
	 * 
	 * } }
	 */

	@Then("^I get the value from Support Ticket page '(.*)' and store as '(.*)'$")
	public static void i_get_value_from_Support_Ticket_Page(String element, String customName) throws Exception {
		try {
			String ticket = driver.findElement(GetPageObjectRead.OR_GetElement(element)).getAttribute("value");
			System.out.println(ticket);
			HashMapContainer.add("$$" + customName, ticket);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Unable to get text from Support Ticket page");
		}

	}

	@Then("^I get the text from Support Ticket page '(.*)' and store as '(.*)'$")
	public static void i_get_text_from_Support_Ticket_Page(String element, String customName) throws Exception {
		try {
			String ticket = driver.findElement(GetPageObjectRead.OR_GetElement(element)).getText();
			System.out.println(ticket);
			HashMapContainer.add("$$" + customName, ticket);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Unable to get text from Support Ticket page");
		}

	}

	@When("I compare '(.*)' with text Omega '(.*)'$")
	public static void i_compare_ProductName(String string, String string2) throws Exception {
		try {

			String string3 = HashMapContainer.get(string);

			String string4 = HashMapContainer.get(string2);

			Assert.assertEquals(string3, string4);
			System.out.println("Actual and expected text is same");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception("Comparison failed");
		}

	}

	@Given("^I verify Spectris Footer is navigating to respective page$")
	public static void I_verify_Spectris_at_Footer_of_the_page() throws Exception {
		try {
			String title = driver.getTitle();
			System.out.println(title);
			if (title.equals("Spectris")) {
				System.out.println("Spectris is navigating to corresponding page");
			} else {
				System.out.println("Spectris is NOT navigating to corresponding page");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new Exception();
		}
	}

	@When("^I remove feedback link$")
	public static void i_remove_feedback() {
		WebElement yourElement = driver.findElement(By.xpath("//span[text()='Feedback']"));
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].parentNode.removeChild(arguments[0])", yourElement);
	}

	@When("^I remove need help$")
	public static void i_remove_need_help() {
		WebElement yourElement = driver.findElement(By.xpath("//div[@class='chat_invitation']"));
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].parentNode.removeChild(arguments[0])", yourElement);
	}

	@Given("^I focus and click the button '(.*)'$")
	public static void I_focus_And_click(String element) throws Exception {
		try {
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));

			Actions ac = new Actions(driver);
			WebElement we = driver.findElement(GetPageObjectRead.OR_GetElement(element));
			ac.moveToElement(we).build().perform();
			ac.click().build().perform();

		} catch (Exception e) {
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Then("I select United States from dropdown")
	public void i_select_United_States_from_dropdown() {
		Select country = new Select(driver.findElement(By.id("address.country")));
		country.selectByVisibleText("United States");
	}

	@When("I select Albama from dropdown")
	public void i_select_Albama_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000iHDo")));
		state.selectByValue("AL");
	}

	@When("I select 3-10 in dropdown")
	public void i_select_3_10_in_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000hLJF")));
		state.selectByValue("3-10");
	}

	@When("I select option Government Agency from dropdown")
	public void i_select_option_Government_Agency_from_dropdown() {
		// Write code here that turns the phrase above into concrete actions
		Select state = new Select(driver.findElement(By.id("00N2F000000hQOy")));
		state.selectByValue("Government Agency");
	}
}