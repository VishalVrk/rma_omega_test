package com.TEAF.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class CustomMethod_StoreFront {

	static WrapperFunctions wrapFunc = new WrapperFunctions();
	static StepBase sb = new StepBase();
	// static StepBase_D2 sb = new StepBase_D2();
	public static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	
	@When("^I should see the one line order related information and store it in a excel sheet '(.*)'$")
	public static void i_should_see_the_one_order_related_information_and_store(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("OrderNumber");
		CommonSteps.I_should_see_on_page("OrderPlacedBy");
		CommonSteps.I_should_see_on_page("OrderStatus");
		CommonSteps.I_should_see_on_page("DatePlaced");
		CommonSteps.I_should_see_on_page("Total");
		CommonSteps.I_should_see_on_page("ShipTo");
		CommonSteps.I_should_see_on_page("ShippingMethod");
		CommonSteps.I_scroll_to_element("element", "ShipTo");
		Thread.sleep(3000);
		CommonSteps.I_should_see_on_page("ProductName");
		CommonSteps.I_should_see_on_page("ItemName");
		CommonSteps.I_should_see_on_page("UnitPrice_1");
		CommonSteps.I_should_see_on_page("Quantity");
		CommonSteps.I_should_see_on_page("TotalPrice");
		CommonSteps.I_should_see_on_page("BillingAddress");
//		CommonSteps.I_should_see_on_page("CCPaymentType");
		CommonSteps.I_should_see_on_page("OrderTotal");
		CommonSteps.I_should_see_on_page("OrderSummary_Subtotal");
		CommonSteps.I_should_see_on_page("OrderSummary_Delivery");
		CommonSteps.I_should_see_on_page("OrderSummary_Tax");
		Omega_CustomMethods.i_get_the_emailid_order_confirmation_page(sheetname, "ContactEmail");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderNumber", sheetname, "OrderNumber");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderPlacedBy", sheetname, "OrderPlacedBy");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderStatus", sheetname, "OrderStatus");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("DatePlaced", sheetname, "DatePlaced");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Total", sheetname, "Total");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShipTo", sheetname, "ShipTo");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShippingMethod", sheetname, "ShippingMethod");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ProductName", sheetname, "ProductName");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ItemName", sheetname, "ItemName");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("UnitPrice_1", sheetname, "UnitPrice");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Quantity", sheetname, "Quantity");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("TotalPrice", sheetname, "TotalPrice");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("BillingAddress", sheetname, "BillingAddress");
//		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("CCPaymentType", sheetname, "PaymentOption");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderTotal", sheetname, "OrderTotal");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderSummary_Delivery", sheetname, "Delivery");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderSummary_Subtotal", sheetname, "Subtotal");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderSummary_Tax", sheetname, "Tax");
		StepBase.embedScreenshot();
	}

	@When("^I should see the two line order related information and store it in a excel sheet '(.*)'$")
	public static void i_should_see_the_order_related_information_and_store(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("OrderNumber");
		CommonSteps.I_should_see_on_page("OrderPlacedBy");
		CommonSteps.I_should_see_on_page("OrderStatus");
		CommonSteps.I_should_see_on_page("DatePlaced");
		CommonSteps.I_should_see_on_page("Total");
		CommonSteps.I_should_see_on_page("ShipTo");
		CommonSteps.I_should_see_on_page("ShippingMethod");
		CommonSteps.I_scroll_to_element("element", "ShipTo");
		Thread.sleep(3000);
		CommonSteps.I_should_see_on_page("ProductName1");
		CommonSteps.I_should_see_on_page("ItemName1");
		CommonSteps.I_should_see_on_page("UnitPrice_1");
		CommonSteps.I_should_see_on_page("Quantity1");
		CommonSteps.I_should_see_on_page("TotalPrice1");
		CommonSteps.I_should_see_on_page("ProductName2");
		CommonSteps.I_should_see_on_page("ItemName2");
		CommonSteps.I_should_see_on_page("UnitPrice_2");
		CommonSteps.I_should_see_on_page("Quantity2");
		CommonSteps.I_should_see_on_page("TotalPrice2");
		CommonSteps.I_should_see_on_page("BillingAddress");
//		CommonSteps.I_should_see_on_page("CCPaymentType");
		CommonSteps.I_should_see_on_page("OrderTotal");
		CommonSteps.I_should_see_on_page("OrderSummary_Subtotal");
		CommonSteps.I_should_see_on_page("OrderSummary_Delivery");
		CommonSteps.I_should_see_on_page("OrderSummary_Tax");
		Omega_CustomMethods.i_get_the_emailid_order_confirmation_page(sheetname, "ContactEmail");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderNumber", sheetname, "OrderNumber");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderPlacedBy", sheetname, "OrderPlacedBy");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderStatus", sheetname, "OrderStatus");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("DatePlaced", sheetname, "DatePlaced");
//		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Total", sheetname, "Total");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShipTo", sheetname, "ShipTo");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ShippingMethod", sheetname, "ShippingMethod");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ProductName1", sheetname, "ProductName1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ItemName1", sheetname, "ItemName1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("UnitPrice_1", sheetname, "UnitPrice1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Quantity1", sheetname, "Quantity1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("TotalPrice1", sheetname, "TotalPrice1");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ProductName2", sheetname, "ProductName2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("ItemName2", sheetname, "ItemName2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("UnitPrice_2", sheetname, "UnitPrice2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("Quantity2", sheetname, "Quantity2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("TotalPrice2", sheetname, "TotalPrice2");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("BillingAddress", sheetname, "BillingAddress");
//		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("CCPaymentType", sheetname, "PaymentOption");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderTotal", sheetname, "OrderTotal");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderSummary_Delivery", sheetname, "Delivery");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderSummary_Subtotal", sheetname, "Subtotal");
		Omega_CustomMethods.i_get_the_details_from_orderdetails_page("OrderSummary_Tax", sheetname, "Tax");
		StepBase.embedScreenshot();
	}

	@When("^I verify the order related information in order history page from '(.*)'$")
	public static void i_verify_the_order_related_information_in_order_details_page(String sheetname) throws Exception {

		CommonSteps.I_should_see_on_page("OrderNumberTitle");
		CommonSteps.I_should_see_on_page("OrderPlacedTitle");
		CommonSteps.I_should_see_on_page("TotalTitle");
		CommonSteps.I_should_see_on_page("OrderStatusTitle");
		CommonSteps.I_should_see_on_page("OrderPlacedByTitle");
		String orderno = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb", "OrderNumber", "StoreFront");
		CommonSteps.I_should_see_text_present_on_page_At(orderno, "OrderNumberValue");
		String date = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb", "DatePlaced", "StoreFront");
		CommonSteps.I_should_see_on_page("OrderPlacedValue");
		// CommonSteps.I_should_see_text_present_on_page_At(date, "OrderPlacedValue");
		String total = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb", "OrderTotal", "StoreFront");
		CommonSteps.I_should_see_text_present_on_page_At(total.trim(), "TotalValue");
		String placedby = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb", "OrderPlacedBy",
				"StoreFront");
		CommonSteps.i_should_see_contains_text_present_on_page_at("OrderPlacedByValue", placedby.trim());

		String shipTo = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb", "ShipTo", "StoreFront");
		CommonSteps.I_should_see_on_page("ShipToAddress");
		// CommonSteps.i_should_see_contains_text_present_on_page_at("ShipToAddress",
		// shipTo.trim());

		String shipToMethod = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb", "ShippingMethod",
				"StoreFront");
		CommonSteps.I_should_see_on_page("ShippingMethod");

//		CommonSteps.i_should_see_contains_text_present_on_page_at("ShipToAddress_OHP", shipToMethod.trim());

		String billToMethod = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("TestWeb", "BillingAddress",
				"StoreFront");
		CommonSteps.I_should_see_on_page("BillingAddress");

		// CommonSteps.i_should_see_contains_text_present_on_page_at("BillToAddress_OHP",
		// billToMethod.trim());

	}
	
	@Given("^I click on the RMA ID from '(.*)' : sheet$")
	public static void i_click_on_RMAID(String sheetname) throws Exception {
		String value = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet(sheetname, "RMA_ID", "StoreFront");
		String locator ="//a[contains(text(),'"+value+"')]";
		System.out.println("Found Locator: "+locator);
		WebElement element = driver.findElement(By.xpath(locator));
		wrapFunc.highLightElement(element);
		element.click();
	}
	
	@Given("^I enter discount unit price$")
	public static void i_enter_discount_unit_() throws Exception {
		String Fetchvalues = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("VolumeDiscountsRMA", "UnitPrice", "StoreFront");
		String Fetchvalues1 = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("VolumeDiscountsRMA", "TotalPrice", "StoreFront");
		String totalprice = Fetchvalues1.replaceAll("*", "");
		String unitprice = Fetchvalues.substring(6);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel("VolumeDiscountsRMA", "UnitPrice", unitprice, "StoreFront");
		ExcelSpreadSheetCustomSteps.updateValuesToExcel("VolumeDiscountsRMA", "TotalPrice", totalprice, "StoreFront");
	}
	
	@Given("^I select return quantity from ordered quantity for line full return$")
	public static void i_validate_return_order_price() throws Exception {
		CommonSteps.I_should_see_on_page("RestockingFee_item1");
		CommonSteps.I_should_see_on_page("ProductPrice_1");
		CommonSteps.I_should_see_on_page("QtyProduct_1");
		CommonSteps.I_mouse_over("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_disbaled_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_clear_Field("FirstProductReturnQty");
		String Qty = driver.findElement(GetPageObjectRead.OR_GetElement("QtyProduct_1")).getText().trim();
		System.out.println("Quantity to return: "+Qty);
		CommonSteps.i_enter_in_the_feild_using_actions(Qty, "FirstProductReturnQty");
		Omega_CustomMethods.i_press_enter_key_();
		CommonSteps.I_focus_click("QtyProduct_1");
		CommonSteps.I_pause_for_seconds(5);
	}
	
	@Given("^I select return quantity from ordered quantity for line full return for multi-line orders$")
	public static void i_enter_return_quntity_for_multi_line_order() {
		CommonSteps.I_should_see_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_disbaled_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_on_page("ReturnOrderPageHeader");
		CommonSteps.I_should_see_on_page("RestockingFee_item1");
		CommonSteps.I_should_see_on_page("ProductPrice_1");
		CommonSteps.I_should_see_on_page("QtyProduct_1");
		CommonSteps.I_scroll_to_element("coordinates", "0,150");
		CommonSteps.I_should_see_on_page("RestockingFee_item2");
		CommonSteps.I_should_see_on_page("ProductPrice_2");
		CommonSteps.I_should_see_on_page("QtyProduct_2");
		String Qty1 = driver.findElement(GetPageObjectRead.OR_GetElement("QtyProduct_1")).getText().trim();
		System.out.println("Quantity to return: "+Qty1);
		CommonSteps.I_clear_Field("FirstProductReturnQty");
		CommonSteps.i_enter_in_the_feild_using_actions(Qty1, "FirstProductReturnQty");
		CommonSteps.I_select_option_in_dd_by("OrderedInError", "ReturnReasonDD", "value");
		String Qty2 = driver.findElement(GetPageObjectRead.OR_GetElement("QtyProduct_2")).getText().trim();
		System.out.println("Quantity to return: "+Qty2);
		CommonSteps.I_clear_Field("SecondProductBlock");
		CommonSteps.i_enter_in_the_feild_using_actions(Qty2, "SecondProductReturnQty");
		CommonSteps.I_select_option_in_dd_by("OrderedInError", "ReturnReasonDD2", "value");
	}
	
	@Given("^I select return quantity from ordered quantity for line full return for multi-line orders customer error$")
	public static void i_enter_return_quntity_for_multi_line_order_customer_error() {
		CommonSteps.I_should_see_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_disbaled_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_on_page("ReturnOrderConfirmationBtn");
		CommonSteps.I_should_see_on_page("ReturnOrderPageHeader");
		CommonSteps.I_should_see_on_page("RestockingFee_item1");
		CommonSteps.I_should_see_on_page("ProductPrice_1");
		CommonSteps.I_should_see_on_page("QtyProduct_1");
		CommonSteps.I_scroll_to_element("coordinates", "0,150");
		CommonSteps.I_should_see_on_page("RestockingFee_item2");
		CommonSteps.I_should_see_on_page("ProductPrice_2");
		CommonSteps.I_should_see_on_page("QtyProduct_2");
		String Qty1 = driver.findElement(GetPageObjectRead.OR_GetElement("QtyProduct_1")).getText().trim();
		System.out.println("Quantity to return: "+Qty1);
		CommonSteps.I_clear_Field("FirstProductReturnQty");
		CommonSteps.i_enter_in_the_feild_using_actions(Qty1, "FirstProductReturnQty");
		CommonSteps.I_select_option_in_dd_by("2", "ReturnReasonDD", "index");
		String Qty2 = driver.findElement(GetPageObjectRead.OR_GetElement("QtyProduct_2")).getText().trim();
		System.out.println("Quantity to return: "+Qty2);
		CommonSteps.I_clear_Field("SecondProductReturnQty");
		CommonSteps.i_enter_in_the_feild_using_actions(Qty2, "SecondProductReturnQty");
		CommonSteps.I_select_option_in_dd_by("2", "ReturnReasonDD2", "index");
	}
	
	
	@Given("^I get refund amount and restocking fee and store in '(.*)' : sheet$")
	public static void i_get_refund_amount_store_in_excel(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("ProductPrice_1");
		CommonSteps.I_should_see_on_page("RestockingFeeAmount_1");
		CommonSteps.I_should_see_on_page("QtyProduct_1");
		CommonSteps.I_should_see_on_page("ProductTotal_1Review");
		String RefundAmount = driver.findElement(GetPageObjectRead.OR_GetElement("RestockingFeeAmount_1")).getText().trim();
		System.out.println("Refund Amount: "+RefundAmount);
		String RestockingFee = driver.findElement(GetPageObjectRead.OR_GetElement("ProductTotal_1Review")).getText().trim();
		System.out.println("Refund Fee: "+RestockingFee);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "RefundAmount", RestockingFee, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+RefundAmount);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "RestockingFee", RefundAmount, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+RestockingFee);
	}
	
	@Given("^I get refund amount and restocking fee and store in '(.*)' : sheet multi line$")
	public static void i_get_refund_amount_store_in_excel_multiline_order(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("ProductPrice_1");
		CommonSteps.I_should_see_on_page("RestockingFeeAmount_1");
		CommonSteps.I_should_see_on_page("QtyProduct_1");
		CommonSteps.I_should_see_on_page("ProductTotal_1Review");
		String RefundAmount_1 = driver.findElement(GetPageObjectRead.OR_GetElement("RestockingFeeAmount_1")).getText().trim();
		System.out.println("Refund Amount: "+RefundAmount_1);
		String RestockingFee_1 = driver.findElement(GetPageObjectRead.OR_GetElement("ProductTotal_1Review")).getText().trim();
		System.out.println("Refund Fee: "+RestockingFee_1);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "RefundAmount", RefundAmount_1, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+RefundAmount_1);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "RestockingFee", RestockingFee_1, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+RestockingFee_1);
		CommonSteps.I_scroll_to_element("coordinates", "0,150");
		CommonSteps.I_should_see_on_page("ProductTotal_2");
		CommonSteps.I_should_see_on_page("RestockingFeeAmount_2");
		CommonSteps.I_should_see_on_page("QtyProduct_2");
		CommonSteps.I_should_see_on_page("ProductTotal_2Review");
		String RefundAmount_2 = driver.findElement(GetPageObjectRead.OR_GetElement("RestockingFeeAmount_1")).getText().trim();
		System.out.println("Refund Amount: "+RefundAmount_2);
		String RestockingFee_2 = driver.findElement(GetPageObjectRead.OR_GetElement("ProductTotal_1Review")).getText().trim();
		System.out.println("Refund Fee: "+RestockingFee_2);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "RefundAmount", RefundAmount_2, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+RefundAmount_2);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "RestockingFee", RestockingFee_2, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+RestockingFee_2);
	}
	
	@Given("^I get Subtotal total and tax refund and store in '(.*)' : sheet$")
	public static void i_get_total_subtotal_and_refund_tax(String sheetname) throws Exception {
		CommonSteps.I_should_see_on_page("ReturnDelivery_Cost");
		CommonSteps.I_should_see_on_page("ReturnTotal_refund");
		CommonSteps.I_should_see_on_page("ReturnSubtotal");
		String Return_Subtotal = driver.findElement(GetPageObjectRead.OR_GetElement("ReturnSubtotal")).getText().trim();
		System.out.println("Refund Subtotal: "+Return_Subtotal);
		String Return_TotalRefund = driver.findElement(GetPageObjectRead.OR_GetElement("ReturnTotal_refund")).getText().trim();
		System.out.println("Refund Total: "+Return_TotalRefund);
		String Return_DeliveryCost = driver.findElement(GetPageObjectRead.OR_GetElement("ReturnDelivery_Cost")).getText().trim();
		System.out.println("Refund Cost: "+Return_DeliveryCost);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "Return_Subtotal", Return_Subtotal, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+Return_Subtotal);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "Return_TotalRefund", Return_TotalRefund, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+Return_TotalRefund);
		ExcelSpreadSheetCustomSteps.updateValuesToExcel(sheetname, "Return_DeliveryCost", Return_DeliveryCost, "StoreFront");
		System.out.println("Updated: "+sheetname+" "+Return_TotalRefund);
	}
	
	
	@Given("^I navigate to return history Page$")
	public static void i_navigate_to_return_history_page() {
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_scroll_to_element("coordinates", "0,150");
		CommonSteps.I_click("MyAccount");
		CommonSteps.I_should_see_on_page("OrderHistorySubMenu");
		CommonSteps.I_click("OrderHistorySubMenu");
		CommonSteps.I_should_see_on_page("ReturnHistory_sublink");
		CommonSteps.I_click("ReturnHistory_sublink");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_should_see_on_page("FirstElementReturnHistory");
	}
	
	@Given("^I login to the backoffice Hrbris$")
    public static void i_login_to_backoffice_hybris() throws Exception {
        try {
            for (int i = 0; i < 7; i++) {
                driver.findElement(GetPageObjectRead.OR_GetElement("Hybris_LoginUsername")).sendKeys("admin");

 

                driver.findElement(GetPageObjectRead.OR_GetElement("Hybris_LoginPassword")).sendKeys("nimda", Keys.ENTER);

 

                CommonSteps.I_pause_for_seconds(5);
                try {
                    WebElement hybrisMarketign = driver
                            .findElement(GetPageObjectRead.OR_GetElement("Hybris_orderOption"));
                    WebDriverWait wb = new WebDriverWait(driver, 30);
                    wb.until(ExpectedConditions.visibilityOf(hybrisMarketign));
                    break;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    System.out.println("Relogin for " + i + " times for Hybris backoffice");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception();
        }
    }
	
	@Given("^I enter date before 60 days to validate restocking fee from Back-office$")
	public static void i_change_date_in_hybris_back_office() throws Exception {
		Omega_CustomMethods.i_navigate_to_application("https://spt-q-adm.ms.ycs.io/backoffice/login.zul");
		CommonSteps.I_pause_for_seconds(5);
		i_login_to_backoffice_hybris();
		CommonSteps.I_wait_for_visibility_of_element("Hybris_orderOption");
		CommonSteps.I_should_see_on_page("Hybris_orderOption");
		CommonSteps.I_click("Hybris_orderOption");
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_should_see_on_page("Hybris_SelectOrders");
		CommonSteps.I_click("Hybris_SelectOrders");
		CommonSteps.I_pause_for_seconds(10);
		CommonSteps.I_wait_for_visibility_of_element("Hybris_SearchField");
		CommonSteps.I_should_see_on_page("Hybris_SearchField");
		Omega_CustomMethods.enter_field("OrderNumber", "RestockingFee", "Hybris_SearchField");
		CommonSteps.I_pause_for_seconds(5);
		Omega_CustomMethods.i_press_enter_key_();
		CommonSteps.I_pause_for_seconds(5);
		String value = ExcelSpreadSheetCustomSteps.fetchValuesFromSpreadSheet("RestockingFee", "OrderNumber", "StoreFront");
		String xpathvalue = "//span[text()='"+value+"']";
		System.out.println("Xpath: "+xpathvalue);
		WebElement element = driver.findElement(By.xpath(xpathvalue));
		element.click();
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_should_see_on_page("Hybris_Positions");
		CommonSteps.I_click("Hybris_Positions");
		CommonSteps.I_clear_Field("Hybris_PositionsDate");
		Omega_CustomMethods.i_get_60_days_before_given_date("Hybris_PositionsDate");
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_click("Hybris_SaveButton");
		CommonSteps.I_pause_for_seconds(2);
		CommonSteps.I_click("Hybris_rightButton");
		CommonSteps.I_click("Hybris_rightButton");
		CommonSteps.I_should_see_on_page("Hybris_ConsignmentsTab");
		CommonSteps.I_click("Hybris_ConsignmentsTab");
		CommonSteps.I_doubleClick("Hybris_ShippingMethod");
		CommonSteps.I_should_see_on_page("Hybris_ShippingMethodDate");
		CommonSteps.I_clear_Field("Hybris_ShippingMethodDate");
		Omega_CustomMethods.i_get_60_days_before_given_date("Hybris_ShippingMethodDate");
		Omega_CustomMethods.i_press_enter_key_();
		CommonSteps.I_pause_for_seconds(5);
		CommonSteps.I_click("Hybris_SaveConsigment");
	}
}
